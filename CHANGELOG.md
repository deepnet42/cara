# Changelog 

All notable changes to this project will be documented in this file.


## [0.6] - 2024-09-18

### Security Enhancements and Behavior Changes
The changes below enhance the security of Cara and improve its stability.

- Unknown instruction handling
	- **Previous behavior**: Unknown or undefined instructions would halt the system.
	- **New behavior**: Unknown or undefined instructions now emit an error and terminate the program instead of silently halting the system.

**Rationale**

Halting on unknown instructions made it unclear if the program terminated successfully or due to an unknown instruction acting like a HALT instruction.

Error messages provide clearer feedback to developers and users about invalid code.

**Notes**

Unknown instructions typically require bypassing the parser, which is usually done for debugging purposes.

Editing bytecode directly could previously cause unknown instructions to be treated as internal HALT instructions, making it difficult to distinguish between normal exits and early termination.

The bytecode compiler itself cannot generate unknown instructions due to its reliance on the parser.

**Consideration**

Some may argue for unknown instructions to behave like NO OP (no operation). This is not implemented in this update, but it's a consideration for future versions.

## [0.5] - 2023-04-07

### Security Enhancements
The changes below enhance the security of Cara and improve its stability.

- Added parser checks to prevent jumps (JUMP, JUMPN, JUMPZ) to locations beyond the program listing.
- Added parser checks to prevent labels from being added to the end of the program, which can cause runtime errors.
- Implemented static code checks. Note that Cara already has runtime guards, but detecting errors statically can trigger them earlier.