# Introduction

## Bring your Human Resource Machine code to life with Cara!

Cara is a powerful command line suit of tools that allows you to run and experiment with code for the popular puzzle game [Human Resource Machine](https://tomorrowcorporation.com/humanresourcemachine) (HRM). Cara runs any and all HRM code. Cara is also platform independent.

### Cara's Key Features:
+	**Simple**: Run Human Resource Machine code with ease and see the results.
+	**Extensible**: Cara goes beyond the game by allowing you to extend the original instruction set, memory size, and word size. This opens a world of possibilities for experimentation and customization. Your imagination is the limit.
+	**Lightweight and portable**: No need for external libraries. Cara is easy to set up and use from the source.

<div align="center">

![Human Resource Machine Logo](https://upload.wikimedia.org/wikipedia/en/e/e1/Human_resource_machine_cover.jpg "Human Resource Machine Logo")

</div>

You can learn more about Human Resource Machine by following the links below:


>https://tomorrowcorporation.com/humanresourcemachine \
>https://en.wikipedia.org/wiki/Human_Resource_Machine


If you want to read some of my own articles on the Human Resource Machine game you can find them below: 

>[Human Resource Machine: Part 1](https://www.deepnet42.com/post/2021/11/15/human-resource-machine-part-1/) \
>[Human Resource Machine: Part 2](https://www.deepnet42.com/post/2021/12/05/human-resource-machine-part-2/)



>Cara is **not** an emulator in a traditional sense. Why? Cara **does not** try to achieve one to one compatibility with the Human Resource Machine; Cara supports the original instructions of the Human Resource Machine. Cara can be extended with new instructions, a larger word size, a larger memory, etc.


Cara is written in C++. \
Zero external libraries used making it easy to build.

# Quick Start
[Detailed Build Instructions](BUILD-INSTRUCTIONS.md)


# Updates

* Security and stability enhancements
* Detailed changes are now documented in [CHANGELOG.md](CHANGELOG.md)
* New Instructions added 
	- see below (random)
	- see below (stack operations)
* Major update: Windows support added 😺
	- Compiles on Windows (WIN64) systems using Microsoft Visual C++ 2019 (C++20)
* Parsing Reports for 3rd Party code:
	- Parsing reports for HRM code based on [arturoherrero](https://github.com/arturoherrero) solutions (see: https://github.com/arturoherrero/human-resource-machine)
		+ ![](https://img.shields.io/badge/parsing-pass-green)  [report 3 (csv) - arturoherrero](docu/report3.csv)
		+ ![](https://img.shields.io/badge/parsing-pass-green)  [report 3 (txt) - arturoherrero](docu/report3.txt)
	- Parsing reports for HRM code based on Ash's (@PetulantWeeb) solutions (see: https://gitlab.com/PetulantWeeb/human-resource-machine-solutions.)
		+ ![](https://img.shields.io/badge/parsing-pass-green)  [report 2 (csv) - PetulantWeeb](docu/report2.csv)
		+ ![](https://img.shields.io/badge/parsing-pass-green)  [report 2 (txt) - PetulantWeeb](docu/report2.txt)
	- Parsing reports for compiling the largest publicly available collection of HRM code (https://github.com/atesgoral/hrm-solutions). Quite amazing that all the files passed validation on the parser the very first time. However, this does not include running the source code since each program requires input, known good output, and initial memory configuration state. Time is also included in the report and should not be used for reference due to the fact that it is measuring a debug build in a virtual environment.
		+ ![](https://img.shields.io/badge/parsing-pass-green)  [report (csv) - many contributors](docu/report.csv)
		+ ![](https://img.shields.io/badge/parsing-pass-green)  [report (txt) - many contributors](docu/report.txt)
* Bytecode support added to cara vm.
	- cara.bytecode.compiler
		+ Compile a plain text ASCII HRM source to bytecode.
	- [specification sheet available](docu/bytecode.md)
	- [Getting Started Guide](docu/getting-started.md)


# Supported Commands/Instructions
## Default Commands


+ Input method for Cara is a file queue, called the INBOX;
+ Output method for Cara is a file queue, called the OUTBOX;

| COMMAND | DESCRIPTION |
| :--- | :--- |
| `INBOX` | Pop from the front of the `INBOX` queue into the register. |
| `OUTBOX` | Push the register value into the back of the `OUTBOX` queue. |
|`COPYTO` | Copy a register value to memory (has direct and indirect version) |
|`COPYFROM` | Copy from a value from memory to a register (has direct and indirect version) |
|`ADD` | Adds the current value in the register to the value held in memory and stores the result in the register. Memory is not updated. Use `COPYTO` to update the memory. (has direct and indirect version) |
|`SUB` | Same as add but subtracts.|
|`BUMPUP` | Increment memory value by one, update memory value, and copy the new value to a register (has direct and indirect version) |
|`BUMPDN` | Same as `BUMPUP` but decrement by one |
`JUMP` | Jump to any memory location specified by an address (line number) |
|`JUMPN` | Same as above but `JUMP` only if a register is negative, else execute the next instruction (next line) |
|`JUMPZ` | Same as above but `JUMP` only if a register value is equal to zero. |


## Non compliant Commands


| COMMAND | DESCRIPTION |
| :--- | :--- |
| `MUL` | Same as `ADD` but multiply. |
| `DIV` | Same as `ADD` but divide. Note: Division by zero is an error. |
| `MOD` | Same as `ADD` but mod. Note: Mod by zero is an error. |
| `RAND` | Sets register to random number between `-999` and `999`  |
| `PUSH` | Pushes a value from a register onto a stack |
| `POP` | Pops a value from the top of the stack into a register |

### Notes on Non-compliant Commands

##### Stack operations

Cara has a built-in stack. A `push` operation will take a value from a register and place it onto the top of a stack. It is an error to push into a full stack. The default maximum size for a stack is 32.

A `pop` operation will remove the value from the top of the stack and assign it to a register. It is an error to pop an empty stack.

##### RAND command

A `rand` command generates a random value in the range `[-999,999]`

# About Human Resource Machine Programs

<div align="center">


![Human Resource Machine Internals](https://www.deepnet42.com/images/hrm/hrm.png "Human Resource Machine Internals")

</div>

All *interesting* Human Resource Machine (HRM) programs read input, process it, and write output. The input comes from an `INBOX` queue and the output is written to an `OUTBOX` queue. Each program is a *blackbox* such that a list of instructions is stored and executed in a sequential order. Instructions can change the flow of the program and manipulate memory, write output, and read input. The *blackbox* has one register, which we call the register which can be thought of as scrap or work variable. Commonly such register is called an accumulator, but we will simply call it the register since it is the only one; it is a generic register.


## Sample HRM Program


The program listing below sorts a list of numbers.

```
-- HUMAN RESOURCE MACHINE PROGRAM --

a:
    COPYFROM 24
    COPYTO   20
    COPYTO   21
    COPYTO   22
    COPYTO   19
b:
    INBOX   
    COPYTO   [20]
    JUMPZ    c
    BUMPUP   20
    JUMP     b
c:
    JUMP     h
d:
    COPYFROM [21]
    COPYTO   23
    COPYFROM [19]
    COPYTO   [21]
    COPYFROM 23
    COPYTO   [19]
    JUMP     l
e:
f:
    COPYFROM [19]
    SUB      [22]
    JUMPN    g
    COPYFROM 22
    COPYTO   19
g:
    BUMPUP   22
    SUB      20
    JUMPN    f
    JUMP     d
h:
i:
    BUMPUP   22
    SUB      20
    JUMPN    j
    JUMP     k
j:
    JUMP     e
k:
l:
    BUMPUP   21
    SUB      20
    JUMPN    m
    JUMP     n
m:
    COPYFROM 21
    COPYTO   19
    COPYTO   22
    JUMP     i
n:
    COPYFROM 24
    COPYTO   21
o:
    COPYFROM [21]
    JUMPZ    a
    OUTBOX  
    BUMPUP   21
    JUMP     o
```


The program above takes the input from an inbox stream, which is a queue of numbers and/or single letters separated by a white space.

The program above writes the output by sending the items to an outbox, which is a queue of numbers and/or letters separated by white space.

The above program is configured with a certain amount of internal working space. Like memory on any real computer, this internal working space is addressable.

Cara allocates `32` memory cells for every program. The cells are numbered by an address from `0` to `31`. It is easy to configure Cara to accept larger memory page files and configure the word size. The word size of each cell respects the original limits imposed by the game; The word size range is from '-999` to `999`. 


A sample inbox file might look like this:

## Sample Inbox file

```
cat ./sortingfloor.inbox 
10 9 8 7 6 5 4 3 2 1 0 A B C D E F 0 F E Z Y R Z L K 0 1 2 3
```
> There is no limit on the size of the program. Each cell can contain numbers from `-999 to 999`, and/or `A to Z`. The limits are arbitrary and can be extended, however this will make your programs non compatible with the original game.



## Sample memory file


```
cat ./sortingfloor.mem
. . . . . . . . . . . .
. . . . . . . . . . . . 0
```
> '.' represents an uninitialized memory cell aka `EMPTY` cell. Notice that we specify memory sequentially. The first entry is the first box, the second entry is the second box, etc. If you want to set say memory at address 5 to the value A you need to write "`. . . . A`" 

NEW MEMORY INPUT METHOD ADDED (detected automatically)
```
cat ./sortingfloor.mem
[24] = 0
```
>`[ADDRESS] = VALUE`, where `ADDRESS` is in range of [0,31] and `VALUE` is in range of [-999,999]. 

## Running the program


```
./cara ./sortingfloor.hrm ./sortingfloor.inbox ./sortingfloor.mem ./sortingfloor.control 
1 2 3 4 5 6 7 8 9 10 65(A) 66(B) 67(C) 68(D) 69(E) 70(F) 69(E) 70(F) 75(K) 76(L) 82(R) 89(Y) 90(Z) 90(Z) 1 2 3 
[info    ]: Output validation PASSED against control

```

or

```
./cara --ez sortingfloor
1 2 3 4 5 6 7 8 9 10 65(A) 66(B) 67(C) 68(D) 69(E) 70(F) 69(E) 70(F) 75(K) 76(L) 82(R) 89(Y) 90(Z) 90(Z) 1 2 3 
[info    ]: Output validation PASSED against control

```


## Compiling HRM source to bytecode

```
./cara.bytecode.compiler source.hrm destination.hrmb
Use extension .hrmb for bytecode
```


## Running the program from bytecode

```
/cara --bytecode ./sortingfloor.hrmb ./sortingfloor.inbox ./sortingfloor.mem ./sortingfloor.control
1 2 3 4 5 6 7 8 9 10 65(A) 66(B) 67(C) 68(D) 69(E) 70(F) 69(E) 70(F) 75(K) 76(L) 82(R) 89(Y) 90(Z) 90(Z) 1 2 3 
[info    ]: Output validation PASSED against control
```

# Utilities that come with Cara


These utilities allow an examination of the lexer and the parser outputs; cara.lexer and cara.parser respectively are shown below:

```
./cara.lexer ./sortingfloor.hrm              
[cursor,(row,col)] type                      
[     0,(  0,  0)] type =              COMMENT, value = -- HUMAN RESOURCE MACHINE PROGRAM --
[    38,(  2,  1)] type =    LABEL_DECLARATION, value = a
[    45,(  3,  5)] type =             MNEMONIC, value = COPYFROM
[    54,(  3, 14)] type =              ADDRESS, value = 24
[    61,(  4,  5)] type =             MNEMONIC, value = COPYTO
[    70,(  4, 14)] type =              ADDRESS, value = 20
[    77,(  5,  5)] type =             MNEMONIC, value = COPYTO
[    86,(  5, 14)] type =              ADDRESS, value = 21
[    93,(  6,  5)] type =             MNEMONIC, value = COPYTO
[   102,(  6, 14)] type =              ADDRESS, value = 22
[   109,(  7,  5)] type =             MNEMONIC, value = COPYTO
[   118,(  7, 14)] type =              ADDRESS, value = 19
[   121,(  8,  1)] type =    LABEL_DECLARATION, value = b
[   128,(  9,  5)] type =             MNEMONIC, value = INBOX
[   141,( 10,  5)] type =             MNEMONIC, value = COPYTO
[   150,( 10, 14)] type =             LBRACKET, value = [
[   151,( 10, 15)] type =              ADDRESS, value = 20
[   153,( 10, 17)] type =             RBRACKET, value = ]
[   159,( 11,  5)] type =             MNEMONIC, value = JUMPZ
[   168,( 11, 14)] type =           LABEL_NAME, value = c
[   174,( 12,  5)] type =             MNEMONIC, value = BUMPUP
[   183,( 12, 14)] type =              ADDRESS, value = 20
[   190,( 13,  5)] type =             MNEMONIC, value = JUMP
[   199,( 13, 14)] type =           LABEL_NAME, value = b
[   201,( 14,  1)] type =    LABEL_DECLARATION, value = c
[   208,( 15,  5)] type =             MNEMONIC, value = JUMP
[   217,( 15, 14)] type =           LABEL_NAME, value = h
[   223,( 16,  5)] type =      KEYWORD_COMMENT, value = COMMENT
[   232,( 16, 14)] type =              ADDRESS, value = 0
[   234,( 17,  1)] type =    LABEL_DECLARATION, value = d
[   241,( 18,  5)] type =             MNEMONIC, value = COPYFROM
[   250,( 18, 14)] type =             LBRACKET, value = [
[   251,( 18, 15)] type =              ADDRESS, value = 21
[   253,( 18, 17)] type =             RBRACKET, value = ]
[   259,( 19,  5)] type =             MNEMONIC, value = COPYTO
[   268,( 19, 14)] type =              ADDRESS, value = 23
[   275,( 20,  5)] type =             MNEMONIC, value = COPYFROM
[   284,( 20, 14)] type =             LBRACKET, value = [
[   285,( 20, 15)] type =              ADDRESS, value = 19
[   287,( 20, 17)] type =             RBRACKET, value = ]
[   293,( 21,  5)] type =             MNEMONIC, value = COPYTO
[   302,( 21, 14)] type =             LBRACKET, value = [
[   303,( 21, 15)] type =              ADDRESS, value = 21
[   305,( 21, 17)] type =             RBRACKET, value = ]
[   311,( 22,  5)] type =             MNEMONIC, value = COPYFROM
[   320,( 22, 14)] type =              ADDRESS, value = 23
[   327,( 23,  5)] type =             MNEMONIC, value = COPYTO
[   336,( 23, 14)] type =             LBRACKET, value = [
[   337,( 23, 15)] type =              ADDRESS, value = 19
[   339,( 23, 17)] type =             RBRACKET, value = ]
[   345,( 24,  5)] type =             MNEMONIC, value = JUMP
[   354,( 24, 14)] type =           LABEL_NAME, value = l
[   360,( 25,  5)] type =      KEYWORD_COMMENT, value = COMMENT
[   369,( 25, 14)] type =              ADDRESS, value = 1
[   375,( 26,  5)] type =      KEYWORD_COMMENT, value = COMMENT
[   384,( 26, 14)] type =              ADDRESS, value = 2
[   386,( 27,  1)] type =    LABEL_DECLARATION, value = e
[   389,( 28,  1)] type =    LABEL_DECLARATION, value = f
[   396,( 29,  5)] type =             MNEMONIC, value = COPYFROM
[   405,( 29, 14)] type =             LBRACKET, value = [
[   406,( 29, 15)] type =              ADDRESS, value = 19
[   408,( 29, 17)] type =             RBRACKET, value = ]
[   414,( 30,  5)] type =             MNEMONIC, value = SUB
[   423,( 30, 14)] type =             LBRACKET, value = [
[   424,( 30, 15)] type =              ADDRESS, value = 22
[   426,( 30, 17)] type =             RBRACKET, value = ]
[   432,( 31,  5)] type =             MNEMONIC, value = JUMPN
[   441,( 31, 14)] type =           LABEL_NAME, value = g
[   447,( 32,  5)] type =             MNEMONIC, value = COPYFROM
[   456,( 32, 14)] type =              ADDRESS, value = 22
[   463,( 33,  5)] type =             MNEMONIC, value = COPYTO
[   472,( 33, 14)] type =              ADDRESS, value = 19
[   475,( 34,  1)] type =    LABEL_DECLARATION, value = g
[   482,( 35,  5)] type =             MNEMONIC, value = BUMPUP
[   491,( 35, 14)] type =              ADDRESS, value = 22
[   498,( 36,  5)] type =             MNEMONIC, value = SUB
[   507,( 36, 14)] type =              ADDRESS, value = 20
[   514,( 37,  5)] type =             MNEMONIC, value = JUMPN
[   523,( 37, 14)] type =           LABEL_NAME, value = f
[   529,( 38,  5)] type =             MNEMONIC, value = JUMP
[   538,( 38, 14)] type =           LABEL_NAME, value = d
[   544,( 39,  5)] type =      KEYWORD_COMMENT, value = COMMENT
[   553,( 39, 14)] type =              ADDRESS, value = 3
[   559,( 40,  5)] type =      KEYWORD_COMMENT, value = COMMENT
[   568,( 40, 14)] type =              ADDRESS, value = 4
[   570,( 41,  1)] type =    LABEL_DECLARATION, value = h
[   573,( 42,  1)] type =    LABEL_DECLARATION, value = i
[   580,( 43,  5)] type =             MNEMONIC, value = BUMPUP
[   589,( 43, 14)] type =              ADDRESS, value = 22
[   596,( 44,  5)] type =             MNEMONIC, value = SUB
[   605,( 44, 14)] type =              ADDRESS, value = 20
[   612,( 45,  5)] type =             MNEMONIC, value = JUMPN
[   621,( 45, 14)] type =           LABEL_NAME, value = j
[   627,( 46,  5)] type =             MNEMONIC, value = JUMP
[   636,( 46, 14)] type =           LABEL_NAME, value = k
[   638,( 47,  1)] type =    LABEL_DECLARATION, value = j
[   645,( 48,  5)] type =             MNEMONIC, value = JUMP
[   654,( 48, 14)] type =           LABEL_NAME, value = e
[   656,( 49,  1)] type =    LABEL_DECLARATION, value = k
[   659,( 50,  1)] type =    LABEL_DECLARATION, value = l
[   666,( 51,  5)] type =             MNEMONIC, value = BUMPUP
[   675,( 51, 14)] type =              ADDRESS, value = 21
[   682,( 52,  5)] type =             MNEMONIC, value = SUB
[   691,( 52, 14)] type =              ADDRESS, value = 20
[   698,( 53,  5)] type =             MNEMONIC, value = JUMPN
[   707,( 53, 14)] type =           LABEL_NAME, value = m
[   713,( 54,  5)] type =             MNEMONIC, value = JUMP
[   722,( 54, 14)] type =           LABEL_NAME, value = n
[   724,( 55,  1)] type =    LABEL_DECLARATION, value = m
[   731,( 56,  5)] type =             MNEMONIC, value = COPYFROM
[   740,( 56, 14)] type =              ADDRESS, value = 21
[   747,( 57,  5)] type =             MNEMONIC, value = COPYTO
[   756,( 57, 14)] type =              ADDRESS, value = 19
[   763,( 58,  5)] type =             MNEMONIC, value = COPYTO
[   772,( 58, 14)] type =              ADDRESS, value = 22
[   779,( 59,  5)] type =             MNEMONIC, value = JUMP
[   788,( 59, 14)] type =           LABEL_NAME, value = i
[   794,( 60,  5)] type =      KEYWORD_COMMENT, value = COMMENT
[   803,( 60, 14)] type =              ADDRESS, value = 5
[   809,( 61,  5)] type =      KEYWORD_COMMENT, value = COMMENT
[   818,( 61, 14)] type =              ADDRESS, value = 6
[   820,( 62,  1)] type =    LABEL_DECLARATION, value = n
[   827,( 63,  5)] type =             MNEMONIC, value = COPYFROM
[   836,( 63, 14)] type =              ADDRESS, value = 24
[   843,( 64,  5)] type =             MNEMONIC, value = COPYTO
[   852,( 64, 14)] type =              ADDRESS, value = 21
[   855,( 65,  1)] type =    LABEL_DECLARATION, value = o
[   862,( 66,  5)] type =             MNEMONIC, value = COPYFROM
[   871,( 66, 14)] type =             LBRACKET, value = [
[   872,( 66, 15)] type =              ADDRESS, value = 21
[   874,( 66, 17)] type =             RBRACKET, value = ]
[   880,( 67,  5)] type =             MNEMONIC, value = JUMPZ
[   889,( 67, 14)] type =           LABEL_NAME, value = a
[   895,( 68,  5)] type =             MNEMONIC, value = OUTBOX
[   908,( 69,  5)] type =             MNEMONIC, value = BUMPUP
[   917,( 69, 14)] type =              ADDRESS, value = 21
[   924,( 70,  5)] type =             MNEMONIC, value = JUMP
[   933,( 70, 14)] type =           LABEL_NAME, value = o
[   937,( 73,  1)] type =     BEGIN_DEFINITION, value = DEFINE
[   944,( 73,  8)] type =      KEYWORD_COMMENT, value = COMMENT
[   952,( 73, 16)] type =              ADDRESS, value = 0
[   954,( 74,  1)] type =              RAWBLOB, value = eJybycDAoC [... omitted]
[  1790,( 86,  1)] type =     BEGIN_DEFINITION, value = DEFINE
[  1797,( 86,  8)] type =      KEYWORD_COMMENT, value = COMMENT
[  1805,( 86, 16)] type =              ADDRESS, value = 1
[  1807,( 87,  1)] type =              RAWBLOB, value = eJzLYWBgMJ [... omitted]
[  2404,( 96,  1)] type =     BEGIN_DEFINITION, value = DEFINE
[  2411,( 96,  8)] type =      KEYWORD_COMMENT, value = COMMENT
[  2419,( 96, 16)] type =              ADDRESS, value = 2
[  2421,( 97,  1)] type =              RAWBLOB, value = eJyrZWBgmC [... omitted]
[  3119,(107,  1)] type =     BEGIN_DEFINITION, value = DEFINE
[  3126,(107,  8)] type =      KEYWORD_COMMENT, value = COMMENT
[  3134,(107, 16)] type =              ADDRESS, value = 3
[  3136,(108,  1)] type =              RAWBLOB, value = eJyLZWBguC [... omitted]
[  3663,(116,  1)] type =     BEGIN_DEFINITION, value = DEFINE
[  3670,(116,  8)] type =      KEYWORD_COMMENT, value = COMMENT
[  3678,(116, 16)] type =              ADDRESS, value = 4
[  3680,(117,  1)] type =              RAWBLOB, value = eJxrYWBg+K [... omitted]
[  4406,(127,  1)] type =     BEGIN_DEFINITION, value = DEFINE
[  4413,(127,  8)] type =      KEYWORD_COMMENT, value = COMMENT
[  4421,(127, 16)] type =              ADDRESS, value = 5
[  4423,(128,  1)] type =              RAWBLOB, value = eJyLY2BgmK [... omitted]
[  4948,(136,  1)] type =     BEGIN_DEFINITION, value = DEFINE
[  4955,(136,  8)] type =      KEYWORD_COMMENT, value = COMMENT
[  4963,(136, 16)] type =              ADDRESS, value = 6
[  4965,(137,  1)] type =              RAWBLOB, value = eJwLYWBg2G [... omitted]
[  5443,(144,  1)] type =     BEGIN_DEFINITION, value = DEFINE
[  5450,(144,  8)] type =        KEYWORD_LABEL, value = LABEL
[  5456,(144, 14)] type =              ADDRESS, value = 19
[  5459,(145,  1)] type =              RAWBLOB, value = eJxTZWBgUA [... omitted]
[  5682,(149,  1)] type =     BEGIN_DEFINITION, value = DEFINE
[  5689,(149,  8)] type =        KEYWORD_LABEL, value = LABEL
[  5695,(149, 14)] type =              ADDRESS, value = 20
[  5698,(150,  1)] type =              RAWBLOB, value = eJxzYmBgeB [... omitted]
[  6081,(156,  1)] type =     BEGIN_DEFINITION, value = DEFINE
[  6088,(156,  8)] type =        KEYWORD_LABEL, value = LABEL
[  6094,(156, 14)] type =              ADDRESS, value = 21
[  6097,(157,  1)] type =              RAWBLOB, value = eJzjZmBg+F [... omitted]
[  6325,(161,  1)] type =     BEGIN_DEFINITION, value = DEFINE
[  6332,(161,  8)] type =        KEYWORD_LABEL, value = LABEL
[  6338,(161, 14)] type =              ADDRESS, value = 22
[  6341,(162,  1)] type =              RAWBLOB, value = eJwTZGBgEK [... omitted]
[  6469,(165,  1)] type =     BEGIN_DEFINITION, value = DEFINE
[  6476,(165,  8)] type =        KEYWORD_LABEL, value = LABEL
[  6482,(165, 14)] type =              ADDRESS, value = 23
[  6485,(166,  1)] type =              RAWBLOB, value = eJzzYmBgeO [... omitted]
[  6919,(173,  1)] type =     BEGIN_DEFINITION, value = DEFINE
[  6926,(173,  8)] type =        KEYWORD_LABEL, value = LABEL
[  6932,(173, 14)] type =              ADDRESS, value = 24
[  6935,(174,  1)] type =              RAWBLOB, value = eJyzZWBgYL [... omitted]
total tokens: 190
lexer time : 2.963185e-03 s
```



```
./cara.parser ./sortingfloor.hrm      
program listing:
[  0]       a RESERVED 0
[  0]         COPYFROM 24
[  1]         COPYTO 20
[  2]         COPYTO 21
[  3]         COPYTO 22
[  4]         COPYTO 19
[  5]       b RESERVED 5
[  5]         INBOX
[  6]         COPYTOI 20
[  7]         JUMPZ 10
[  8]         BUMPUP 20
[  9]         JUMP 5
[ 10]       c RESERVED 10
[ 10]         JUMP 27
[ 11]       d RESERVED 11
[ 11]         COPYFROMI 21
[ 12]         COPYTO 23
[ 13]         COPYFROMI 19
[ 14]         COPYTOI 21
[ 15]         COPYFROM 23
[ 16]         COPYTOI 19
[ 17]         JUMP 32
[ 18]       e RESERVED 18
[ 18]       f RESERVED 18
[ 18]         COPYFROMI 19
[ 19]         SUBI 22
[ 20]         JUMPN 23
[ 21]         COPYFROM 22
[ 22]         COPYTO 19
[ 23]       g RESERVED 23
[ 23]         BUMPUP 22
[ 24]         SUB 20
[ 25]         JUMPN 18
[ 26]         JUMP 11
[ 27]       h RESERVED 27
[ 27]       i RESERVED 27
[ 27]         BUMPUP 22
[ 28]         SUB 20
[ 29]         JUMPN 31
[ 30]         JUMP 32
[ 31]       j RESERVED 31
[ 31]         JUMP 18
[ 32]       k RESERVED 32
[ 32]       l RESERVED 32
[ 32]         BUMPUP 21
[ 33]         SUB 20
[ 34]         JUMPN 36
[ 35]         JUMP 40
[ 36]       m RESERVED 36
[ 36]         COPYFROM 21
[ 37]         COPYTO 19
[ 38]         COPYTO 22
[ 39]         JUMP 27
[ 40]       n RESERVED 40
[ 40]         COPYFROM 24
[ 41]         COPYTO 21
[ 42]       o RESERVED 42
[ 42]         COPYFROMI 21
[ 43]         JUMPZ 0
[ 44]         OUTBOX
[ 45]         BUMPUP 21
[ 46]         JUMP 42


label : address
    a :       0
    b :       5
    c :      10
    d :      11
    e :      18
    f :      18
    g :      23
    h :      27
    i :      27
    j :      31
    k :      32
    l :      32
    m :      36
    n :      40
    o :      42


The program is valid (parsing succeeded)
parser time : 3.369630e-03 s
```

# Useful links

## Solutions

https://atesgoral.github.io/hrm-solutions/ \
https://github.com/atesgoral/hrm-solutions \
https://github.com/arturoherrero/human-resource-machine \
https://gitlab.com/PetulantWeeb/human-resource-machine-solutions


# End







(c) 2021 [DeepNet42](https://www.deepnet42.com "DeepNet42"). All Rights Reserved.

Updated Version.

https://www.deepnet42.com \
https://www.deepnet42.com/projects/#cara-human-resource-machine \
https://gitlab.com/deepnet42/cara/-/wikis/home

```
______  _______ _______  _____  __   _ _______ _______     
|     \ |______ |______ |_____] | \  | |______    |        
|_____/ |______ |______ |       |  \_| |______    |        
                                                           
Deepnet42
Deepnet42.com
```
