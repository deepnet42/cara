# BUILD INSTRUCTIONS
Build instructions for *Nix and Windows systems follow below. 


### Steps
## 0. Acquiring the Source Code:

Use your favorite method to acquire the source code for Cara.


## *nix Based System Instructions (Linux)


### Prerequistes:


- CMake
- Make
- g++ (c++20)




1. **CREATE BUILD DIRECTORIES**

Open a terminal and navigate to the directory containing the source code.  Then, run the following commands to create separate directories for **Debug** and **Release** builds:

```
cd cara
mkdir Debug Release
```

2. **CREATE A DEBUG BUILD TARGET**

Navigate to the Debug directory and build the **Debug** target:

```
cd Debug
cmake -DCMAKE_BUILD_TYPE=Debug ..
make
```

This will configure the build for debugging and then compile the project. The compiled executable will be located in the ./cara/Debug/build/ directory.

>If you have trouble compiling switch target to C++20 instead of C++23. You can also use earlier versions of CMake.
<br />`set(CMAKE_CXX_STANDARD 20)` in CMakeLists.txt

3. **CREATE A REALEASE BUILD TARGET**

To create a release build follow the same steps as for the debug target, but navigate to the Release directory instead of Debug:

```
cd Release
cmake -DCMAKE_BUILD_TYPE=Release ..
make
```

This will configure the build for release and then compile the project. The compiled executable will be located in the ./cara/Release/build/ directory.


4. **DONE**

The following executables should be present in the build dirtectory for both **Debug** and **Release** targets:


- cara
- cara.lexer
- cara.parser
- carac
- cara.validator
- cara.bytecode.compiler

The test files are located in `./cara/<BUILD_TYPE>/build/tests/`


<br>

**RUNNING THE TESTS**

Run the `ctest` in `./cara/<BUILD_TYPE>/` and not in `./cara/<BUILD_TYPE>/build/`

This concludes the *Nix build instructions for both **Debug** and **Release** builds.

<br>

## Windows Build Instructions

To build Cara on Windows you must use Microsoft Visual Studio 2019 or later. You can also use Microsoft Visual Studio Community Edition.


```
1. Launch Microsoft Visual Studio.
2. Clone the repository
	1. Repository location: https://gitlab.com/deepnet42/cara.git
	2. Select a project path.
	3. Click clone.
3. Build the project via Build -> Build All
```


You can try launching `cara.validator` from the Visual Studio GUI to see if everything is in order which should run basic tests.

Once you have built the project look into the `out` folder.

All build artifacts can be found in: 

`<REPO_DIR>/out/build/x64-Debug/build`

Open a terminal and navigate to the directory above replacing <REPO_DIR> with the actual directory location.



<div align="center">

![Human Resource Machine - Level 31](./docu/windows-cara-validator.png)

</div> 


(c) 2021-2023 DeepNet42. All Rights Reserved.

Updated Version.
```
______  _______ _______  _____  __   _ _______ _______     
|     \ |______ |______ |_____] | \  | |______    |        
|_____/ |______ |______ |       |  \_| |______    |        
                                                           
Deepnet42
Deepnet42.com
```