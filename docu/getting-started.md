# Getting Started with Cara (HRMVM)

## Introduction
This is a basic (read beginner) guide for getting started with the Cara Human Resource Machine Virtual Machine and its compiler suite. Cara (HRMVM) runs HRM programs from source, custom bytecode, or generates native code. Let us do a short walkthrough. 


Suppose we would like to run **Level 31**, String Reverse, below in Cara:

<div align="center">

![Human Resource Machine - Level 31](human-resource-machine-level31.png)

</div> 

## Creating an .hrm source file

We would first want to copy this program or recreate it in plain text (ASCII). So, we create a new file, paste the contents, and save it. Below are the contents of level31.hrm. Here we are using an .hrm extension to mean human resource machine program, but you can use your own such as .txt or none. I personally prefer .hrm extension since we do have bytecode files, and this helps to distinguish the two.

 
```
$>cat level31.hrm  
-- HUMAN RESOURCE MACHINE PROGRAM -- 

a: 
b: 
   INBOX    
   JUMPZ    c 
   COPYTO   [14] 
   BUMPUP   14 
   JUMP     b 
c: 
d: 
   BUMPDN   14 
   COPYFROM [14] 
   OUTBOX   
   COPYFROM 14 
   JUMPZ    a 
   JUMP     d 


DEFINE LABEL 14 
eJzzZWBgmBdWmhYQ8D5Rzvt94iq3kNTzLrcypzodLbzuGFEf7Hyy29vtx9Rab7UFfIHvl7GFCK64EH1u 
aXem5HSgVgbH3KOF3wtOdhsWT56UXGE940ut9QzOpmtTFrUETEhoXdGV0FrVcLypKb+3YXr6rmr3mOSK 
RxEiRWrRlgVdcf9zS9MW5Nrn7s1rygeZ1dXLEuXRx1GiPOF1z/yJWyZPnzJ/tsas+bOZ5sTMLFsQMGHD 
otQOnSUR9d1LvMvyFmfG8i8+m/B/xesekF7NjV1xc7cytO7a9nhu9La2zuvbI+pX7UnISzgwPf3noZDU 
K0eOFq4/1ta5/ljAhK4TLPNBeh6e3msrfU4w+dv5yZO2XHg81+CS4ArfyyGrXK8Zrey5/3ju3ieFE6uf 
tnWC1P48lJD3++afLMOXIakMo2AUDCMAAFcQlJA;
```


## Creating an .inbox file (input file)

Next, we would need to create an `INBOX` file. This is the input to our program. Basically this is the input belt in the game.

```
$>cat level31.inbox  
T I M E 0 W A I T S 0 F O R 0 N O 0 O N E 0
```

## Creating a .mem file (initial memory configuration aka tiles of the machine)

Next, we would need to create another file representing the initial memory of our machine. We can do it two ways: we can create a file like this: 

```
$>cat level31.mem 
. . . . . 
. . . . . 
. . . . 0
```

Or

```
$>cat level31_.mem 
[14]=0
```

The second way is more readable and should be a preferred method. The first method, however, is used by native programs. We will cover this later.


## Running the program

Now we can finally run our program like this

```
$>./cara leve31.hrm level31.inbox level31.mem level31.control 
69(E) 77(M) 73(I) 84(T) 83(S) 84(T) 73(I) 65(A) 87(W) 82(R) 79(O) 70(F) 79(O) 78(N) 69(E) 78(N) 79(O) 

```

Pretty neat. No? There are many flags that can be useful in debugging and running the program. See all of them by invoking `./cara` with no command line arguments.

## Creating a control (validation file)

Another useful feature is verifying the output of a program with a known good output. For example, suppose we would like to automatically verify the output of the above program with a known good output. We can do that by providing a control file.


```
$>cat level31.control  
E M I T S T I A W R O F O N E N O

$>./cara level31.hrm level31.inbox level31.mem level31.control 
69(E) 77(M) 73(I) 84(T) 83(S) 84(T) 73(I) 65(A) 87(W) 82(R) 79(O) 70(F) 79(O) 78(N) 69(E) 78(N) 79(O)  
[info    ]: Output validation PASSED against control
```


That covers the basic usage. 

## Running a bytecode version of an .hrm source

Suppose now, you wanted to run a bytecode version of the above program. Imagine that we somehow have this program in a bytecode format level31.hrmb (b for bytecode). Here is a sample hexdump.

```
$>hexdump -C level31.hrmb 
00000000  48 52 4d 42 00 40 00 f0  05 88 0e c0 0e e0 00 d0  |HRMB.@..........| 
00000010  0e 98 0e 48 00 90 0e f0  00 e0 05                 |...H.......| 
0000001b
```

This bytecode is unique to Cara VM. [Read the specification here](bytecode.md).


```
./cara --bytecode level31.hrmb level31.inbox level31.mem level
31.control 
69(E) 77(M) 73(I) 84(T) 83(S) 84(T) 73(I) 65(A) 87(W) 82(R) 79(O) 70(F) 79(O) 78(N) 69(E) 78(N) 79(O)  
[info    ]: Output validation PASSED against control
```


## Creating the bytecode version of the .hrm source

So how do we actually make our own bytecode program? Luckily for us, we have Cara’s bytecode compiler. Here is how to use it:

```
$>./cara.bytecode.compiler level31.hrm level31.hrmb             
[info    ]: good header 
L. binary (big en.) |         MNEMONIC | OP | DATA |  CARA VM TRANSLATION 
 0 0100000000000000 |            INBOX |  8 |    0 |                INBOX 
 1 1111000000000101 |            JUMPZ | 30 |    5 |                JUMPZ 5 
 2 1000100000001110 |         COPYTO_I | 17 |   14 |              COPYTOI 14 
 3 1100000000001110 |         BUMPUP_V | 24 |   14 |               BUMPUP 14 
 4 1110000000000000 |             JUMP | 28 |    0 |                 JUMP 0 
 5 1101000000001110 |         BUMPDN_V | 26 |   14 |             BUMPDOWN 14 
 6 1001100000001110 |       COPYFROM_I | 19 |   14 |            COPYFROMI 14 
 7 0100100000000000 |           OUTBOX |  9 |    0 |               OUTBOX 
 8 1001000000001110 |       COPYFROM_V | 18 |   14 |             COPYFROM 14 
 9 1111000000000000 |            JUMPZ | 30 |    0 |                JUMPZ 0 
10 1110000000000101 |             JUMP | 28 |    5 |                 JUMP 5
```

And the rest we know.


## Transpiling (source-to-source) 
Now for kicks how about we turn the above code to native code. The way we would do this is use a source-to-source compiler provided with the Cara VM. This tool is called, wait for it, carac (cara compiler):

```
./carac level31.hrm > level31.cpp 
WARNING NEVER EVER EVER RUN UNTRUSTED CODE FROM THE NET IN THIS MODE 
WARNING NEVER EVER EVER RUN UNTRUSTED CODE FROM THE NET IN THIS MODE 
WARNING NEVER EVER EVER RUN UNTRUSTED CODE FROM THE NET IN THIS MODE 


YES THIS IS MEANT TO LOOK SCARY BECAUSE YOU WILL GENERATE NATIVE CODE 
AND IT IS RISKY! 
THE MESSAGE BELOW IS MEANT TO BE ANNOYING! 
type [yes, I know what I am doing.] if you want to execute! : yes, I know what I am doing. 
YOU HAVE BEEN WARNED!
```

Yes the scary warning but you are going to run your own programs right?

Now we have a C++ file which we can compile and run like this:

```
$>g++ level31.cpp -o level31
$>./level31 level31.inbox level31.mem 
WARNING NEVER EVER EVER RUN UNTRUSTED CODE FRON THE NET IN THIS MODE 
WARNING NEVER EVER EVER RUN UNTRUSTED CODE FRON THE NET IN THIS MODE 
WARNING NEVER EVER EVER RUN UNTRUSTED CODE FRON THE NET IN THIS MODE 
YOU HAVE BEEN WARNED! 
done. 
69(E) 77(M) 73(I) 84(T) 83(S) 84(T) 73(I) 65(A) 87(W) 82(R) 79(O) 70(F) 79(O) 78(N) 69(E) 78(N) 79(O)
```

Perfect! So we can run directly from source, bytecode, or even compile natively. Here is the translated snippet on what level31.cpp looks like:
 
```C++
a: 
b: 
if(inp>=inbox.size()) {std::cout << "done.\n"; goto end;} reg = inbox[inp++]; 
if (reg==0) goto d; 
mem[mem[14]] = reg; 
mem[14]++; reg = mem[14]; 
goto b; 
c: 
d: 
mem[14]--; reg = mem[14]; 
reg = mem[mem[14]]; 
outbox.push_back(reg); 
reg = mem[14]; 
if (reg==0) goto b; 
goto d;
```
It maps cleanly to our original source:

```
a: 
b: 
   INBOX    
   JUMPZ    c 
   COPYTO   [14] 
   BUMPUP   14 
   JUMP     b 
c: 
d: 
   BUMPDN   14 
   COPYFROM [14] 
   OUTBOX   
   COPYFROM 14 
   JUMPZ    a 
   JUMP     d
```

## Trying some other code from the interwebs

There is a nice project here:
https://atesgoral.github.io/hrm-solutions/

Let us run the same program for level31 and see what happens. We will run the direct source.


https://github.com/atesgoral/hrm-solutions/blob/master/solutions/31-String-Reverse-11.122/112.58.unroll-viamodulo.asm
by viamodulo 

```
time ./cara level31_interwebs.hrm level31.inbox level31.mem level31.control
69(E) 77(M) 73(I) 84(T) 83(S) 84(T) 73(I) 65(A) 87(W) 82(R) 79(O) 70(F) 79(O) 78(N) 69(E) 78(N) 79(O) 
[info    ]: Output validation PASSED against control
```

## Final words

I hope this guide helped you to get started with running your own HRM programs.

Remember to have fun. 



## End


(c) 2022 DeepNet42. All Rights Reserved.

Updated Version.
```
______  _______ _______  _____  __   _ _______ _______     
|     \ |______ |______ |_____] | \  | |______    |        
|_____/ |______ |______ |       |  \_| |______    |        
                                                           
Deepnet42
Deepnet42.com
```
