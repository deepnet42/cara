# Bytecode Design Specification for the Human Resource Machine

```
HRM BYTECODE Version 0 (initial version)
Full support for all features of the HRM game.
Cara VM implements this full feature set of the specification
```
 

## Bytecode

```
Bytecode format
Each instruction is 16 bits long. (2 bytes)


OpCode is  5 bits
Data   is 11 bits
```

Instructions, which are stored on a disk, are to be in [network byte order](https://www.ibm.com/docs/en/zos/2.4.0?topic=hosts-network-byte-order) (big-endian).

## Diagram

```
OPCODE is stored in bits [15,11]
DATA   is stored in bits [10, 0]
```

```
MSB                                         LSB
15 14 13 12 11 10 09 08 07 06 05 04 03 02 01 00
 O  P  -  -  -
                D  A  T  A  -  -  -  -  -  -  -

```

example:

```
    OP--- DATA-------    MNEMONIC
bin 11100 00000100000 -> JUMP    
dec 28    32
```


Below is a table for the HRM Bytecode used by the Cara VM/interpreter.

## The Human Resource Machine Bytecode table


| OP CODE (BIN) 5bits | OP CODE (DEC) |         MNEMONIC |
| ------------------- | ------------- | -----------------|
|      00000 |          0 |         RESERVED|
|      00001 |          1 |         RESERVED|
|      00010 |          2 |         RESERVED|
|      00011 |          3 |         RESERVED|
|      00100 |          4 |         RESERVED|
|      00101 |          5 |         RESERVED|
|      00110 |          6 |         RESERVED|
|      00111 |          7 |         RESERVED|
|      01000 |          8 |            INBOX|
|      01001 |          9 |           OUTBOX|
|      01010 |         10 |         RESERVED|
|      01011 |         11 |         RESERVED|
|      01100 |         12 |         RESERVED|
|      01101 |         13 |         RESERVED|
|      01110 |         14 |         RESERVED|
|      01111 |         15 |         RESERVED|
|      10000 |         16 |         COPYTO_V|
|      10001 |         17 |         COPYTO_I|
|      10010 |         18 |       COPYFROM_V|
|      10011 |         19 |       COPYFROM_I|
|      10100 |         20 |            ADD_V|
|      10101 |         21 |            ADD_I|
|      10110 |         22 |            SUB_V|
|      10111 |         23 |            SUB_I|
|      11000 |         24 |         BUMPUP_V|
|      11001 |         25 |         BUMPUP_I|
|      11010 |         26 |         BUMPDN_V|
|      11011 |         27 |         BUMPDN_I|
|      11100 |         28 |             JUMP|
|      11101 |         29 |            JUMPN|
|      11110 |         30 |            JUMPZ|




# File format for an .hrmb binary file

The file starts with a 5 Byte header.

```
Byte 0 : H
Byte 1 : R
Byte 2 : M
Byte 3 : B
Byte 4 : 0
```

The first group of (4) bytes is the magic, which identifies the file as a bytecode file.\
The fifth (5th) byte is the version number of the bytecode. The version of the bytecode is zero (0).\
The rest that follow are the HRM bytecode instruction encoded in network byte order (2 bytes per instruction). 


# End

(c) 2022 DeepNet42. All Rights Reserved.

Updated Version.
```
______  _______ _______  _____  __   _ _______ _______     
|     \ |______ |______ |_____] | \  | |______    |        
|_____/ |______ |______ |       |  \_| |______    |        
                                                           
Deepnet42
Deepnet42.com
```