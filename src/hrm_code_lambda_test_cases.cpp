/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

std::map<std::string, std::function<bool(const std::vector<int>& in,const std::vector<int>& otheroutbox)> > TEST_CASES;


auto sorting = [](const std::vector<int>& in,const std::vector<int>& otheroutbox) -> bool {
    std::vector<int> out;
    std::vector<int> partial;
    for(auto e: in) {
        
        if(e==0) {
            std::sort(partial.begin(), partial.end());
            out.insert(out.end(), partial.begin(), partial.end());
            partial.clear();
        } else {
            partial.push_back(e);
        }
        
    }
    return out==otheroutbox;
};


using INBOX = std::vector<int>;
using OUTBOX = std::vector<int>;


auto mailroom = [](const std::vector<int>& in,const std::vector<int>& otheroutbox) -> bool {
    return in==otheroutbox;
};

auto busymailroom = [](const std::vector<int>& in,const std::vector<int>& otheroutbox) -> bool {
    return in==otheroutbox;
};


auto copyfloor = [](const std::vector<int>& in,[[maybe_unused]] const std::vector<int>& otheroutbox) -> bool {
    return in==std::vector<int>{int('B'),int('U'),int('G')};
};

auto scramblerhandler = [](const std::vector<int>& in,const std::vector<int>& otheroutbox) -> bool {
	if(in.size()%2!=0)
		throw std::runtime_error("inbox has an odd size. Solution requires even size.");
	auto out = in;
	
	for(std::size_t i = 0; i < in.size(); i+=2) {
		std::swap(out[i], out[i+1]);
	}

	return out==otheroutbox;
};


auto rainysummer = [](const std::vector<int>& in,const std::vector<int>& otheroutbox) -> bool {
	if(in.size()%2!=0)
		throw std::runtime_error("inbox has an odd size. Solution requires even size.");
	
	OUTBOX out;
	for(std::size_t i = 0; i < in.size(); i+=2) {
		out.push_back(in[i]+in[i+1]);
	}
	return out==otheroutbox;
};

auto zeroexterminator = [](const std::vector<int>& in,const std::vector<int>& otheroutbox) -> bool {
	OUTBOX out; 
	std::copy_if(	in.begin(), 
					in.end(), 
					std::back_inserter(out), [](int i){return i!=0;} 
				);
	return out==otheroutbox;
};

auto triplerroom = [](const std::vector<int>& in,const std::vector<int>& otheroutbox) -> bool {
	auto out = in; 
	std::transform(out.begin(), out.end(), out.begin(), [](int n) {return n*3;});
	return out==otheroutbox;
};

auto zeropreservationinitiative = [](const std::vector<int>& in,const std::vector<int>& otheroutbox) -> bool {
	OUTBOX out;
	std::copy_if(	in.begin(), 
					in.end(), 
					std::back_inserter(out), [](int i){return i==0;} 
				);
	return out==otheroutbox;
};


auto octopliersuite = [](const std::vector<int>& in,const std::vector<int>& otheroutbox) -> bool {
	auto out = in;
	std::transform(out.begin(), out.end(), out.begin(), [](int n) {return n*8;});
	return out==otheroutbox;
};


auto subhallway = [](const std::vector<int>& in,const std::vector<int>& otheroutbox) -> bool {
	if(in.size()%2!=0)
		throw std::runtime_error("inbox has an odd size. Solution requires even size.");
	OUTBOX out;

	for(std::size_t i = 0; i < in.size(); i+=2) {
		out.push_back(in[i+1]-in[i]);
        out.push_back(in[i]-in[i+1]);
	}

	return out==otheroutbox;
};

auto tetracontiplier = [](const std::vector<int>& in,const std::vector<int>& otheroutbox) -> bool {
	OUTBOX out = in;
	std::transform(out.begin(), out.end(), out.begin(), [](int n) { return n*40;});
	return out==otheroutbox;
};


auto eqaulizationroom = [](const std::vector<int>& in,const std::vector<int>& otheroutbox) -> bool {
	if(in.size()%2!=0)
		throw std::runtime_error("inbox has an odd size. Solution requires even size.");
	
	OUTBOX out;
	for(std::size_t i = 0; i < in.size(); i+=2) {
		if(in[i]==in[i+1]) out.push_back(in[i]);
	}
	return out==otheroutbox;
};


auto maximizationroom = [](const std::vector<int>& in,const std::vector<int>& otheroutbox) -> bool {
	if(in.size()%2!=0)
		throw std::runtime_error("inbox has an odd size. Solution requires even size.");
	
	OUTBOX out;

	for(std::size_t i = 0; i < in.size(); i+=2) {
		out.push_back(std::max(in[i],in[i+1]));
	}
	return out==otheroutbox;
};



auto absolutepositivity = [](const std::vector<int>& in,const std::vector<int>& otheroutbox) -> bool {
	auto out = in;
	std::transform(out.begin(), out.end(), out.begin(), [](int n) {return std::abs(n);});
	return out==otheroutbox;
};

auto exclusivelounge = [](const std::vector<int>& in,const std::vector<int>& otheroutbox) -> bool {
	if(in.size()%2!=0)
		throw std::runtime_error("inbox has an odd size. Solution requires even size.");
	
	OUTBOX out;


	auto sign = []<typename T>(T val) -> T { return (T(0) < val) - (val < T(0));};


	for(std::size_t i = 0; i < in.size(); i+=2) {
		out.push_back(sign(in[i])!=sign(in[i+1]));
	}

	return 	out==otheroutbox;
};


auto vowelincinerator = [](const std::vector<int>& in,const std::vector<int>& otheroutbox) -> bool {
    OUTBOX out;
    auto is_vowel = [](int i) -> bool {
        if(i=='A' or i=='E' or i=='I' or i=='O' or i=='U')
            return true;
        return false;
        };

    for(auto i : in) {
        if(!is_vowel(i)) {
            out.push_back(i);
        }
    }

    return out==otheroutbox;
};

auto duplicateremoval = [](const std::vector<int>& in,const std::vector<int>& otheroutbox) -> bool {
    OUTBOX out = in;
    OUTBOX otherbox = otheroutbox;

    sort( out.begin(), out.end() );
    out.erase( unique( out.begin(), out.end() ), out.end() );

    sort( otherbox.begin(), otherbox.end() );
    otherbox.erase( unique( otherbox.begin(), otherbox.end() ), otherbox.end() );

    return out==otherbox;

};

auto alphabetizer = [](const std::vector<int>& in,const std::vector<int>& otheroutbox) -> bool {
    
    int loc = 0;
    for(loc = 0; loc < std::ssize(in) and in[loc]!=0; loc++)
        ;

    loc++;
    int first_end = loc-1;

    int copy_word_start = 0;
    int copy_word_end = first_end;
    for(int i = 0; i < first_end and loc < std::ssize(in); i++,loc++) {
        if(in[i]>in[loc]) {
            copy_word_start = first_end+1;
            copy_word_end = in.size();
            break;
        } else if(in[i]<in[loc]) {
            break;
        }
    }

    std::vector<int> out(&in[copy_word_start],&in[copy_word_end]);
    

    return out==otheroutbox;
};

auto digitexploder = [](const std::vector<int>& in,const std::vector<int>& otheroutbox) -> bool {
    
   
    auto decompose = [](int n) -> std::vector<int> {
        std::stack<int> st;
        std::vector<int> out;

        if(n==0) {
            return {0};
        }
        while (n > 0) {
            int digit = n%10;
            n /= 10;
            st.push(digit);
        }

        while (!st.empty()) {
            int digit = st.top();
            out.push_back(digit);
            st.pop();
        }

        return out;
    };

    std::vector<int> out;
    for(auto n : in) {
        auto alldigits = decompose(n);  
        out.insert(std::end(out), std::begin(alldigits), std::end(alldigits));  
    }
    

    return out==otheroutbox;
};


auto recoordinator = [](const std::vector<int>& in,const std::vector<int>& otheroutbox) -> bool {
    
    std::vector<int> out;
    for(auto i : in) {
        int x = i % 4;
        int y = i / 4;
        out.push_back(x);
        out.push_back(y);
    }

    return out==otheroutbox;
};


auto countdown = [](const std::vector<int>& in,const std::vector<int>& otheroutbox) -> bool {
    

    OUTBOX out;
    
    auto dec = [&out](int n) { while(n>0) out.push_back(n--);};
    auto inc = [&out](int n) { while(n<0) out.push_back(n++);};

    for(auto n : in) {
        if(n>0)
            dec(n);
        else if (n<0)
            inc(n);
        out.push_back(0);
    }

    return out==otheroutbox;

};






auto cumulativecountdown = [](const std::vector<int>& in,const std::vector<int>& otheroutbox) -> bool {
    OUTBOX out;
    std::for_each(std::begin(in), std::end(in), [&out](int n) { out.push_back(n*(n+1)/2);});
    return out==otheroutbox;

};


auto threesort = [](const std::vector<int>& in,const std::vector<int>& otheroutbox) -> bool {
    

    OUTBOX out = in;
    if(out.size()%3!=0) {
        throw std::runtime_error("threesort input must be a multiple of three");
    }

	for (auto it = out.begin(); it != out.end(); it += 3) {
		std::sort(it, it + 3);
	}

    return out==otheroutbox;

};


auto zeroterminatedsum = [](const std::vector<int>& in,const std::vector<int>& otheroutbox) -> bool {
    

    OUTBOX out;
    int sum = 0;
    for(auto i : in) {
        if(i==0) {
            out.push_back(sum);
            sum = 0;
        }
        sum+=i;
    }

    return out==otheroutbox;

};

auto modmodule = [](const std::vector<int>& in,const std::vector<int>& otheroutbox) -> bool {
    
    if(in.size()%2!=0)
        throw std::runtime_error("input must be a multiple of two");

    OUTBOX out;
    for(std::size_t i = 0; i < in.size(); i+=2) {
        if(in[i+1]==0)
            throw std::runtime_error("can't divide by zero");
        out.push_back(in[i]%in[i+1]);
    }

    return out==otheroutbox;

};

auto thelittlestnumber = [](const std::vector<int>& in,const std::vector<int>& otheroutbox) -> bool {

    OUTBOX out;

    
    if(!in.empty()) {
        int min = in[0];
        for(auto i : in) {
            if(i==0) {
                out.push_back(min);
                min = INT_MAX;
            } else {
                min = min > i ? i : min; 
            }                
        }
    }

    return out == otheroutbox;
};



auto smalldivide = [](const std::vector<int>& in,const std::vector<int>& otheroutbox) -> bool {
    if(in.size()%2!=0)
        throw std::runtime_error("input must be a multiple of two");

    OUTBOX out;
    for(std::size_t i = 0; i < in.size(); i+=2) {
        if(in[i+1]==0)
            throw std::runtime_error("can't divide by zero");
        out.push_back(in[i]/in[i+1]);
    }

    return out==otheroutbox;
};

auto stringreverse = [](const std::vector<int>& in,const std::vector<int>& otheroutbox) -> bool {

    OUTBOX out;

    std::list<int> lst;
    for(auto i : in) {
        if(i==0) {
            std::copy(std::begin(lst), std::end(lst), std::back_inserter(out));
            lst.clear();
        } else {
            lst.push_front(i);
        }
    }

    return out==otheroutbox;
};


auto multiplicationworkshop = [](const std::vector<int>& in,const std::vector<int>& otheroutbox) -> bool {

    if(in.size()%2!=0)
        throw std::runtime_error("input must be a multiple of two");

    OUTBOX out;

    for(std::size_t i = 0; i < in.size(); i+=2) {
        out.push_back(in[i]*in[i+1]);
    }

    return out==otheroutbox;
};


auto fibonaccivisitor = [](const std::vector<int>& in,const std::vector<int>& otheroutbox) -> bool {

    OUTBOX out;

    for(auto n : in) {

        int fn_1    = 0;
        int fn      = 1;
        

        while(fn <= n) {
            out.push_back(fn);
            int temp = fn_1;
            fn_1 = fn;
            fn = temp + fn;
        }
    }

    return out==otheroutbox;
};


TEST_CASES.emplace("mailroom",mailroom);
TEST_CASES.emplace("busymailroom",busymailroom);
TEST_CASES.emplace("copyfloor",copyfloor);
TEST_CASES.emplace("scramblerhandler",scramblerhandler);
TEST_CASES.emplace("rainysummer",rainysummer);
TEST_CASES.emplace("triplerroom",triplerroom);
TEST_CASES.emplace("zeroexterminator",zeroexterminator);
TEST_CASES.emplace("octopliersuite",octopliersuite);
TEST_CASES.emplace("zeropreservationinitiative",zeropreservationinitiative);
TEST_CASES.emplace("tetracontiplier",tetracontiplier);
TEST_CASES.emplace("subhallway",subhallway);
TEST_CASES.emplace("eqaulizationroom",eqaulizationroom);
TEST_CASES.emplace("maximizationroom",maximizationroom);
TEST_CASES.emplace("absolutepositivity",absolutepositivity);
TEST_CASES.emplace("exclusivelounge",exclusivelounge);



TEST_CASES.emplace("alphabetizer",alphabetizer);
TEST_CASES.emplace("vowelincinerator",vowelincinerator);
TEST_CASES.emplace("duplicateremoval",duplicateremoval);
TEST_CASES.emplace("digitexploder",digitexploder);
TEST_CASES.emplace("recoordinator",recoordinator);


TEST_CASES.emplace("sorting",sorting);
TEST_CASES.emplace("countdown",countdown);
TEST_CASES.emplace("cumulativecountdown",cumulativecountdown);
TEST_CASES.emplace("threesort",threesort);
TEST_CASES.emplace("zeroterminatedsum",zeroterminatedsum);
TEST_CASES.emplace("modmodule",modmodule);
TEST_CASES.emplace("thelittlestnumber",thelittlestnumber);
TEST_CASES.emplace("smalldivide",smalldivide);
TEST_CASES.emplace("stringreverse",stringreverse);

TEST_CASES.emplace("fibonaccivisitor",fibonaccivisitor);
TEST_CASES.emplace("multiplicationworkshop",multiplicationworkshop);


