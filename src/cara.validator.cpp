/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/**
 * \author DeepNet42
 * \file cara.validator.h
 * \version 0
 * \date 2021
 * \copyright (c) 2021 DeepNet42.
 * This project is released under the Mozilla Public License v. 2.0.	 
 */


#include <iostream>
#include <functional>
#include <vector>
#include <algorithm>
#include <random>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <stack>
#include <climits>
#include <list>
#include "vm.h"
#include "lexer.h"
#include "parser.h"
#include "common.h"


using namespace hrmtk;

int main([[maybe_unused]] int argc, [[maybe_unused]] char* argv[]) {

    std::cout << "Cara Default Validator\n";
    std::cout << "----------------------\n";

    #include "validator.cpp"


    {
    
        for(auto& [f,s] : HRM_CODE) {
            std::istringstream code(s);

            std::cout << "Running test named " << std::setw(50) << f << " ... " << std::flush;
            
            
            if(TEST_CASES.find(f)==TEST_CASES.end()) {
                std::cout << "SKIP\n";
                continue;
            }

            Lexer lexer(code);
            Parser parser(lexer);
            
            auto good = parser.parse();

            if(!good) {
                std::cout << "parse failed.";
                return EXIT_FAILURE;
            }

            VM Cara;
            std::vector<int> outbox;
            std::vector<int> memory;
            try {
            if(FLOOR.find(f)!=FLOOR.end()) {
                memory = FLOOR[f];
            }
            std::cout <<  std::boolalpha << std::setw(4) << (Cara.validatedRun(parser.getIR(), INBOXES[f], memory, outbox, TEST_CASES[f])?"\x1B[34mPASS\033[0m":"\x1B[31mFAIL\033[0m");
            std::cout << " : " << std::setw(5) << Cara.stats().cycles << " cycles\n";
            } catch(const std::exception &exc) {
                std::cout << exc.what() << "\n";
            } catch(...) {
                std::cout << "ERROR\n";
            }
        }

    }

    return EXIT_SUCCESS;
}