/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/**
 * \author DeepNet42
 * \file vmbytecode.spec.h
 * \version 0
 * \date 2022
 * \copyright (c) 2022 DeepNet42.
 * This project is released under the Mozilla Public License v. 2.0. 
 */

#pragma once

#include <map>
#include <string>

namespace hrmtk {

	namespace BYTECODE_IMPL {
		enum OP: char {
			NO_OP=0,
			INBOX=8,
			OUTBOX=9,
			COPYTO_V=16,
			COPYTO_I=17,
			COPYFROM_V=18,
			COPYFROM_I=19,
			ADD_V=20,
			ADD_I=21,
			SUB_V=22,
			SUB_I=23,
			BUMPUP_V=24,
			BUMPUP_I=25,
			BUMPDN_V=26,
			BUMPDN_I=27,
			JUMP=28,
			JUMPN=29,
			JUMPZ=30,
			HALT=31,
		};

		const std::map<BYTECODE_IMPL::OP, std::string> OP_STRINGS_TABLE = {
			{BYTECODE_IMPL::NO_OP,"NO_OP"},
			{BYTECODE_IMPL::INBOX,"INBOX"},
			{BYTECODE_IMPL::OUTBOX,"OUTBOX"},
			{BYTECODE_IMPL::COPYTO_V,"COPYTO_V"},
			{BYTECODE_IMPL::COPYTO_I,"COPYTO_I"},
			{BYTECODE_IMPL::COPYFROM_V,"COPYFROM_V"},
			{BYTECODE_IMPL::COPYFROM_I,"COPYFROM_I"},
			{BYTECODE_IMPL::ADD_V,"ADD_V"},
			{BYTECODE_IMPL::ADD_I,"ADD_I"},
			{BYTECODE_IMPL::SUB_V,"SUB_V"},
			{BYTECODE_IMPL::SUB_I,"SUB_I"},
			{BYTECODE_IMPL::BUMPUP_V,"BUMPUP_V"},
			{BYTECODE_IMPL::BUMPUP_I,"BUMPUP_I"},
			{BYTECODE_IMPL::BUMPDN_V,"BUMPDN_V"},
			{BYTECODE_IMPL::BUMPDN_I,"BUMPDN_I"},
			{BYTECODE_IMPL::JUMP,"JUMP"},
			{BYTECODE_IMPL::JUMPN,"JUMPN"},
			{BYTECODE_IMPL::JUMPZ,"JUMPZ"},
			{BYTECODE_IMPL::HALT,"HALT"}
		};
	};

};