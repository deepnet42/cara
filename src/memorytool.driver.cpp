/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/**
 * \author DeepNet42
 * \file memorytool.cpp
 * \version 0
 * \date 2021
 * \copyright (c) 2021 DeepNet42.
 * This project is released under the Mozilla Public License v. 2.0.	 
 */
#include "memorytool.h"
#include "common.h"
#include <iostream>
#include <cstdlib>
#include <vector>

using namespace hrmtk;

int main([[maybe_unused]] int argc, [[maybe_unused]] char* argv[]) {


    std::string mem001 = 
    R"mem(
    [0]=0
    [1]=1
    [2]=2
    [3]=3
    [4]=4
    [5]=5
    [6]=6
    [7]=7
    [8]=8
    [9]=9
    [10]=10
    [11]=11
    [12]=12
    [13]=13
    [14]=14
    [15]=15
    [16]=16
    [17]=17
    [18]=18
    [19]=19
    [20]=20
    [21]=21
    [22]=22
    [23]=23
    [24]=24
    [25]=25
    [26]=26
    [27]=27
    [28]=28
    [29]=29
    [30]=30
    [31]=31
    )mem";



    std::string mem002 = 
    R"mem(
    [0]=A
    [1]=B
    [2]=C
    [3]=D
    [4]=E
    [5]=F
    [6]=G
    [7]=H
    [8]=I
    [9]=J
    [10]=K
    [11]=L
    [12]=M
    [13]=N
    [14]=O
    [15]=P
    [16]=Q
    [17]=R
    [18]=S
    [19]=T
    [20]=U
    [21]=V
    [22]=W
    [23]=X
    [24]=Y
    [25]=Z
    [26]=.
    [27]=.
    [28]=.
    [29]=.
    [30]=.
    [31]=-9999
    )mem";


    std::vector<int> mem1 = MemoryParser(mem001);
    std::vector<int> mem2 = MemoryParser(mem002);
    printVec(mem1,mem2);


    std::vector<int>mem1varifier = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31};
    std::vector<int>mem2varifier = {65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,-9999,-9999,-9999,-9999,-9999,-9999};



    return (mem1varifier==mem1 and mem2varifier==mem2) ? EXIT_SUCCESS : EXIT_FAILURE;
}