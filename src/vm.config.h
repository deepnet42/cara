#ifndef __HRMTK__VM__CONFIG__H__
#define __HRMTK__VM__CONFIG__H__

namespace hrmtk {
    constexpr const int MEM_SIZE			=	32;			///< Think of this as RAM
    //constexpr const int MAX_PROGRAM_SIZE	=	512;		///< not used
    constexpr const int ZERO				=	0;
    constexpr const int ONE					=	1;
    constexpr const int MAX_REG_VALUE_SIZE	=	999;		///< max value allowed (inclusive)
    constexpr const int MIN_REG_VALUE_SIZE	=	-999;		///< min value allowed (inclusive)

    constexpr const int MEMORY_EMPTY		=	-9999;		///< a flag to represent uninitalized memory (think empty)
}
#endif