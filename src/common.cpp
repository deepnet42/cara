/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/**
 * \author DeepNet42
 * \file common.cpp
 * \version 0
 * \date 2021
 * \copyright (c) 2021 DeepNet42.
 * This project is released under the Mozilla Public License v. 2.0. 
 */

#include "common.h"
#include <iostream>
#include <iomanip>


void warning(std::string_view s) {
	std::clog << "[warning ]: " << s << "\n";
}
void error(std::string_view s) {
	std::cerr << "[error   ]: " << s << "\n";
}
void info(std::string_view s) {
	std::clog << "[info    ]: " << s << "\n";
}


void action(std::string_view s) {
	std::clog << "[action  ]: " << s << "\n";
}

void detail(std::string_view field, std::string_view s) {
	std::clog << "[" << field << "]: " << s << "\n";
}



std::vector<std::string>    toArgumentVector(int argc, char* argv[]) {
    return std::vector<std::string>(argv + 1, argv + argc);
}

bool                        isArgumentDefined(std::vector<std::string>& args, std::string key) {
    for(auto& arg : args) {
        if(key==arg) {
            return true;
        }
    }
    return false;
}

std::string                 getArgumentValue(std::vector<std::string>& args, std::string key) {
    for(std::size_t i = 0; i < args.size()-1; i++) {
        if(key == args[i] and args[i][0]=='-' and args[i+1][0]!='-') {
            return args[i+1];
        }
    }
    return "";
}

std::vector<std::string>    getUnamedValuesByRelativePosition(std::vector<std::string>& args) {
     std::vector<std::string> v;
     for(auto& arg : args) {
         if(arg[0]!='-') {
             v.push_back(arg);
         }
    }
    return v;
}


std::string                 processName(int argc, char* argv[]) {
    return (argc>=1?argv[0]:"???");
}


/*
void printVec(const std::vector<int>& vec) {
    for(int n : vec) {
        std::cout << n << " ";
    }
}
*/