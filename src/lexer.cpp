/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/**
 * \author DeepNet42
 *  \file lexer.cpp
 * \version 0
 * \date 2021
 * \copyright (c) 2021 DeepNet42.
 * This project is released under the Mozilla Public License v. 2.0.
 */

#include "lexer.h"
#include <cctype>
#include <stdexcept>
#include <string>
#include <iostream>
#include <vector>
#include <iomanip>
#include <algorithm>

namespace hrmtk {



Lexer::Lexer(std::istream& file) :
		
		file_{file}, atend_{false}, cursor_{-1}, row_{0}, col_{-1},
		tok_{HRM_TOKEN_TYPE::END_TOKEN}, tokens_{}, tokenized_{false},
		previouslyNewLine{false}, lexState_{LEXER_STATE::NORMAL},
		define_{-1},	label_{-1}, comment_{-1}, number_{-1},
		order_{0} {

}

std::vector<HRM_Token> Lexer::getTokens() {

	if(not tokenized_) {
		tokenize();
	}
	return tokens_;
}

/**
 * \brief	constructs the next token piece by piece
 * \pre		a valid stream.
 * \post 	an internal vector of all tokens is constructed
 * 			from the input stream.
 */
void Lexer::lex() {

	char ch = getch();

	// token starts as undefined and becomes a correct token after further
	// processing. All bookkeeping about the token is known at this point


	tok_ = {HRM_TOKEN_TYPE::UNDEFINED, std::string(1,ch), cursor_, row_, col_};


	if(atend()) {
		set(HRM_TOKEN_TYPE::END_TOKEN);
		return;
	}

	switch(ch) {

		case S_WS: case S_NL: case S_CR: case S_HT:
			set(HRM_TOKEN_TYPE::WS);
			break;

		case S_LBRACKET:	set(HRM_TOKEN_TYPE::LBRACKET);				break;
		case S_RBRACKET:	set(HRM_TOKEN_TYPE::RBRACKET);				break;
		case S_COLON:		set(HRM_TOKEN_TYPE::COLON);					break;

		// This parses a specific kind of comment 
		// (e.g. -- COMMENT CODE --)
		// HRM has other comment types
		case S_MINUS:		lexComment();								break;

		default:
			if(std::isalpha(ch))
				lexText();
			else if(std::isdigit(ch))
				lexNumber();
			break;
		}
}

void Lexer::lexText() {

	std::string buffer;
	char ch = peekch();

	/* HRM encodes label names and comments as text blobs which complicates
	 * lexing quite a bit. To resolve the issue the lexer switches lexing modes
	 * just for parsing these blobs. The way the lexer knows when to switch modes
	 * is by detecting a specific pattern where these blobs are expected.
	 * They are expected after DEFINE (LABEL | COMMENT) NUMBER (BLOB);
	 * The lexer looks for an ordering of DEFINE, LABEL/COMMENT, and NUMBER
	 * (this extra bookkeeping complicates it a bit)
	 *
	 */


	if((define_==0 and (label_==1 or comment_==1)) and number_==2) {
		//we are reading a text blob for base64 encoded comment or label
		buffer+=getTextUntilSemicolon();
		tok_.value+=buffer;
		set(HRM_TOKEN_TYPE::RAW_BLOB);
		define_ = label_ = comment_ = number_ = -1;
		return;
	}


	while((not atend()) and ((std::isalpha(ch)) or (std::isdigit(ch)))) { //hmmm can digits follow a name or a keyword with digits?
		buffer.push_back(ch);
		getch();			// discard known value (we have it from peek)
		ch = peekch();		// look-ahead
	}

	tok_.value+=buffer;

	auto uCaseValue = tok_.value;

	// case insensitivity for mnemonics
	std::transform(std::begin(tok_.value), std::end(tok_.value), std::begin(uCaseValue), ::toupper);

	if (tok_.value==M_COMMENT or uCaseValue==M_COMMENT) {
		tok_.value = uCaseValue;
		set(HRM_TOKEN_TYPE::KEYWORD_COMMENT);
		comment_ = order_++;
	} else if(tok_.value==M_LABEL or uCaseValue==M_LABEL) {
		tok_.value = uCaseValue;
		set(HRM_TOKEN_TYPE::KEYWORD_LABEL);
		label_ = order_++;
	} else if((MNEMONIC_TABLE.count(tok_.value)>0) or (MNEMONIC_TABLE.count(uCaseValue)>0)) {
		// check the text value against known keywords
		tok_.value = uCaseValue;
		set(HRM_TOKEN_TYPE::MNEMONIC);
	} else if(tok_.value == M_DEFINE or uCaseValue==M_DEFINE) {
		tok_.value = uCaseValue;
		set(HRM_TOKEN_TYPE::BEGIN_DEFINITION);
		order_ = 0;
		define_ = order_++;
	} else {
		set(HRM_TOKEN_TYPE::LABEL_NAME);
		if(peekch()==S_COLON) {
			getch(); // discard colon
			set(HRM_TOKEN_TYPE::LABEL_DECLARATION);
		}
	}
}

void Lexer::lexNumber() {
	std::string buffer;

	char ch = peekch();
	while(not atend() and std::isdigit(ch)) {
		buffer.push_back(ch);
		getch();		//discard known value (we have it from peek)
		ch = peekch();	// look-ahead
	}

	tok_.value+=buffer;
	set(HRM_TOKEN_TYPE::ADDRESS);
	number_=order_++;
}

void Lexer::lexComment() {
	std::string buffer = "-";
	if(peekch()==S_MINUS) {
		char ch = getch(); 
		buffer.push_back(ch);
		// pull the second minus which begins a comment
		// skip all the text until we hit a (next minus?) or '\n'?
		// No. Making a decision until end of line \n (this will disable multi line comments)

		while((not atend()) and ((ch=getch())!=S_NL))
			buffer.push_back(ch);
			
		if(not atend() and ch==S_NL) { //if(not atend() and getch()==S_NL)  was because comments were only valid if they had a closing tag
			set(HRM_TOKEN_TYPE::COMMENT);
			tok_.value = buffer;
		} else {
			// what is this?
			// possibly a malformed comment but undefined
		}
	} else {
		std::cerr << "[lexer] [warning] " << tok_ << " comment must start with \"--\". deferred to Parser!\n";
	}
}

std::string Lexer::getTextUntilSemicolon() {
	std::string buffer;
	char ch = peekch();
	while(not atend() and ch!=S_SEMICOLON) {
		buffer.push_back(getch());
		ch = peekch();
	}
	getch(); // discard semicolon
	return buffer;
}

HRM_Token Lexer::nextToken() {
	lex();
	return tok_;
}

void Lexer::setLexState(LEXER_STATE state) {
	lexState_ = state;
}

bool Lexer::atend() {
	return atend_;
}

char Lexer::getch() {
	char ch = file_.get();

	if(ch == EOF) {
		atend_ = true;
	} else if (ch=='\n') {
		row_++;
		col_ = -1;
		previouslyNewLine = true;
	} else {
		col_++;
	}


	cursor_++;

	if(previouslyNewLine) {
		col_=0;
		previouslyNewLine = false;
	}


	return ch;
}



char Lexer::peekch() {
	return file_.peek();
}




bool Lexer::isws(char ch) {
	return (ch == S_WS or ch == S_NL or ch == S_CR or ch == S_HT);
}


void Lexer::skipWS(bool ws, bool nl, bool cr, bool ht) {
	while(char ch = peekch()) {
		if(		(ws and ch==S_WS) or
				(nl and ch==S_NL) or
				(cr and ch==S_CR) or
				(ht and ch==S_HT) ) {
			getch();
		}

	}
}


void Lexer::set(HRM_TOKEN_TYPE type) {
	tok_.type = type;	//abstracted for now. This action may be more complex in the future.
}


std::ostream& operator<<(std::ostream& os, const HRM_Token& token) {



    std::ios  state(NULL);
    state.copyfmt(os);

	os 	<< "[" << std::setw(6) << token.cursor << ",(" << std::setw(3)<< token.row << "," << std::setw(3)<< token.col << ")] type = " << std::setw(20);

	using enum HRM_TOKEN_TYPE;
	// slight clean up whenever (very low priority)
	if		(token.type==MNEMONIC)			os << "MNEMONIC";
	else if	(token.type==ADDRESS)			os << "ADDRESS";
	else if	(token.type==LABEL_DECLARATION)	os << "LABEL_DECLARATION";
	else if	(token.type==LABEL_NAME)		os << "LABEL_NAME";
	else if	(token.type==LBRACKET)			os << "LBRACKET";
	else if	(token.type==RBRACKET)			os << "RBRACKET";
	else if	(token.type==COLON)				os << "COLON";
	else if	(token.type==WS)				os << "WS";
	else if	(token.type==COMMENT)			os << "COMMENT";
	else if	(token.type==UNDEFINED)			os << "UNDEFINED";
	else if	(token.type==END_TOKEN)			os << "END_TOKEN";
	else if	(token.type==BEGIN_DEFINITION)	os << "BEGIN_DEFINITION";
	else if	(token.type==KEYWORD_COMMENT)	os << "KEYWORD_COMMENT";
	else if	(token.type==KEYWORD_LABEL)		os << "KEYWORD_LABEL";
	else if	(token.type==RAW_BLOB)			os << "RAWBLOB";
	
	os << ", value = ";

	if (token.value==" " or token.value=="\r" or token.value=="\n" or token.value=="\t") {
		os << int(token.value[0]);
	} else {
		if(token.type == RAW_BLOB)
			// we don't parse blobs for correctness but acknowledge their existence
			// and skip them. blobs are printed out up to a first few characters 
			// keeping debug pollution down. 
			os << token.value.substr(0,10)+" [... omitted]";
		else
			os << token.value;
	}

	os.copyfmt(state);

	return os;
}

void Lexer::tokenize() {
	lex();
	while(not atend()) {
		if(tok_.type!=HRM_TOKEN_TYPE::WS) { //possible to make this configurable to allow WS tokens as needed
			tokens_.push_back(tok_);
		}
		lex();
	}
	tokenized_=true;
}

}

