/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/**
 * \author DeepNet42
 * \file parser.cpp
 * \version 1
 * \date 2021
 * \date 2023
 * \copyright (c) 2021 DeepNet42.
 * This project is released under the Mozilla Public License v. 2.0. 
 */

#include "parser.h"
#include "is.h"
#include <vector>
#include <iostream>
#include <iomanip>
#include <string>
#include <sstream>

namespace hrmtk {

Parser::Parser(Lexer& lexer) : lexer_{lexer}, legalCode_{true},
		lst_{}, statements_{}, statementNo_{0} {

}

bool Parser::parse() {


	std::vector<HRM_Token> tokens = lexer_.getTokens();


	for(auto it = tokens.begin(); it != tokens.end(); ++it) {
		switch(it->type) {
		case HRM_TOKEN_TYPE::MNEMONIC:
			parseMnemonic(it);
			break;
		case HRM_TOKEN_TYPE::LABEL_DECLARATION:
			parseLabel(it);
			break;
		case HRM_TOKEN_TYPE::COMMENT:
			// we don't parse code comments or care
			break;
		case HRM_TOKEN_TYPE::KEYWORD_COMMENT:
			// we don't care about this reserved keyword but we do
			// care that it is followed by a number and if it does
			// we just ignore it but otherwise report and error
			parseKeywordComment(it);
			break;
		case HRM_TOKEN_TYPE::BEGIN_DEFINITION:
			// we do not care about visual definitions of labels or any
			// comments but we do parse them and their blobs for correctness
			// and ignore them
			parseDefinition(it);
			break;
		default:
			error("MNEMONIC or LABEL DECLARATION expected", *it);
			break;
		}
	}


	/*for(auto map_iter = lst_.begin(); map_iter != lst_.end(); ++map_iter) {
		std::cout << map_iter->first << ":" << map_iter->second << std::endl;
	}
	*/

	std::map<int, std::string> addressToLabel;
	int lastInstructionLine = 0;
	for(auto& e : statements_) {
		if (e.statement.op == OP::RESERVED) {
			//std::cout << "adding statement " << e.statementLabel << "->" << e.statementNo << "\n"; 
			lst_[e.statementLabel] = e.statementNo;
			addressToLabel[e.statementNo] = e.statementLabel;
		} else {
			lastInstructionLine = e.statementNo;
		}
	}

	
	for(auto& e : statements_) {
		if(e.needsTranslation) {
			//std::cout << "looking up " << e.parameter << "->" << lst_.at(e.parameter)<< "\n"; 
			if(lst_.count(e.parameter)>0) {
				int address = lst_.at(e.parameter);
				e.statement.oper = address;
			} else {
				//general error cannot translate label to address
				//throw("cannot translate label to address"); // come back to this later
				error("cannot translate label to address");
				error("no address for label '" + e.parameter + "'");
				error("'"+e.parameter+"' is defined on line " + std::to_string(e.statementNo));
			}
		}
	}

	/* 	From a technical point of view, it is not a job of a parser to check for such errors, 
		and we usually let some other system down the line catch these kinds of errors. 
		However, this also a good place to stop simple errors from propagating since we have 
		all of the context available to us. In this case there is no reason to allow labels to
		point to locations after the end of the program listing as well as letting jump instructions
		go past the end of the program. It is possible to have multiple programs that can link together,
		and we would want to allow such behavior. As of the time of the writing of the code below, 
		we do not have a concept of linking multiple programs into one process. 
		In this case—it would be very useful to allow such labels.
	*/
	for(auto& [label, address] : lst_) {
		if(address > lastInstructionLine) {
			error("label named \"" + label + "\" references beyond the end of the program listing");
			for(auto& statement : statements_) {
				if(	statement.statement.op == OP::JUMP or 
					statement.statement.op == OP::JUMPN or
					statement.statement.op == OP::JUMPZ) {
						if(statement.statement.oper > lastInstructionLine) {
							std::stringstream ss;
							ss << statement.statement << " (LABEL : " << label << ")";
							error(ss.str() +" would result in an illigal jump location");
						}
					}
			}
		}
	}

	return legalCode_;
}

void Parser::parseDefinition(std::vector<HRM_Token>::iterator& it) {
	it++;
	if(	it->type==HRM_TOKEN_TYPE::KEYWORD_COMMENT or
		it->type==HRM_TOKEN_TYPE::KEYWORD_LABEL) {
		it++;
		if(it->type==HRM_TOKEN_TYPE::ADDRESS) {
			it++;
			if(it->type==HRM_TOKEN_TYPE::RAW_BLOB) {
				// NICE WE GOT IT!
			} else {
				error("base64 or raw_blob data expected and must end with a semicolon", *it);
			}
		} else {
			error("address(number) expected after define label or define comment", *it);
		}
	} else {
		error("COMMENT or LABEL expected after DEFINE statement", *it);
	}
}

void Parser::parseKeywordComment(std::vector<HRM_Token>::iterator& it) {
	it++;
	if(it->type!=HRM_TOKEN_TYPE::ADDRESS) {
		error("COMMENT expects an address (number)", *it);
	}
}
std::list<PIR> Parser::getIR() {
	return statements_;
}

void Parser::parseMnemonic(std::vector<HRM_Token>::iterator& it) {
	if(				it->value == M_INBOX or
					it->value == M_OUTBOX or
					it->value == M_RAND or
					it->value == M_PUSH or
					it->value == M_POP) {
			addInstructionTypeNoParam(*it);
			return;

	} else if (		it->value == M_JUMP or
					it->value == M_JUMPZ or
					it->value == M_JUMPN){
		it++;
		if(it->type==HRM_TOKEN_TYPE::LABEL_NAME) {
			// jump destination
			addInstructionTypeDirect(*(it-1),*it);
		} else {
			error("LABEL_NAME (Address) expected", *it);
		}
	} else {
		// add, sub, bumpup/down copyto/from
		it++;
		if(it->type == HRM_TOKEN_TYPE::ADDRESS) {
			// direct addressing
			addInstructionTypeDirect(*(it-1),*it);
		} else if (it->type == HRM_TOKEN_TYPE::LBRACKET) {
			// indirect addressing
			it++;
			if(it->type == HRM_TOKEN_TYPE::ADDRESS) {
				// indirect address
				addInstructionTypeIndirect(*(it-2),*it);
				it++;
				if(it->type == HRM_TOKEN_TYPE::RBRACKET) {
					// closes the indirect addressing instruction
				} else {
					error("']' is missing", *it);
				}
			} else {
				error("ADDRESS is expected", *it);
			}
		} else {
			error("ADDRESS or '[' is expected", *it);
		}
	}
}

void Parser::parseLabel(std::vector<HRM_Token>::iterator& it) {
	addLabel(it->value,it->row);
}


void Parser::error(std::string message, HRM_Token& token) {
	std::cerr << "[parser] [error] :" << "["<< token.row  << "] " << message << ", but instead got " << token << std::endl;
	legalCode_=false;
}

void Parser::error(std::string message) {
	std::cerr << "[pasrer] [error] :" << message << std::endl;
	legalCode_=false;
}

void  Parser::addInstructionTypeNoParam(HRM_Token& token) {
	Instruction statement{OP::UNDEFINED,0};
	if(token.value == M_INBOX)			statement.op = OP::INBOX;
	else if (token.value == M_OUTBOX)	statement.op = OP::OUTBOX;
	else if (token.value == M_RAND)		statement.op = OP::RAND;
	else if (token.value == M_PUSH)		statement.op = OP::PUSH;
	else if (token.value == M_POP)		statement.op = OP::POP;
	else {
		error("unknown mnemonic", token);
		throw std::runtime_error("UNKNOWN MNEMONIC.");
	}
	statements_.push_back({statementNo_, "",statement,"", true,false});
	statementNo_++;
}

void  Parser::addInstructionTypeDirect(HRM_Token& op,HRM_Token& oper) {
	Instruction statement{OP::UNDEFINED,0};
	if(op.value == M_ADD)				statement.op = OP::ADD;
	else if (op.value == M_SUB)			statement.op = OP::SUB;
	else if (op.value == M_COPYTO)		statement.op = OP::COPYTO;
	else if (op.value == M_COPYFROM)	statement.op = OP::COPYFROM;
	else if (op.value == M_JUMP)		statement.op = OP::JUMP;
	else if (op.value == M_JUMPZ)		statement.op = OP::JUMPZ;
	else if (op.value == M_JUMPN)		statement.op = OP::JUMPN;
	else if (op.value == M_BUMPUP)		statement.op = OP::BUMPUP;
	else if (op.value == M_BUMPDOWN)	statement.op = OP::BUMPDOWN;
	// extended
	else if (op.value == M_MUL)			statement.op = OP::MUL;
	else if (op.value == M_DIV)			statement.op = OP::DIV;
	else if (op.value == M_MOD)			statement.op = OP::MOD;
	// extended
	else {
		error("unknown mnemonic", op);
		throw std::runtime_error("UNKNOWN MNEMONIC.");
	}
	bool needsTranslation = false;
		if(oper.value.length()>0 and std::isdigit(oper.value[0]))
			statement.oper = std::stoi(oper.value);
		else
			needsTranslation = true;

	statements_.push_back({statementNo_, "",statement,oper.value, true,needsTranslation});
	statementNo_++;
}


void  Parser::addInstructionTypeIndirect(HRM_Token& op,HRM_Token& oper) {
	Instruction statement;
	if(op.value == M_ADD)				statement.op = OP::ADDI;
	else if (op.value == M_SUB)			statement.op = OP::SUBI;
	else if (op.value == M_COPYTO)		statement.op = OP::COPYTOI;
	else if (op.value == M_COPYFROM)	statement.op = OP::COPYFROMI;
	else if (op.value == M_BUMPUP)		statement.op = OP::BUMPUPI;
	else if (op.value == M_BUMPDOWN)	statement.op = OP::BUMPDOWNI;
	// extended
	else if (op.value == M_MUL)			statement.op = OP::MUL;
	else if (op.value == M_DIV)			statement.op = OP::DIV;
	else if (op.value == M_MOD)			statement.op = OP::MOD;
	// extended
	else {
		error("unknown mnemonic", oper);
		//std::runtime_error("UNKNOWN MNEMONIC.");
	}
	bool needsTranslation = false;
	if(oper.value.length()>0 and std::isdigit(oper.value[0]))
		statement.oper = std::stoi(oper.value);
	else
		needsTranslation = true;

	statements_.push_back({statementNo_, "",statement,oper.value, true,needsTranslation});
	statementNo_++;
}



void  Parser::addLabel(std::string labelName, int address) {

	if(lst_.count(labelName)>0) {
		//found
		std::string e;
		e+=labelName;
		e+=" is already defined. Defined previously on line # ";
		e+=std::to_string(lst_.at(labelName));
		error(e);
	} else {
		statements_.push_back({statementNo_, labelName,{OP::RESERVED,statementNo_},"", true,false});
		lst_.emplace(labelName, address);
	}
}

std::map<std::string, int> Parser::getLabels() {
	return lst_;
}


}

