/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/**
 * \author DeepNet42
 * \file cara.validator.vmerrors_detection.cpp
 * \version 0
 * \date 2024
 * \copyright (c) 2024 DeepNet42.
 * This project is released under the Mozilla Public License v. 2.0.
 */




#include <iostream>
#include <functional>
#include <vector>
#include <algorithm>
#include <random>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <stack>
#include <climits>
#include <list>
#include "vm.h"
#include "lexer.h"
#include "parser.h"
#include "common.h"


using namespace hrmtk;

using INBOX = std::vector<int>;
using OUTBOX = std::vector<int>;
using MEMORY = std::vector<int>;
using FLOOR = std::vector<int>;

constexpr const int E = MEMORY_EMPTY;


// QUICK RIG
struct TestCase {
	std::string name;
	std::string code;
	INBOX inbox;
	OUTBOX outbox;
	FLOOR floor;
	VMError expected;
	bool pass{false};
	bool parsed{false};
};



int main([[maybe_unused]] int argc, [[maybe_unused]] char* argv[]) {

	std::cout << "Cara Default Validator\n";
	std::cout << "VMERROR detection test\n";
	std::cout << "----------------------\n";



	std::vector<TestCase> testCases;
	
	testCases = {
		TestCase{
			.name = "Indirect access violation",
			.code = R"hrm(-- Indirect access violation --
					INBOX 
					COPYTO 0
					COPYFROM [0]
					OUTBOX
					)hrm",
			.inbox = {99},
			.outbox = {},
			.floor = {},
			.expected = VMError::MEMORY_PROTECTION_FAULT
		},
		TestCase{
			.name = "Invalid inbox data value [55555]",
			.code = R"hrm(-- BAD INBOX --
					INBOX 
					OUTBOX
					)hrm",
			.inbox = {55555},
			.outbox = {},
			.floor = {},
			.expected = VMError::BAD_INBOX_FILE
		},
		TestCase{
			.name = "Invalid inbox data value [-55555]",
			.code = R"hrm(-- BAD INBOX --
					INBOX 
					OUTBOX
					)hrm",
			.inbox = {-55555},
			.outbox = {},
			.floor = {},
			.expected = VMError::BAD_INBOX_FILE
		},
		TestCase{
			.name = "Invalid memory data value [5555]",
			.code = R"hrm(-- Invalid memory data value [5555]--
					INBOX 
					OUTBOX
					)hrm",
			.inbox = {10},
			.outbox = {},
			.floor = {55555},
			.expected = VMError::BAD_MEM_FILE
		},
		TestCase{
			.name = "Invalid memory data value [-5555]",
			.code = R"hrm(-- Invalid memory data value [-5555] --
					INBOX 
					OUTBOX
					)hrm",
			.inbox = {10},
			.outbox = {},
			.floor = {-55555},
			.expected = VMError::BAD_MEM_FILE
		},
		TestCase{
			.name = "Direct access violation read",
			.code = R"hrm(-- Direct access violation read --
					COPYFROM 99
					)hrm",
			.inbox = {},
			.outbox = {},
			.floor = {},
			.expected = VMError::MEMORY_PROTECTION_FAULT
		},
		TestCase{
			.name = "Direct Access violation write",
			.code = R"hrm(-- Direct Access violation write--
					COPYTO 99
					)hrm",
			.inbox = {},
			.outbox = {},
			.floor = {},
			.expected = VMError::MEMORY_PROTECTION_FAULT
		},
		TestCase{
			.name = "Division by zero error",
			.code = R"hrm(-- Division by zero error--
					COPYFROM 0
					DIV 1
					)hrm",
			.inbox = {},
			.outbox = {},
			.floor = {10,0},
			.expected = VMError::DIV_ZERO_EXCEPTION
		},
		TestCase{
			.name = "Mod by zero error",
			.code = R"hrm(-- Mod by zero error --
					COPYFROM 0
					MOD 1
					)hrm",
			.inbox = {},
			.outbox = {},
			.floor = {10,0},
			.expected = VMError::DIV_ZERO_EXCEPTION
		},
		TestCase{
			.name = "Access violation for empty cell",
			.code = R"hrm(-- Access violation for empty cell --
					COPYFROM 0
					)hrm",
			.inbox = {},
			.outbox = {},
			.floor = {E},
			.expected = VMError::MEMORY_PROTECTION_FAULT
		},
		TestCase{
			.name = "Direct Access violation (range error read)",
			.code = R"hrm(--Direct Access violation (range error read) --
					COPYFROM 99
					)hrm",
			.inbox = {},
			.outbox = {},
			.floor = {10,0},
			.expected = VMError::MEMORY_RANGE_ERROR
		},
		TestCase{
			.name = "Direct Access violation (range error write)",
			.code = R"hrm(-- "Direct Access violation (range error write) --
					COPYTO 99
					)hrm",
			.inbox = {},
			.outbox = {},
			.floor = {10,0},
			.expected = VMError::MEMORY_RANGE_ERROR
		},
		TestCase{
			.name = "Access violation for uninitialized register",
			.code = R"hrm(-- Access violation for uninitialized register --
					ADD 0
					)hrm",
			.inbox = {},
			.outbox = {},
			.floor = {E},
			.expected = VMError::REGISTER_EMPTY
		},
		TestCase{
			.name = "Access violation for uninitialized memory",
			.code = R"hrm(-- Access violation for uninitialized memory" --
					COPYFROM 0
					)hrm",
			.inbox = {},
			.outbox = {},
			.floor = {E},
			.expected = VMError::MEMORY_EMPTY
		},
		TestCase{
			.name = "Register overflow",
			.code = R"hrm(-- "Register overflow" --
					INBOX
					COPYFROM 0
					a: ADD 0
					JUMP a 
					)hrm",
			.inbox = {1},
			.outbox = {},
			.floor = {1},
			.expected = VMError::REGISTER_RANGE_ERROR
		},
		TestCase{
			.name = "Register undeflow",
			.code = R"hrm(-- Register Underflow --
					INBOX
					COPYFROM 0
					a: SUB 0
					JUMP a
					)hrm",
			.inbox = {1},
			.outbox = {},
			.floor = {1},
			.expected = VMError::REGISTER_RANGE_ERROR
		}
		
	};





	{
		
		std::size_t testsFailCount		= 0;
		std::size_t testSuccessCount 	= 0;
		std::size_t parsingErrorCount 	= 0;


		for(auto& testCase : testCases) {

			std::istringstream code(testCase.code);
			std::cout << "Running test named " << std::setw(50) << testCase.name << " ... \n" << std::flush;
			
			
			Lexer lexer(code);
			Parser parser(lexer);
			
			auto good = parser.parse();

			if(!good) {
				std::cerr << "parse failed.\n";
				parsingErrorCount++;
				testCase.parsed = false;
				continue; // we can't run invalid code
			}

			testCase.parsed = true;

			try {
				VM Cara;

				OUTBOX outbox;
				FLOOR memory = testCase.floor;
				
				std::cout << "\n\n\n";
				std::cout << "TEST START\n\n\n";

				Cara.run(parser.getIR(), testCase.inbox, testCase.floor, testCase.outbox);


				if(Cara.machineState() == MState::UNRECOVERABLE_ERROR) {
					std::cout << "[info ] : UNRECOVERABLE_ERROR was expected and recieved\n";
					testSuccessCount++;
					testCase.pass = true;
				} else {
					std::cerr << "[error] : UNRECOVERABLE_ERROR was expected and BUT NOT recieved\n";
					testsFailCount++;
					testCase.pass = false;
				}

			} catch (VMError& exc) {
				if(exc == testCase.expected) {
					testSuccessCount++;
					std::cout << "[info ] : EXCEPTION expected and recieved; value: " << toString(exc) << "\n";
					testCase.pass = true;
				} else {
					std::cerr	<< "[error] : EXCEPTION expected and recieved BUT of INCORRECT TYPE; value: " << toString(exc) << ", but expected value : "
								<< toString(testCase.expected) << "\n";
					testCase.pass = false;
				}
			}

			std::cout << "TEST END\n\n\n";
		}


		std::cout << "Summary\n\n";

		std::cout << "Lexer/Parser Errors : " << parsingErrorCount << "\n";
		std::cout << "Expected Errors     : " << testSuccessCount << "\n";
		std::cout << "Total tests         : " << testCases.size() << "\n";



		std::cout << "\n\nReport\n\n";
		std::cout << "------\n";
		for(auto& testCase : testCases) {
			std::cout << "Test " << std::setw(50) << testCase.name << " ... " << std::flush;

			if(!testCase.parsed) {
				std::cout << "\x1B[31mPARSER ERROR\033[0m\n";
				continue;
			}

			std::cout << (testCase.pass?"\x1B[34mPASS (EXPECTED)\033[0m":"\x1B[31mFAIL\033[0m") << "\n";
		}


		if(parsingErrorCount>0) {
			std::cerr   << "[error] : " << "Parse errors detected. Please check test cases.\n";
			std::cout   << "[info ] : " << "Test count for lexer or parser errors must always be zero.\n";
		}

		if(testSuccessCount != testCases.size()) {
			std::cerr << "[error ] : " << "Some test cases failed.\n";
		}

		if(parsingErrorCount>0 or testSuccessCount != testCases.size()) {
			return EXIT_FAILURE;
		}

	}

	std::cout << "All test cases passed :)\n";

	return EXIT_SUCCESS;
}