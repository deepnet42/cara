/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/**
 * \author DeepNet42
 * \file vm.main.cpp
 * \version 0
 * \date 2021
 * \date 2022
 * \copyright (c) 2021 DeepNet42.
 * This project is released under the Mozilla Public License v. 2.0. 
 */

#include "VMConfigVersion.h"
#include "VMConfigVersion.unbuild.h"

#include <iostream>
#include <algorithm>
#include <vector>
#include <fstream>
#include <iomanip>
#include <cstdlib>
#include <cstdio>
#include <climits>
#include <string_view>
#include <algorithm>
#include <filesystem>
#include <fstream>
#include <chrono>


#include "vm.h"
#include "common.h"
#include "lexer.h"
#include "parser.h"
#include "memorytool.h"
#include "vmbytecode.h"
#include "vmbytecode.spec.h"


#if __linux__
#include <arpa/inet.h>
#elif _MSC_VER >= 1900
#include <winsock2.h>
#endif
using namespace hrmtk;



void header();
void usage(std::string_view processName);


// various utility
std::vector<std::string> assemblefiles(std::string stem);
void openallfiles(std::vector<std::string> fileNames, std::vector<std::ifstream>& files);
void closeallfiles(std::vector<std::ifstream>& files);
std::vector<int> readInbox(std::ifstream& inboxFile);
std::vector<int> readMem(std::ifstream& memFile);
std::vector<int> readControl(std::ifstream& controlFile);




// argv[0] = process
// argv[1,2,3,4] = {SOURCE, INBOX, MEMORY, CONTROL}

constexpr const int FILE_COUNT 		= 4;

constexpr const int SOURCE_FILE		= 0;
constexpr const int INBOX_FILE		= 1;
constexpr const int MEMORY_FILE		= 2;
constexpr const int CONTROL_FILE 	= 3;


constexpr const std::string_view HRM_PREFIX			= ".hrm";
constexpr const std::string_view HRMBYTECODE_PREFIX	= ".hrmb";
constexpr const std::string_view INBOX_PREFIX		= ".inbox";
constexpr const std::string_view MEM_PREFIX			= ".mem";
constexpr const std::string_view CONTROL_PREFIX		= ".control";
constexpr const std::string_view TEST_DIR			= "./tests/";



constexpr const char EMPTY_DOT			= '.';




int main(int argc, char* argv[]) {




	auto arguments		= toArgumentVector(argc, argv);
	auto fileNames		= getUnamedValuesByRelativePosition(arguments);
	auto processname	= processName(argc, argv);
	bool ezmode			= isArgumentDefined(arguments, "--ez");
	bool testDir		= isArgumentDefined(arguments, "--defaultTestDirectory");
	bool verbose		= isArgumentDefined(arguments, "--verbose");
	bool bytecodeflag	= isArgumentDefined(arguments, "--bytecode");


	if(ezmode and (fileNames.size()!=1)) {
		error("--ez switch requires an argument.");
		info("--ez sorting");
		usage(processname);
		return EXIT_FAILURE;
	} else if(!ezmode and fileNames.size() < 3) {
		error("missing arguments.");
		info("file names required in the following order: source_file, inbox_file, memory_file");
		usage(processname);
		return EXIT_FAILURE;
	}


	if(testDir) {
		auto path = std::filesystem::current_path();
    	std::filesystem::current_path(path.append(TEST_DIR));
		info("changed current working directory to: ");
		info(std::filesystem::current_path().string());
	}



	if(ezmode) {
		if(verbose) info("--ez switch enabled. Looking for files.");
		fileNames = assemblefiles(fileNames[0]);
	}

	bool enableTrace	= isArgumentDefined(arguments, "--trace");
	bool outboxFile		= isArgumentDefined(arguments, "--outboxfile");
	
	bool controlFileExists	= fileNames.size()>CONTROL_FILE?true:false;
	bool disableTypeCasting = !isArgumentDefined(arguments, "--disable-type-casting");

	if(verbose) header();


	//make sure all files are regular files and if so open them
	std::vector<std::ifstream> files(FILE_COUNT);
	try {
		openallfiles(fileNames, files);
	} catch(const std::filesystem::filesystem_error& e) {
		error(e.what());
		error(e.code().message());
		action("exit.");
		closeallfiles(files);
		return EXIT_FAILURE;

	}	catch(const std::runtime_error& e) {
		error(e.what());
		action("exit");
		closeallfiles(files);
		return EXIT_FAILURE;
	}	catch(...) {
		closeallfiles(files);
		return EXIT_FAILURE;
	}


	std::vector<int> in;
	std::vector<int> memory;
	std::vector<int> controlOutbox;
	
	try {
		std::ifstream& inboxfile = files[INBOX_FILE];
		std::ifstream& memoryfile = files[MEMORY_FILE];
		
		in = readInbox(inboxfile);
		memory = readMem(memoryfile);
		
		if(controlFileExists) {
			std::ifstream& controlFile = files[CONTROL_FILE];
			controlOutbox = readControl(controlFile);
		}

	} catch(const std::length_error& e) {
		error("Memory file is too large.");
		closeallfiles(files);
		return EXIT_FAILURE;
	} catch(const std::invalid_argument& e) {
		closeallfiles(files);
		return EXIT_FAILURE;
	} catch(...) {
		closeallfiles(files);
		return EXIT_FAILURE;
	}
	


	std::vector<int> out;
	VM vm;
	if(enableTrace)
		vm.enableTrace();
	else
		vm.disiableTrace();

		
	std::ifstream& sourceFile = files[SOURCE_FILE];

	if(bytecodeflag and not ezmode) {

		// kind of a hack need to re-architecture this
		// close the source file since everything was opened in text mode 
		// but we need to read the bytecode which is in binary 

		sourceFile.close();
		sourceFile.open(fileNames[SOURCE_FILE], std::ios::binary);

		{
			HRMVMBytecodeHeader header;
			sourceFile.read(header.identifier, sizeof(header.identifier));
			sourceFile.read(&header.version, sizeof(header.version));

			std::string ident(header.identifier, header.identifier + 4);

			if(ident != "HRMB" and header.version!=0) {
				error("bad header in bytecode file or unsupported bytecode version");
				closeallfiles(files);
				return EXIT_FAILURE;
			}
		}

		//info("good header");


		unsigned short instruction = 0;
		std::vector<Instruction> nativeInstructions;
		while(sourceFile.read(reinterpret_cast<char *>(&instruction), sizeof(unsigned short))) {
				nativeInstructions.push_back(unpackToVMNative(ntohs(instruction)));
		}

		try {
		
			vm.load(nativeInstructions);
			vm.setinbox(in);
			vm.setmemory(memory);
			vm.run();

		} catch(VMError& e) {
			::error(toString(e));
			return EXIT_FAILURE;
		}
			//std::clog << "done.";
	} else {

		Lexer lexer(sourceFile);
		Parser parser(lexer);

		bool good = parser.parse();

		//at this point we can close the files since they are not needed
		closeallfiles(files);

		
		if(verbose or !good) info(std::string("The program is ") + (good?"valid":"invalid"));
		
		if(!good) {
			::error("cannot execute an invalid program.");
			::info("see error messages from parser");
			return EXIT_FAILURE;
		}

		if(verbose) {
			auto statements = parser.getIR();
			for(auto i = statements.begin(); i != statements.end(); i++) {
				std::cout 	<< "[" << std::setw(3) << i->statementNo << "] "
							<< std::setw(7) << i->statementLabel << " "
							<< i->statement << "\n";
			}
		}


		try {
			vm.run(parser.getIR(), in, memory, out);
		} catch(VMError& e) {
			::error(toString(e));
			
			return EXIT_FAILURE;
		}

	}


		out = vm.getoutbox();
		if(verbose) {
			hrmtk::Stats s = vm.stats();
			
			std::cout << "machine steps   " << s.cycles << " cycles or steps\n";
			std::cout << "machine time    " << std::scientific << s.time << " seconds\n";

			if(s.time !=0 )
				std::cout << "machine steps/s "  << (double)s.cycles / (double)s.time << " steps/s\n";
			else
				std::cout << "machine steps/s "  << "0" << " steps/s\n";

			std::cout << "outbox contents: " << "\n\n";
		}
			
			
			std::for_each(
						out.cbegin(),
						out.cend(),
						[disableTypeCasting] (const int c) {
								std::cout << (int)c;
								if(disableTypeCasting) {
									if((c>='a' and c<='z') or (c>='A' and c<='Z')) {
										std::cout << "("<< (char)c <<")";
									}
								}
								std::cout << " ";
							}
						);

		
		if(outboxFile) {
			std::string inboxfilename = fileNames[INBOX_FILE];
			std::string outboxfilename = inboxfilename.substr(0, inboxfilename.find(".inbox", 0));
			outboxfilename+=".outbox";


			
			std::ofstream os(outboxfilename);
			std::for_each(
				out.cbegin(),
				out.cend(),
				[&os,disableTypeCasting] (int c) {
						  	os << (int)c;
							if(disableTypeCasting) {
								if((c>='a' and c<='z') or (c>='A' and c<='Z')) {
									os << "("<< (char)c <<")";
								}
							}
							os << " ";
						  }
					  );
			os << "\n";
			os.close();

			info("outbox saved to " + outboxfilename);
		}


		if(verbose) {
			std::cout << "\n\nMEMORY DUMP:\n";
			std::cout << "Legend: . (empty)\n";

			for(int i = 0; i < MEM_SIZE; i++) {

				if(i % 8 == 0)
					std::cout << "\n";

				if(vm.mem(i)==MEMORY_EMPTY) {
					std::cout << std::setw(6) << "." << " ";
				} else {
					std::cout << std::setw(6) << vm.mem(i) << " ";
				}
			}
		}


		std::cout << "\n";
		if(controlFileExists and vm.machineState()!=MState::UNRECOVERABLE_ERROR) {
			if(controlOutbox == out) {
				info("\x1B[34mOutput validation PASSED against control\033[0m");
			} else {
				info("\x1B[31mOutput validation FAILED against control\033[0m");
			}
		}


		if(vm.machineState()==MState::UNRECOVERABLE_ERROR) {
			::error("machine is in unrecoverable error. See output above.");
		}

		if(verbose) info("end.");


		

	

	std::cout << std::endl;
	return EXIT_SUCCESS;

}



void closeallfiles(std::vector<std::ifstream>& files) {
	for(auto& f : files) {
		if(f.is_open())
			f.close();
	}
}

void header() {

	std::cout	<< "------------------------------------\n"
				<< "-CARA VM.                          -\n"
				<< "VERSION" << CARA_VERSION_MAJOR << "." << CARA_VERSION_MINOR << "\n"
				<< "-(c) 2021 DEEPNET42                -\n"
				<< "-WARNING: NEVER RUN UNTRUSTED CODE.-\n"
				<< "------------------------------------\n"
				<< "MEMORY SIZE      " << MEM_SIZE << " cells\n"
				//<< "MAX PROGRAM SIZE " << MAX_PROGRAM_SIZE << " lines\n"
				<< "MAX PROGRAM SIZE " << "UNLIMITTED" << " (HUGE!) lines\n"
				<< "READY...\n";
}

void usage(std::string_view processName) {
	std::cout	<< "\n\n";
	std::cout	<< "Cara is a feature-rich virtual machine and a suit of tools built to execute\n";
	std::cout 	<< "Human Resource Machine assembly code.\n\n\n";
	std::cout 	<< "USAGE 1: " << processName << " SOURCE INBOX MEMORY [CONTROL] [...]\n\n";
	std::cout 	<< "OR\n\n";
	std::cout 	<< "USAGE 2: " << processName << " --ez prefix [...]\n\n";
	std::cout	<< "OR\n\n";
	std::cout 	<< "USAGE 3: " << processName << " --bytecode SOURCE_BYTECODE INBOX MEMORY [CONTROL] [...]\n\n";
	std::cout	<< "       --trace                : Enables step-by-step execution history for all instructions.\n";
	std::cout	<< "       --outboxfile           : Creates an output file named [inboxfile_stem].outbox containing the program's output.\n";
	std::cout	<< "       --disable-type-casting : Prevents showing integer values with their corresponding ASCII values in the output.\n";
	std::cout	<< "       --ez                   : Prefix for files. This option searches for files with the following names:\n";
	std::cout	<< "                                prefix.hrm, prefix.inbox, prefix.mem, prefix.control\n";
	std::cout	<< "                                cannot be combined with manual file specification\n";
	std::cout	<< "                                prefix.control is optional.\n";
	std::cout	<< "       --defaultTestDirectory : Sets the default directory for files when using --ez\n";
	std::cout	<< "       --bytecode             : Indicates the source code is in bytecode format. Note: This option cannot be with combined with --ez\n";
	std::cout	<< "\n\n";
	std::cout	<< "FILES:\n\n";
	std::cout	<< "SOURCE, INBOX, MEMORY, and CONTROL are ASCII text files:\n";
	std::cout 	<< " SOURCE  : (required) Path to the source code which contains instructions.\n";
	std::cout 	<< " INBOX   : (required) Path to the data input file containing integers and ASCII letters.\n";
	std::cout 	<< " MEMORY  : (required) Path to the memory initialization data file.\n";
	std::cout 	<< " CONTROL : (optional) Path to the file containing expected verification output.\n\n";
	std::cout 	<< "SOURCE, INBOX, MEMORY, and CONTROL files contain numbers, lower case and upper case ASCII letters\n";
	std::cout	<< "separated by a white space\n\n";	
	std::cout	<< "EXAMPLES: \n\n";
	std::cout	<< "          "<< processName << " --ez sorting\n";
	std::cout	<< "          "<< processName << " sorting.hrm sorting.inbox sorting.mem sorting.control\n";
	std::cout	<< "NOTES:\n\n";
	std::cout	<< "* soon(tm) or soonish(tm)?\n";
	std::cout	<< "VERSION: " << CARA_VERSION_MAJOR << "." << CARA_VERSION_MINOR <<"\n\n\n";
	std::cout	<< "(c) 2021 DeepNet42.\n";
	std::cout  	<< "\n";

}



std::vector<int> readInbox(std::ifstream& inboxFile) {
	std::vector<int> in;
	std::string readval;
	while(inboxFile >> readval) {
		//read in single letters
		if(readval.size()==1 and std::isalpha(readval[0]) and std::isupper(readval[0])) {
			in.push_back(int(readval[0]));
		} else {
			try {
				int digits = std::stoi(readval);
				in.push_back(digits);
			} catch(std::invalid_argument& e) {
				std::string s("Invalid inbox input. Expected a number but got '");
				s+=readval;
				s+="'";
				error(s);
				info("numbers and upper case letters only.");
				throw;
			}
		}
	}
	return in;
}

std::vector<int> readMem(std::ifstream& memFile) {
  	const std::string ws =" \t\r\n";
  	while(ws.find(memFile.peek()) != std::string::npos){
    	memFile.ignore();
  	}


	char startsWithBracket = memFile.peek();

	if(startsWithBracket == '[') {
		//user supplied an advanced format for memory
		std::string line;
		std::string buffer;
		while(std::getline(memFile, line)) {
			buffer+=line;
		}
		try {
			return MemoryParser(buffer);
		} catch(std::runtime_error& e) {
			error(e.what());
			throw;
		}
	}

	std::vector<int> memory;
	int mem_size = 0;

	std::string readval;
	while(memFile >> readval) {
		if(mem_size >= MEM_SIZE) {
			throw std::length_error("Memory file is too big");
		}

		if(readval.size()==1 and std::isalpha(readval[0]) and std::isupper(readval[0])) {

			memory.push_back(int(readval[0]));
		} else if (readval.size()==1 and readval[0]==EMPTY_DOT) {
			memory.push_back(MEMORY_EMPTY);
		} else {
			try {
				int digits = std::stoi(readval);
				memory.push_back(digits);
			} catch(std::invalid_argument& e) {
				std::string s("Invalid memory file. Expected a number but got '");
				s+=readval;
				s+="'";
				error(s);
				info("numbers and upper case letters only.");
				throw;
			}
		}
		mem_size++;
	}


	std::fill_n(back_inserter(memory), MEM_SIZE-memory.size(), MEMORY_EMPTY);
	return memory;
}


std::vector<int> readControl(std::ifstream& controlFile) {
	std::vector<int> controlOutbox;
	std::string readval;
	while(controlFile >> readval) {
		if(readval.size()==1 and std::isalpha(readval[0]) and std::isupper(readval[0])) {
			controlOutbox.push_back(int(readval[0])); 
		} else {
			try {
				int digits = std::stoi(readval);
				controlOutbox.push_back(digits);
			} catch(std::invalid_argument& e) {
				std::string s("Invalid control input. Expected a number but got '");
				s+=readval;
				s+="'";
				error(s);
				info("numbers and upper case letters only.");
				throw;
			}
		}
	}
	return controlOutbox;
}


void openallfiles(std::vector<std::string> fileNames, std::vector<std::ifstream>& files) {

		
		for(std::size_t i = 0; i < fileNames.size() and i < FILE_COUNT; i++) {
			
			if(not std::filesystem::exists(fileNames[i])) {
				throw std::runtime_error(fileNames[i] + " does not exist.");
			}
			
			if(not std::filesystem::is_regular_file(fileNames[i])) {
				// support for other files unknown yet
				throw std::runtime_error(fileNames[i]+" is not a regular file.");
			}

			files[i].open(fileNames[i]);
			if(not files[i].is_open()) {
				throw std::runtime_error(fileNames[i] + " cannot be opened.");
			}
		}
}

std::vector<std::string> assemblefiles(std::string stem) {


		std::string hrmfile;
		std::string inboxFile;
		std::string memFile;
		std::string controlFile;

		std::vector<std::string> fileNames;
		for(auto& dirEntry : std::filesystem::directory_iterator(".")) {
			
			std::string filename_  = dirEntry.path().stem().string();
			std::string extension_ = dirEntry.path().extension().string();

			if(filename_ == stem) {
				if(extension_ == HRM_PREFIX) {				hrmfile = filename_+extension_;
				} else if(extension_ == INBOX_PREFIX) {		inboxFile = filename_+extension_;
				} else if (extension_ == MEM_PREFIX) {		memFile = filename_+extension_;
				} else if(extension_ == CONTROL_PREFIX) {	controlFile = filename_+extension_;
				} else if(extension_ == HRMBYTECODE_PREFIX) {
					warning("Found a bytecode version.");
					warning("To run a bytecode version of your source see docs.");
				}
			}
		}


		//REQUIRED FILES
		//HRM, INBOX, and MEMFILE
		//CONROL is optional
		if(!hrmfile.empty() and !inboxFile.empty() and !memFile.empty()) {

			fileNames.clear();
			fileNames.push_back(hrmfile);
			fileNames.push_back(inboxFile);
			fileNames.push_back(memFile);
			if(!controlFile.empty()) fileNames.push_back(controlFile);

		} else {
			
			error("One of template files not found.");
			info("The following are missing:");
			if(hrmfile.empty()) info(stem+".hrm");
			if(inboxFile.empty()) info(stem+".inbox");
			if(memFile.empty()) info(stem+".mem");
			if(controlFile.empty()) info(stem+".control (optional)");

		}

		return fileNames;
}