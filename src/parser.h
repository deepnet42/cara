/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/**
 * \author DeepNet42
 * \file parser.h
 * \version 0
 * \date 2021
 * \copyright (c) 2021 DeepNet42.
 * This project is released under the Mozilla Public License v. 2.0. 
 */



/**
 * Parser consumes tokens from the lexer and validates correctness of the code.
 * Since the language is simple enough it is able to produce a syntax tree (more of a 
 * syntax list) which can be used by a VM or a translator to execute or emit code
 * from an intermidiate representation.
 */

#ifndef __HRMTK__PARSER_H__
#define __HRMTK__PARSER_H__


#include "lexer.h"
#include "is.h"
#include <map>
#include <string>
#include <list>


namespace hrmtk {

/**
 * \brief Intermidiate Representation
 */
struct PIR {
	int				statementNo;		///< order is determined the way it appears in the source
	std::string		statementLabel;		///< extracted label name from the source for the line number
	Instruction		statement;			///< the actuall instruction
	std::string 	parameter;			///< extract parameter that follows an instruction
	bool			isLabeled;			///< is this statement number labeled
	bool			needsTranslation;	///< is deferred translation needed for a label location
};





class Parser {
public:

	Parser(Lexer& lexer);
	bool parse();

	std::list<PIR> getIR();
	std::map<std::string, int> getLabels();
	//std::vector<INST> getInstructions();

private:


	void parseMnemonic(std::vector<HRM_Token>::iterator& it);
	void parseLabel(std::vector<HRM_Token>::iterator& it);
	void parseKeywordComment(std::vector<HRM_Token>::iterator& it);
	void parseDefinition(std::vector<HRM_Token>::iterator& it);
	void error(std::string message, HRM_Token& token);
	void error(std::string message);


	void addInstructionTypeNoParam(HRM_Token& op);
	void addInstructionTypeDirect(HRM_Token& op,HRM_Token& oper);
	void addInstructionTypeIndirect(HRM_Token& op,HRM_Token& oper);
	void addLabel(std::string labelName, int absoluteAddress);

	Lexer& lexer_;
	bool legalCode_;

	std::map<std::string, int> lst_;
	std::list<PIR> statements_;


	int statementNo_;
};



}




#endif
