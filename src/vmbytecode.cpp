/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/**
 * \author DeepNet42
 * \file vm.main.cpp
 * \version 0
 * \date 2021
 * \date 2022
 * \copyright (c) 2021 DeepNet42.
 * This project is released under the Mozilla Public License v. 2.0. 
 */


#include "vmbytecode.h"
#include "is.h"

#if __linux__
#include <arpa/inet.h>
#elif __WIN32__
#include <winsock2.h>
#endif


namespace hrmtk {

	unsigned short pack(BYTECODE_IMPL::OP opCode, unsigned short data) {
		unsigned short packed = opCode;
		// e.g. 01000 -> 01000 00000000000
		packed = packed << 11;

		// combine 01000 00000000000 + (11 bit data)
		// shave off 5 bits from end of data
		// move it back in place
		// combine using or
		//packed = packed | ((data << 5) >> 5);
		packed = packed | (data & 0x7FF); 
		return packed;
	}

	void unpack(unsigned short instruction, BYTECODE_IMPL::OP& opCode, unsigned short& data) {
		opCode = static_cast<BYTECODE_IMPL::OP> (instruction >> 11);
		data = instruction & 0x7FF;
	}


	unsigned short narrow(int value) {
		if(value >= 0 and value <= 999)
			return static_cast<unsigned short>(value);
		throw std::domain_error("value must be [0-999]");
	}


	unsigned short pack(const INST& inst) {
		
		switch(inst.op) {
			case OP::ADD        : return pack(BYTECODE_IMPL::ADD_V, narrow(inst.oper)); break;
			case OP::ADDI       : return pack(BYTECODE_IMPL::ADD_I, narrow(inst.oper)); break;
			case OP::SUB        : return pack(BYTECODE_IMPL::SUB_V, narrow(inst.oper)); break;
			case OP::SUBI       : return pack(BYTECODE_IMPL::SUB_I, narrow(inst.oper)); break;
			case OP::BUMPUP     : return pack(BYTECODE_IMPL::BUMPUP_V, narrow(inst.oper)); break;
			case OP::BUMPUPI    : return pack(BYTECODE_IMPL::BUMPUP_I, narrow(inst.oper)); break;
			case OP::BUMPDOWN   : return pack(BYTECODE_IMPL::BUMPDN_V, narrow(inst.oper)); break;
			case OP::BUMPDOWNI  : return pack(BYTECODE_IMPL::BUMPDN_I, narrow(inst.oper)); break;

			case OP::COPYFROM   : return pack(BYTECODE_IMPL::COPYFROM_V, narrow(inst.oper)); break;
			case OP::COPYFROMI  : return pack(BYTECODE_IMPL::COPYFROM_I, narrow(inst.oper)); break;

			case OP::COPYTO     : return pack(BYTECODE_IMPL::COPYTO_V, narrow(inst.oper)); break;
			case OP::COPYTOI    : return pack(BYTECODE_IMPL::COPYTO_I, narrow(inst.oper)); break;

			case OP::INBOX      : return pack(BYTECODE_IMPL::INBOX, 0); break;
			case OP::OUTBOX     : return pack(BYTECODE_IMPL::OUTBOX, 0); break;

			case OP::JUMP       : return pack(BYTECODE_IMPL::JUMP, narrow(inst.oper)); break;
			case OP::JUMPN      : return pack(BYTECODE_IMPL::JUMPN, narrow(inst.oper)); break;
			case OP::JUMPZ      : return pack(BYTECODE_IMPL::JUMPZ, narrow(inst.oper)); break;

			case OP::HALT       : return pack(BYTECODE_IMPL::HALT, narrow(inst.oper)); break;

			case OP::RESERVED   : return pack(BYTECODE_IMPL::NO_OP, 0); break;
			default: throw std::domain_error("this instruction cannot be mapped to valid bytecode");
			
		}

		throw std::runtime_error("hat trick");
	}


	Instruction unpackToVMNative(unsigned short instruction) {

		BYTECODE_IMPL::OP op = BYTECODE_IMPL::NO_OP;
		unsigned short data = 0;

		unpack(instruction, op, data);


		Instruction nativeInst;
		nativeInst.op = OP::UNDEFINED;
		nativeInst.oper = data;

		
		switch(op) {
			case BYTECODE_IMPL::ADD_V      : nativeInst.op = OP::ADD; break;
			case BYTECODE_IMPL::ADD_I      : nativeInst.op = OP::ADDI;  break;
			case BYTECODE_IMPL::SUB_V      : nativeInst.op = OP::SUB; break;
			case BYTECODE_IMPL::SUB_I      : nativeInst.op = OP::SUBI; break;
			case BYTECODE_IMPL::BUMPUP_V   : nativeInst.op = OP::BUMPUP; break;
			case BYTECODE_IMPL::BUMPUP_I   : nativeInst.op = OP::BUMPUPI; break;
			case BYTECODE_IMPL::BUMPDN_V   : nativeInst.op = OP::BUMPDOWN; break;
			case BYTECODE_IMPL::BUMPDN_I   : nativeInst.op = OP::BUMPDOWNI; break;

			case BYTECODE_IMPL::COPYFROM_V : nativeInst.op = OP::COPYFROM; break;
			case BYTECODE_IMPL::COPYFROM_I : nativeInst.op = OP::COPYFROMI; break;

			case BYTECODE_IMPL::COPYTO_V   : nativeInst.op = OP::COPYTO; break;
			case BYTECODE_IMPL::COPYTO_I   : nativeInst.op = OP::COPYTOI; break;

			case BYTECODE_IMPL::INBOX      : nativeInst.op = OP::INBOX; break;
			case BYTECODE_IMPL::OUTBOX     : nativeInst.op = OP::OUTBOX; break;

			case BYTECODE_IMPL::JUMP       : nativeInst.op = OP::JUMP; break;
			case BYTECODE_IMPL::JUMPN      : nativeInst.op = OP::JUMPN; break;
			case BYTECODE_IMPL::JUMPZ      : nativeInst.op = OP::JUMPZ; break;

			case BYTECODE_IMPL::HALT       : nativeInst.op = OP::HALT; break;

			default: nativeInst.op = OP::UNDEFINED; break;
			
		}
		return nativeInst; 
	}
};