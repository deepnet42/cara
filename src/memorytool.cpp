#include "memorytool.h"
#include <utility>
#include <sstream>
#include <exception>
#include "vm.config.h"

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/**
 * \author DeepNet42
 * \file memorytool.driver.cpp
 * \version 0
 * \date 2021
 * \copyright (c) 2021 DeepNet42.
 * This project is released under the Mozilla Public License v. 2.0.	 
 */

namespace hrmtk {





MemoryParser::MemoryParser(std::string memory) : mem{} {
    std::vector<std::pair<int, int> > index_data;
    std::stringstream ss(memory, std::ios_base::in | std::ios_base::out);



    char bracket;
    char equals;
    int address;
    int value;


    int max = 0;
    while(ss >> bracket) {
        if(bracket=='[') {
            ss >> address;
            
            if(ss.fail())
                throw std::runtime_error("mem: cannot convert number");

            ss >> bracket;
            if(bracket==']') {
                ss >> equals;
                if(equals=='=') {
                    //assume upper case else try numbers

                    auto saveloc = ss.tellg();
                    char c;
                    ss >> c;
                    
                    if(std::isupper(c)) {
                        value = (int)c;
                    } else if(c=='.') {
                        value = MEMORY_EMPTY;
                    } else {

                        ss.seekg(saveloc);

                        ss >> value;
                        

                        if(ss.fail()) {
                            throw std::runtime_error("mem: only digits and upper case letters allowed");
                        }
                    }
                    
                } else {
                    throw std::runtime_error("mem: '=' missing");
                }
            } else {
                throw std::runtime_error("mem: ']' missing");
            }
        } else {
            throw std::runtime_error("mem: '[' missing");
        }

         if(address<0 or address>=MEM_SIZE)
            throw std::runtime_error("mem: memory out of bounds");           

        if((value<MIN_REG_VALUE_SIZE or value>MAX_REG_VALUE_SIZE) and value!=MEMORY_EMPTY)
            throw std::runtime_error("mem: value out of bounds");


        max = std::max(max, address);
        index_data.push_back(std::make_pair(address,value));

    }

    mem.reserve(max+1); // zero based !!!
    mem.resize(max+1,MEMORY_EMPTY);
    for(auto& p : index_data) {
        mem.at(p.first) = p.second;
    }

    
}

MemoryParser::operator std::vector<int>() const {
    return mem;
}

}
