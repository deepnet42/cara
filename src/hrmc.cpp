/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/**
 * \author DeepNet42
 * \file hrmc.cpp
 * \version 0
 * \date 2021
 * \copyright (c) 2021 DeepNet42. 
 * This project is released under the Mozilla Public License v. 2.0. 
 */
#include <array>
#include <string>
#include <iostream>
#include <vector>
#include "lexer.h"
#include "parser.h"
#include <filesystem>
#include <istream>

std::string emit_inbox()        { return "if(inp>=inbox.size()) {std::cout << \"done.\\n\"; goto end;} reg = inbox[inp++];";             }
std::string emit_outbox()       { return "outbox.push_back(reg);"; 	        }

std::string emit_copyfrom(int oper)		{ return "reg = mem["+std::to_string(oper)+"];"; 		    	}
std::string emit_copyfromi(int oper)	{ return "reg = mem[mem["+std::to_string(oper)+"]];";		    }

std::string emit_copyto(int oper)		{ return "mem["+std::to_string(oper)+"] = reg;";				}
std::string emit_copytoi(int oper)		{ return "mem[mem["+std::to_string(oper)+"]] = reg;";           }

std::string emit_add(int oper)			{ return "reg = reg + mem["+std::to_string(oper)+"];"; 		    }
std::string emit_addi(int oper)			{ return "reg = reg + mem[mem["+std::to_string(oper)+"]];"; 	}

std::string emit_sub(int oper)			{ return "reg = reg - mem["+std::to_string(oper)+"];"; 			}
std::string emit_subi(int oper)			{ return "reg = reg - mem[mem["+std::to_string(oper)+"]];";     }

std::string emit_bumpup(int oper)		{ return "mem["+std::to_string(oper)+"]++; reg = mem["+std::to_string(oper)+"];"; 	        }
std::string emit_bumpupi(int oper)		{ return "mem[mem["+std::to_string(oper)+"]]++; reg = mem[mem["+std::to_string(oper)+"]];";	}

std::string emit_bumpdown(int oper)		{ return "mem["+std::to_string(oper)+"]--; reg = mem["+std::to_string(oper)+"];";	            }
std::string emit_bumpdowni(int oper)	{ return "mem[mem["+std::to_string(oper)+"]]--; reg = mem[mem["+std::to_string(oper)+"]];";	}


std::string emit_jump(std::string label)		{ return "goto "+label+";"; }
std::string emit_jumpn(std::string label)		{ return "if (reg<0) goto "+ label+";"; }
std::string emit_jumpz(std::string label)		{ return "if (reg==0) goto "+ label+";"; };
std::string emit_halt()			                { return "return;"; }

std::string emit_label(std::string label)        { return label+":";}


const std::string header =
R"header(

/*
GENERATED CODE 
GENERATED CODE
GENERATED CODE
*/

#include <iostream>
#include <vector>
#include <array>
#include <algorithm>
#include <fstream>
#include <filesystem>
#include <cstdlib>
#include <cstdio>
#include <climits>
#include <string_view>
#include <string>

constexpr int MEM_SIZE  = 32;
constexpr int ZERO      = 0;
constexpr int EMPTY     = -9999;
constexpr int MIN       = -999;
constexpr int MAX       = 999;

using in = std::vector<int>;
using out = std::vector<int>;
    

int main(int argc, char* argv[]) {


    
    if(argc < 3) {
        std::cout << argv[0] << " inbox.inbox mem.mem\n";
        return 0;
    }

    std::cout << "WARNING NEVER EVER EVER RUN UNTRUSTED CODE FRON THE NET IN THIS MODE\n";
    std::cout << "WARNING NEVER EVER EVER RUN UNTRUSTED CODE FRON THE NET IN THIS MODE\n";
    std::cout << "WARNING NEVER EVER EVER RUN UNTRUSTED CODE FRON THE NET IN THIS MODE\n";
    std::cout << "YOU HAVE BEEN WARNED!\n";

    std::array<int, MEM_SIZE> mem{EMPTY};
    in inbox;
    out outbox; 
    int reg = EMPTY;
    
    int inp = 0;
    int oper = 0;






    std::ifstream inboxfile(argv[1]);
	std::vector<int> input;
	std::string readval;
	while(inboxfile >> readval) {

		if(readval.size()==1 and std::isalpha(readval[0]) and std::isupper(readval[0]))
			input.push_back(int(readval[0]));
		else {
			try {
				int digits = std::stoi(readval);
				input.push_back(digits);
			} catch(std::invalid_argument& e) {
				std::string s("Invalid inbox input. Expected a number but got '");
				s+=readval;
				s+="'";
				std::cerr << s << "\n";
				//info("numbers and upper case letters only.");
				throw std::runtime_error("invalid inbox input\n");
				return EXIT_FAILURE;
			}
		}
	}


    inbox = input;
	std::ifstream memoryfile(argv[2]);


	

	std::vector<int> memory;
	int mem_size = 0;
	while(memoryfile >> readval) {
		if(mem_size >= MEM_SIZE) {
			throw std::runtime_error("error: increase memory size. At the limit");
		}

		if(readval.size()==1 and std::isalpha(readval[0]) and std::isupper(readval[0]))
			memory.push_back(int(readval[0]));
        else if (readval.size()==1 and readval[0]=='.') {
            memory.push_back(EMPTY);
        }
		else {
			try {
				int digits = std::stoi(readval);
				memory.push_back(digits);
			} catch(std::invalid_argument& e) {
				std::string s("Invalid memory file. Expected a number but got '");
				s+=readval;
				s+="'";
				std::cerr << s << "\n";
				//info("numbers and upper case letters only.");
				throw std::runtime_error("invalid inbox input\n");
				return EXIT_FAILURE;
			}
		}
		mem_size++;
	}

	std::fill_n(back_inserter(memory), MEM_SIZE-memory.size(), EMPTY);

    if (memory.size() <= mem.size()) {
        for(std::size_t i = 0; i < memory.size(); i++) {
            if(memory[i]!=EMPTY and (memory[i]<MIN or memory[i]>MAX)) {
                throw "error: bad bad bad! BAD MEMORY FILE\n";
            }
            mem[i] = memory[i];
        }
	} else {
        throw "error: bad bad bad! BAD MEMORY FILE\n";
    }
    

    {
)header";

const std::string footer =
R"footer(
    }

    end:
    std::for_each(
                outbox.cbegin(),
                outbox.cend(),
                [] (int c) {
                    std::cout << (int)c;
                    if((c>='a' and c<='z') or (c>='A' and c<='Z')) {
                        std::cout << "("<< (char)c <<")";
                    }
                    std::cout << " ";
                    }
                );

    std::cout << "\n";
    return 0;
}
)footer";


void usage(std::string_view name) {
    std::cout << name << " source.hrm\n";
}

int main(int argc, char* argv[]) {



    if(argc < 2) {
		usage(argv[0]);
		return EXIT_SUCCESS;
	}


    std::clog << "WARNING NEVER EVER EVER RUN UNTRUSTED CODE FROM THE NET IN THIS MODE\n";
    std::clog << "WARNING NEVER EVER EVER RUN UNTRUSTED CODE FROM THE NET IN THIS MODE\n";
    std::clog << "WARNING NEVER EVER EVER RUN UNTRUSTED CODE FROM THE NET IN THIS MODE\n\n\n";
    std::clog << "YES THIS IS MEANT TO LOOK SCARY BECAUSE YOU WILL GENERATE NATIVE CODE\n";
    std::clog << "AND IT IS RISKY!\n";
    std::clog << "THE MESSAGE BELOW IS MEANT TO BE ANNOYING!\n";

    std::clog << "type [yes, I know what I am doing.] if you want to execute! : ";
    
    std::string yes;
    std::getline(std::cin, yes);

    if(yes!="yes, I know what I am doing.") {
        std::clog << "A GOOD CHOICE! goodbye!\n";
        return EXIT_SUCCESS;
    }

    std::clog << "YOU HAVE BEEN WARNED!\n";

	if(!std::filesystem::is_regular_file(argv[1])) {
			// support for other files unknown yet
			throw std::runtime_error("expected a regular file but got something else.\n");
	}

	std::ifstream source(argv[1]);
    if(not source.is_open()) {
        std::cerr << "error  : " << argv[1] << " cannot be opened. see below:\n";
        std::cerr << "action : " << "EXIT\n";
        std::perror(argv[1]);
        return EXIT_FAILURE;
    }

	
    
    hrmtk::Lexer lexer(source);
	hrmtk::Parser parser(lexer);


    if(!parser.parse()) {
        std::cerr << "parsing error.\n";
        source.close();
        return EXIT_FAILURE;
    }

	
    auto IR = parser.getIR();
    auto labels = parser.getLabels();
    std::map<int, std::string> labelids;

    for(auto label : labels) {
        labelids[label.second] = label.first;
    }



    std::cout << header << "\n";
    for(auto statement : IR) {

        const std::string labelname     = statement.parameter;
        const int oper                  = statement.statement.oper;

        switch(statement.statement.op) {
            case hrmtk::OP::RESERVED:
                std::cout << emit_label(statement.statementLabel);
                break;
            case hrmtk::OP::ADD:
                std::cout << emit_add(oper);
                break;
            case hrmtk::OP::ADDI:
                std::cout << emit_addi(oper);
                break;
            case hrmtk::OP::SUB:
                std::cout << emit_sub(oper);
                break;
            case hrmtk::OP::SUBI:
                std::cout << emit_subi(oper);
                break;
            case hrmtk::OP::BUMPDOWN:
                std::cout << emit_bumpdown(oper);
                break;
            case hrmtk::OP::BUMPDOWNI:
                std::cout << emit_bumpdowni(oper);
                break;
            case hrmtk::OP::BUMPUP:
                std::cout << emit_bumpup(oper);
                break;
            case hrmtk::OP::BUMPUPI:
                std::cout << emit_bumpupi(oper);
                break;
            case hrmtk::OP::COPYFROM:
                std::cout << emit_copyfrom(oper);
                break;
            case hrmtk::OP::COPYFROMI:
                std::cout << emit_copyfromi(oper);
                break;
            case hrmtk::OP::COPYTO:
                std::cout << emit_copyto(oper);
                break;
            case hrmtk::OP::COPYTOI:
                std::cout << emit_copytoi(oper);
                break;
            case hrmtk::OP::INBOX:
                std::cout << emit_inbox();
                break;
            case hrmtk::OP::OUTBOX:
                std::cout << emit_outbox();
                break;
            case hrmtk::OP::JUMP:
                {
                    auto labelname = labelids[statement.statement.oper];
                    std::cout << emit_jump(labelname);
                }
                break;
            case hrmtk::OP::JUMPN:
                {
                    auto labelname = labelids[statement.statement.oper];
                    std::cout << emit_jumpn(labelname);  
                }
                break;
            case hrmtk::OP::JUMPZ:
                {
                    auto labelname = labelids[statement.statement.oper];
                    std::cout << emit_jumpz(labelname);
                }
                break;
            default:
                std::cout << "#pragma message(\"bad code!\")";
                return EXIT_FAILURE;
        }
        std::cout << "\n";
    }

    std::cout << footer << "\n";
    source.close();
    return EXIT_SUCCESS;
}






