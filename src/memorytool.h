/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/**
 * \author DeepNet42
 * \file memorytool.h
 * \version 0
 * \date 2021
 * \copyright (c) 2021 DeepNet42.
 * This project is released under the Mozilla Public License v. 2.0.	 
 */

#ifndef __HRMTK__MEMORY__TOOL_H__
#define __HRMTK__MEMORY__TOOL_H__

#include <string>
#include <vector>

namespace hrmtk {


    class MemoryParser final {
        public:
            
            MemoryParser(std::string memory);
            operator std::vector<int>() const;

        private:
            std::vector<int> mem;
    };

};


#endif