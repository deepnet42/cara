/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/**
 * \author DeepNet42
 * \file common.h
 * \version 0
 * \date 2021
 * \copyright (c) 2021 DeepNet42.
 * This project is released under the Mozilla Public License v. 2.0. 
 */

#pragma once
#ifndef __HRMTK__COMMON__H__
#define __HRMTK__COMMON__H__

#include <string_view>
#include <string>
#include <vector>
#include <iostream>



std::vector<std::string>    toArgumentVector(int argc, char* argv[]);
bool                        isArgumentDefined(std::vector<std::string>& args, std::string key);
std::string                 getArgumentValue(std::vector<std::string>& args, std::string key);
std::vector<std::string>    getUnamedValuesByRelativePosition(std::vector<std::string>& args);
std::string                 processName(int argc, char* argv[]);


void warning(std::string_view s);
void error(std::string_view s);
void info(std::string_view s);
void action(std::string_view s);
void detail(std::string_view field, std::string_view s);



template<typename T = std::vector<int> >
void printVec(const T& vec) {
    std::cout << "vect = { ";
    for(int n : vec) {
        std::cout << n << " ";
    }
    std::cout << "}\n";
}

template<typename T = std::vector<int>, typename... Rest>
void printVec(const T& vec, const Rest&... rest) {
    
    std::cout << "vect = { ";
    for(int n : vec) {
        std::cout << n << " ";
    }
    std::cout << "}\n";
    printVec(rest...);
}



#endif