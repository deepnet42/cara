/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/**
 * \author DeepNet42
 * \file parser.main.cpp
 * \version 0
 * \date 2021
 * \copyright (c) 2021 DeepNet42.
 * This project is released under the Mozilla Public License v. 2.0. 
 */

#include <iostream>
#include <iomanip>
#include "lexer.h"
#include "parser.h"
#include <errno.h>
#include <cstring>
#include <chrono>
#include <string_view>

using namespace hrmtk;

void usage(std::string_view processName);

int main(int argc, char* argv[]) {


	if(argc < 2) {
		std::cerr << "fetal error  : no input file." << std::endl;
		usage(argv[0]);
		return EXIT_FAILURE;
	}


	std::ifstream file(argv[1]);

	if(!file) {
		std::cerr << "fetal error  : cannot open file named " << argv[1] << "\n";
		std::cerr << "      reason : " << std::strerror(errno) << "\n";
		std::cerr << "      action : exit.\n";
		return EXIT_FAILURE;
	}

	Lexer lexer(file);
	Parser parser(lexer);

	auto startClock = std::chrono::high_resolution_clock::now();
		bool good = parser.parse();
	auto endClock = std::chrono::high_resolution_clock::now();
	

	if(good) {
		std::cout << "program listing:\n";
		auto labels = parser.getLabels();
		std::map<int, std::string> locations_to_labels;

		for(auto [f,s] : labels) {
			locations_to_labels.emplace(s,f);
		}

		auto statements = parser.getIR();
		for(auto i = statements.begin(); i != statements.end(); i++) {
			std::cout 	<< "[" << std::setw(3) << i->statementNo << "] "
						<< std::setw(7) << i->statementLabel << " "
						<< i->statement;

			if(i->statement.op==OP::JUMP or i->statement.op==OP::JUMPZ or i->statement.op==OP::JUMPN) {
				std::cout << " LABEL(" << locations_to_labels.at(i->statement.oper) <<")";
			}
			std::cout << "\n";
		}

		

		std::cout << "\n\nlabel" << " : " << "address" << "\n";
		if(labels.empty()) {
			std::cout << "source contains no labels.\n";
		}
		for(auto& [f,s] : labels) {
			std::cout << std::setw(5) <<f << " : " << std::setw(7) << s << "\n";
		}
		std::cout << "\n";
	}

	std::cout << "\nThe program is " << (good?"valid (parsing succeeded)":"invalid (parsing failed)") << std::endl;
	std::chrono::duration<double> exectime = endClock-startClock;
	std::cout << "parser time : " << std::scientific << exectime.count() << " s\n";
	
	return 0;
}


void usage(std::string_view processName) {
	std::cerr 	<< "usage: " << processName << " source_file\n";
}