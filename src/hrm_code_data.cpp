/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */



 
std::map<std::string, std::string> HRM_CODE;





HRM_CODE.emplace("mailroom", R"hrm(
-- 1 HUMAN RESOURCE MACHINE PROGRAM --
INBOX   
OUTBOX  
INBOX   
OUTBOX  
INBOX   
OUTBOX  
)hrm");


HRM_CODE.emplace("busymailroom", R"hrm(
-- 2 HUMAN RESOURCE MACHINE PROGRAM --
a:
    INBOX   
    OUTBOX  
    INBOX   
    OUTBOX  
    INBOX   
    OUTBOX  
    INBOX   
    OUTBOX  
    INBOX   
    OUTBOX  
    JUMP     a
)hrm");

HRM_CODE.emplace("copyfloor", R"hrm(
-- 3 HUMAN RESOURCE MACHINE PROGRAM --
    COPYFROM 4
    OUTBOX  
    COPYFROM 0
    OUTBOX  
    COPYFROM 3
    OUTBOX  
)hrm");

HRM_CODE.emplace("scramblerhandler", R"hrm(
-- 4 HUMAN RESOURCE MACHINE PROGRAM --
a:
    INBOX   
    COPYTO   0
    INBOX   
    OUTBOX  
    COPYFROM 0
    OUTBOX  
    JUMP     a
)hrm");

HRM_CODE.emplace("rainysummer", R"hrm(
-- 6 HUMAN RESOURCE MACHINE PROGRAM --
a:
    INBOX   
    COPYTO   0
    INBOX   
    ADD      0
    OUTBOX  
    JUMP     a
)hrm");

HRM_CODE.emplace("zeroexterminator", R"hrm(
-- 7 HUMAN RESOURCE MACHINE PROGRAM --
a:
b:
    INBOX   
    JUMPZ    a
    OUTBOX  
    JUMP     b
)hrm");

HRM_CODE.emplace("triplerroom", R"hrm(
-- 8 HUMAN RESOURCE MACHINE PROGRAM --
a:
    INBOX   
    COPYTO   0
    ADD      0
    ADD      0
    OUTBOX  
    JUMP     a

)hrm");
HRM_CODE.emplace("zeropreservationinitiative", R"hrm(
-- 9 HUMAN RESOURCE MACHINE PROGRAM --
    JUMP     b
a:
    OUTBOX  
b:
c:
    INBOX   
    JUMPZ    a
    JUMP     c

)hrm");

HRM_CODE.emplace("octopliersuite", R"hrm(
-- 10 HUMAN RESOURCE MACHINE PROGRAM --
a:
    INBOX   
    COPYTO   0
    ADD      0
    COPYTO   0
    ADD      0
    COPYTO   0
    ADD      0
    OUTBOX  
    JUMP     a
)hrm");

HRM_CODE.emplace("subhallway", R"hrm(
-- 11 HUMAN RESOURCE MACHINE PROGRAM --
a:
    INBOX   
    COPYTO   0
    INBOX   
    COPYTO   1
    SUB      0
    OUTBOX  
    COPYFROM 0
    SUB      1
    OUTBOX  
    JUMP     a
)hrm");

HRM_CODE.emplace("tetracontiplier", R"hrm(
-- 12 HUMAN RESOURCE MACHINE PROGRAM --
a:
    INBOX   
    COPYTO   0
    ADD      0
    ADD      0
    ADD      0
    ADD      0
    COPYTO   0
    ADD      0
    COPYTO   0
    ADD      0
    COPYTO   0
    ADD      0
    OUTBOX  
    JUMP     a
)hrm");

HRM_CODE.emplace("eqaulizationroom", R"hrm(
-- 13 HUMAN RESOURCE MACHINE PROGRAM --

    COMMENT  0
    JUMP     b
a:
    COPYFROM 0
    OUTBOX  
b:
c:
    INBOX   
    COPYTO   0
    INBOX   
    SUB      0
    JUMPZ    a
    JUMP     c
)hrm");

HRM_CODE.emplace("maximizationroom", R"hrm(
-- 14 HUMAN RESOURCE MACHINE PROGRAM --

a:
    INBOX   
    COPYTO   0
    INBOX   
    SUB      0
    JUMPN    b
    ADD      0
    JUMP     c
b:
    COPYFROM 0
c:
    OUTBOX  
    JUMP     a
)hrm");

HRM_CODE.emplace("absolutepositivity", R"hrm(
-- 16 HUMAN RESOURCE MACHINE PROGRAM --

a:
    INBOX   
    JUMPN    b
    JUMP     c
b:
    COPYTO   0
    SUB      0
    SUB      0
c:
    OUTBOX  
    JUMP     a
)hrm");

HRM_CODE.emplace("exclusivelounge", R"hrm(
-- 17 HUMAN RESOURCE MACHINE PROGRAM --

a:
b:
c:
    INBOX   
    JUMPN    d
    INBOX   
    JUMPN    e
    COPYFROM 4
    OUTBOX  
    JUMP     c
d:
    INBOX   
    JUMPN    f
e:
    COPYFROM 5
    OUTBOX  
    JUMP     b
f:
    COPYFROM 4
    OUTBOX  
    JUMP     a
)hrm");


HRM_CODE.emplace("countdown", R"hrm(
-- 19 HUMAN RESOURCE MACHINE PROGRAM --

a:
b:
    INBOX   
    COPYTO   0
    JUMPN    e
c:
d:
    OUTBOX  
    BUMPDN   0
    JUMPN    b
    JUMPZ    c
    JUMP     d
    COMMENT  1
e:
f:
g:
    OUTBOX  
    BUMPUP   0
    JUMPN    f
    JUMPZ    g
    JUMP     a
    COMMENT  0
)hrm");


HRM_CODE.emplace("multiplicationworkshop", R"hrm(
-- 20 HUMAN RESOURCE MACHINE PROGRAM --

a:
b:
    INBOX   
    COPYTO   0
    COPYTO   5
    INBOX   
    COPYTO   1
    JUMPZ    c
    JUMP     d
c:
    OUTBOX  
    JUMP     b
d:
e:
    COPYFROM 5
    ADD      0
    COPYTO   0
    BUMPDN   1
    JUMPZ    f
    JUMP     e
f:
    COPYFROM 0
    SUB      5
    OUTBOX  
    JUMP     a


DEFINE LABEL 0
eJyTYGBgcMz9kxWYnlt+NVW0dkeKYtuOlMmTrqYenLMhvWtxWnbIKsuCW+uSK26tm1s1fc3U2jVLNtXF
zNxUVzjRrDa1g6Uit3xtyZKC4qKEvFv5CXlA4xim1rZ1inXeWtc0wfwwwygYBaNgUAMAOYItPQ;

DEFINE LABEL 1
eJwTYGBgKEttc9mQfjD8aPbnAzL509e8Lz44Z2n5tSnn69s6OZtOdte2xcyM6MhcJNYZskqsc8nWn+25
e+yqLu2/VOSzm2EUjIJRMKQBAHAiIMw;

DEFINE LABEL 2
eJzjYWBgeF7m3F5cdK+ZP+Ne869k5/a6pNc9dUmP505OS9yclh1xZEaB0unrNUqnGVv3HmcYBaNgFAwr
AAC1GRdp;

DEFINE LABEL 5
eJyzZ2BguJX/fplMfuaio9kH52xIj5mZGWc942vkwTl/w9YseRC6c6192PPtIVFKp9XjA67aJP+4lZZ9
8GFDpdpToFaGO239e63bq46Wdrad+9h18oJH3+uL8ye+vig8Ten0xWmih0BqJnUlbp7Vnrh5f+vz7ftb
c/eAxD4ttN8UuCj1rOfSc690luhd0Vnis5t9WelqxxVdi6tXHZwjstZ6huE66xnv1x2c837duaWX1oas
ilvZtGX2PPPD76bPPgYyQ3DXmiV6u88ttT70fHvqqb3Hm07PPuZwxvzwnrNNW0DyVefeL9tz9ui2SUeP
/gbxt0ydfezblNw9FlO0duX2HN3GMApGwShgAAD2JYR9;

DEFINE LABEL 6
eJxzYmBgaJTaa1spW5o2TW52Y59i3aw+RaOVJ5Rz97ioSt5ZreR8Rl+xactbxdLV8uq8fWVaDK2ftO81
v9JjaJUxmTzpkrn7Qh4r2Y0d1hFHOqzbzgGNYxDzC1mVGiK44m9Y5iKpiPmzb0To9VdGOLc/CPUuexm0
pKDU73nRTx/vslYPvf7j7izzn3iUrk7wWrL1o3/EEamI1LOZcalnQeZMTjvptiE9JkQnMzM2Lftz9a38
eydA4gX1K1zP11vP8G40Pwzix0661/xwAm/fwwmP5/6bOH3N9Ck+u9fMMD+8Y/69EzpL9h4/usz88KPl
G/Y5rjBambbMesbseaK1z2Z5lwlP8y6zmLKh8t/Ee80Mo2AUjFAAAHSSd6I;

DEFINE LABEL 9
eJxLZWBgWGpzz+5BaK7R18hco81RG0ykIhQd2UIm+5530SoNM3s897+R0coFRtPX3DK5te6Od9OWIyFN
Wx6E2m+qjJDdCNTO0J3ZX/EoZ/7sV5knu03SjhaWpU5Pv5rqHpOXMT+MpzTAe2rtPTuQugW5Gyr9SnLL
r9csKQDxRYqsZyRX1M1ybomZCeIf6a8LrZp8tHDL1BVdoVN5+5b39VcEdK+JN+6SDLrSudd2UpeZ5ZF+
b8N30/uMn836bPZrnqKj+IKTbuILJvvazOuK05i1M8Ng+p8spjnPix4tr2oAmVmx/XvAn60sUXZbS9Pu
bzlaaLd1RVfFdusZb3aHrHqyT3bjx6M+u5tOP98ufa5pS+iFW+suXhRc4Xv54BzuKye711ze21R08Whh
1TnB5JcnrIPXH7vmz3ig0GfVnmv+FdsfRYD98GLnWhD9/I19rt0HM0u7D891G95rGTx/c9LN75V9bthz
s7rvzxhaQWr2Ptm5dsaz6WsmvPyz/tILn90iL/r3fn+Wu8fyGcdOy2dNW8Ke228Ke86xk2EUjAI6AgDO
V8Uh;
)hrm");




HRM_CODE.emplace("zeroterminatedsum", R"hrm(
-- 21 HUMAN RESOURCE MACHINE PROGRAM --

a:
    INBOX   
    COPYTO   0
    JUMPZ    d
b:
    INBOX   
    JUMPZ    c
    ADD      0
    COPYTO   0
    JUMP     b
c:
    COPYFROM 0
d:
    OUTBOX  
    JUMP     a
)hrm");

HRM_CODE.emplace("fibonaccivisitor", R"hrm(
-- 22 HUMAN RESOURCE MACHINE PROGRAM --

a:
    INBOX   
    COPYTO   2
    COPYFROM 9
    COPYTO   0
    COPYTO   1
    BUMPUP   1
    COPYTO   0
    OUTBOX  
b:
    COPYFROM 2
    SUB      1
    JUMPN    a
    COPYFROM 1
    OUTBOX  
    COPYFROM 1
    COPYTO   6
    COPYFROM 0
    ADD      1
    COPYTO   1
    COPYFROM 6
    COPYTO   0
    JUMP     b


DEFINE LABEL 0
eJzTZmBg6Ao6m3AmMDP2ih9LlLPnwfBNzl1xc+1lcw7ZbqhcanNwziHbW+uiHfYe3+Xw4xZQOcN30yVb
1TyWbJ0XZr8JxH8av2GfS8K1m4sT9h6vS+rfG5j+Z3135q11R7M5djrmKp12zA24ClKnWGl+eFGL+eHt
HZ8PgPgWU9QWTJ/CsfPmZIi567u7Fqf2z58tPdl94cVpbecuTgu4Kjxt642rC67d9FwqeYdhFIyCUUB1
AABvh1CO;

DEFINE LABEL 1
eJxTZWBgcMydH5aWHeB9NqPN5UDibJu3MVXW0yL32tqHpTq/DCr0ueLXFffTRzbH2mdDpbVPwITtvu4L
xfw4dgK1Mjy33jL5afyPqSC2Ss77ZSo5t9Z5ZlUdfZX5+YBnls/uBblGK8MKH89dWyI5XbPs2pTkih9T
7apY5lfUzD4G0jOpa+fa1P5b60DsvSt/TLVcvWQrwygYBaOAbgAAmdZAwA;

DEFINE LABEL 2
eJzjYmBgsEm+tP9q6p+sT2m3MhfkepclVwRMuNIpesi4a/oasc6T3WKd1/wZRsEoGAXDEgAAcroS6g;

DEFINE LABEL 6
eJwLYWBgeOPqHvPEo6rB2VP0UIKX0ula79SzpX6pZ2PCq44CpRle6bHMF4pmmQ9it8Uv2foq8+i2sMKm
LckVO9cmV4Ss6ih7v2xGgdHKsxmyGyNTfHb/SjY/PDnN+czR7NcXwwr1rhTU814G6c3tYZn/sjdkVWr/
pf1H+gOuNk3gvfxwAsOp2Ekb9oVOTdwMUhM5331h2QL3hRsWGa18teToNvZl906wL/t84NUS+00bFgmu
KFtwbuni2SGrpk95vj1xzpolv+Z1LTZZKLhiw6Kda7uXbNj3aonzmRVL9x5nX9a/N27l9DVha9YsWbs+
c9HSTaWr7bZu2Pdn697jf7Za363YDnGT2BGjlXzHm7a8PPH5QNW5o7+lz2U+f3mCY+fLE12L2U79mNp0
Wq9/z9nXPaEXXvdkXQmYcO/G/Nnlt98v+3xHdmPQ3efbp9zR2mV7M3fP8pM+u88cv7SfYRSMgiEGADgR
tDU;

DEFINE LABEL 9
eJzzYGBgeOOqFv0gdE28VMTZhBsRgsk3Iqanx4Q35TN6scz3dpPdmOGeuDkgIHHz0/ida4HKGaIdUjti
wts67yYptoH4xUVnEyYU38p8X9xfcakocTNLhf2mgnqIWr8S3r4/Vbx9GU0rukD85IrStJ/tZxNA7NhJ
LFHSk91jvk3RKrWYkrhZenLXYpB43VzB5NnzBJOvLtiZMXmhbI7JwudFi2fvbbKYothmMaWt8+TMH1P/
r4CY/2VHQt717Z+rr28vnDh1x4+perszFx3fG7Jq/4E/6ycdbdqy/GTi5oenS1dLn5s/W/pcW2fs2c/V
qaduZXadEEyedehRRMbeRxGCuzJj729JyAOZN3nh+0TumXWhF6fFhFRNrgtlGAWjYJgDAOK1g90;
)hrm");

HRM_CODE.emplace("thelittlestnumber", R"hrm(
-- 23 HUMAN RESOURCE MACHINE PROGRAM --

a:
    INBOX   
    JUMPZ    e
    COPYTO   0
b:
c:
    INBOX   
    JUMPZ    f
    SUB      0
    JUMPN    d
    JUMP     b
d:
    ADD      0
    COPYTO   0
    JUMP     c
e:
f:
    COPYFROM 0
    OUTBOX  
    JUMP     a
)hrm");

HRM_CODE.emplace("modmodule", R"hrm(
-- 24 HUMAN RESOURCE MACHINE PROGRAM --

a:
    INBOX   
    COPYTO   0
    INBOX   
    COPYTO   1
    COPYFROM 0
b:
    SUB      1
    JUMPN    c
    JUMP     b
c:
    ADD      1
    OUTBOX  
    JUMP     a
)hrm");

HRM_CODE.emplace("cumulativecountdown", R"hrm(
-- 25 HUMAN RESOURCE MACHINE PROGRAM --

a:
    INBOX   
    JUMPZ    d
    COPYTO   0
    COPYTO   1
b:
    BUMPDN   1
    JUMPZ    c
    ADD      0
    COPYTO   0
    JUMP     b
c:
    COPYFROM 0
d:
    OUTBOX  
    JUMP     a
)hrm");

HRM_CODE.emplace("smalldivide", R"hrm(
-- 26 HUMAN RESOURCE MACHINE PROGRAM --

a:
    COPYFROM 9
    COPYTO   2
    INBOX   
    COPYTO   0
    INBOX   
    COPYTO   1
    COPYFROM 0
b:
    SUB      1
    JUMPN    c
    COPYTO   3
    BUMPUP   2
    COPYFROM 3
    JUMP     b
c:
    ADD      1
    COPYFROM 2
    OUTBOX  
    JUMP     a
)hrm");

HRM_CODE.emplace("threesort", R"hrm(
-- 28 HUMAN RESOURCE MACHINE PROGRAM --

a:
b:
c:
d:
e:
f:
g:
    INBOX   
    COPYTO   0
    INBOX   
    COPYTO   1
    INBOX   
    COPYTO   2
    COPYFROM 0
    SUB      2
    COMMENT  0
    JUMPN    m
    JUMPZ    l
    COMMENT  1
    COPYFROM 1
    SUB      2
    COMMENT  3
    JUMPN    j
    JUMPZ    k
    COMMENT  4
    COPYFROM 1
    SUB      0
    JUMPN    i
    JUMPZ    h
    COPYFROM 2
    OUTBOX  
    COPYFROM 0
    OUTBOX  
    COPYFROM 1
    OUTBOX  
    JUMP     e
h:
i:
    COPYFROM 2
    OUTBOX  
    COPYFROM 1
    OUTBOX  
    COPYFROM 0
    OUTBOX  
    JUMP     f
j:
k:
    COPYFROM 1
    OUTBOX  
    COPYFROM 2
    OUTBOX  
    COPYFROM 0
    OUTBOX  
    JUMP     a
    COMMENT  5
    COPYFROM 0
    OUTBOX  
    COPYFROM 2
    OUTBOX  
    COPYFROM 1
    OUTBOX  
    JUMP     g
l:
m:
    COMMENT  2
    COPYFROM 1
    SUB      2
    JUMPN    n
    JUMPZ    o
    COPYFROM 0
    OUTBOX  
    COPYFROM 2
    OUTBOX  
    COPYFROM 1
    OUTBOX  
    JUMP     d
n:
o:
    COPYFROM 0
    SUB      1
    JUMPN    p
    JUMPZ    q
    COPYFROM 1
    OUTBOX  
    COPYFROM 0
    OUTBOX  
    COPYFROM 2
    OUTBOX  
    JUMP     c
p:
q:
    COPYFROM 0
    OUTBOX  
    COPYFROM 1
    OUTBOX  
    COPYFROM 2
    OUTBOX  
    JUMP     b


DEFINE COMMENT 0
eJyLZWBgEFLY4qev+D5RX9F6RqZyyKo2lelr6tSnr1mhv2YJUJqBV2xFl7x6ageInWxrPcPaR7Gt1G92
Y4SvaK2zZ3/FeZfc8k3OGyrNnO41X3fk7TNzkpwu6NK1uNVj+ppFnjvX3vHeuXa/1/Q1IP0n4szqzCPN
6m5EMLQKRev1MyXWzSpLZZlfltq12CXhz3r9mMTNlRGJm5cH31oHUn80OybkUc7B8Fv5tzKzS9cs0Sx7
v2xu1bmlILmzGbMbWSqqGs7Xi9aC+JMXVjVEzr/XPHuec/vduXr9d+fGzNwxX23BhkXnlp5dfG5p2rKD
c1SW/5iatsy5vXvJ3iaQHscVqR1xK9s6i9f+mPp84/tlILGAY0sK1PZ9rj6/K7VDb/fJbrV9ev2TjhZO
BMmt2mO08uHp0tUgdvrDts70h4ptr++b1W28218x5U5u+b0bn6uTrt9rdr2m17/k2vzZSdfdF9bfeL8s
/17i5vSHTVtEXuxcyzAKRsEgAgArmKon;

DEFINE COMMENT 1
eJzTYmBguBD9KKIyIjP2SEhpWldQQt6ZQK3S3MDP1WcCV3S9DNoyWTl0/mzzyDVL3saErJqdfGudZ9bO
tY65Riv/555bGpcXM7M553VPYHp/RWJiSCrQOIbJafa5MwqqGvQa1iwB8fe3dsUFLtpQ2T99RVfRNN4+
kNihzc+LsjcIJhuuWxP/fXVC3vfV95pF1gZMmLDu4JyOjaWr7bYmbhbctWSr837ZjRGHjVYyjIJRMApo
AgCMaU7m;

DEFINE COMMENT 2
eJzTY2Bg6HVVdPxj/z3gubVgsqb1rcxTVmZ1S23aOu/bFU40c4qZyemutoDRy30hX6DaggehMTPnhf2Y
KhWh2CYVEVE/L+x5kUewYPIVv7rQBK9CHzUPPS+gkQxsIWviv0Ym5PnEbqisS3Jur85XWwASD+i2DtZr
6Ip7XhaSOqEYaFdpU/6WqXWzQHLXt59NKF6rFm2ykCVKfEFmbOT8W5mfFm6o3LtSr59nw/zZhzazzA/e
qbZA7Mi1KQyjYBSMAqoAAAgdS1I;

DEFINE COMMENT 3
eJyLYWBgMBKZH2YkcitzpiBv322Bc0t/CO5cC+Sv0xI3WgmUZvDm3tu0iG9v0wcRszoQ/4Tyiq676iu6
TLSd2ydr32u+qjW78a56RP1qJbO6zfJ7m0LkV3T1KW6ZfFd9zZLJ2qWrdfRKV4P0zbWPqOexiqgvNp/d
aGmq2HbL5HXPVrPCiZKOB+cEO7sv9HYrXc3pvmSrtxvHzgrH3D3Jtj67eayebwfpPRLSFRcTblZ3I8J+
U2Zc4uay1J1rQeKT/FM7wgoZWkHs6VNYonxn/Ml6Nmtvk/zcNUvq5l7anzhHa1finJBV8nPfLxNf8Hhu
8/IfU/eu/DE1bM3BOX7rb61bu/759rXrfXZfWntpf9qyeyfKFuw9/mzW5wO+M3x2g8z8dr5w4r8zARO+
7GCZr7YvZJXYEftN3Fcu7QfJRX+sm+X2dvIkkRcnu2c8O9ld/fR1z94nARMePVJb4PhYduPW56KHLr2I
OHLqddXRpW+rjt5/L3qIYRSMgkEGAG+SrkM;

DEFINE COMMENT 4
eJzTZmBguOOt6Ljd93uAsf+frICAwokBAUe38QWGrHoZ1LV4XljdLJeEk90mac7trzKd2x/l8PZV58+f
XZ2/ZsnePKOVR7PtN21I19oVmdK/1yf20n6h6A37/oZx7AQay/C8rL/iTYNzu3U7b9/FafNnR85XW9A/
/c/6rt4N+0DyPw85tzMeuNdcsf1ec/Lmtk7NjQET1q63nuG3vmtx9obS1X+2Pt8evJNjp9q+59sfnt65
lmEUjIJRQHUAADiyVZA;

DEFINE COMMENT 5
eJxTY2Bg0LSusmaxPVrIYpu7R9Hu+fZNzrfWTfJfs+RvWOaiGxHnlm6O+rN+c9Tz7ZURnw80hu49HhDg
fMbaJ/VssLPzGUW7qqN/7M0PA41hcG75MXVTXczMvXlGK78X7Fzb28Cxc8+k3D0guepVhRM9lxZO3DH/
x9TEOY/nus5es8R19s61NvNy91xdIHqoe8nsYzNWO5/5s9X5TO3BiCMMo2AUjAK6AAAj70y4;

DEFINE LABEL 0
eJwTZ2BgkMnnKMnLuFR1NbWqoS7pZLdLwvzZTIlGK6+mNm3RyXy+/X3x8+1TaxM3n6+/ta63wWhlb8Pj
uV9qV3S5lW+oLC4C6T1aCDSGwbsxYMLxJsnp67vtN22ZmnqWYRSMglEw6AEAB8IsGg;

DEFINE LABEL 1
eJwTYWBgqKi55h9cF5J6vv5k95sGvSvn6/ceP19fuvpNQ+Yizia1Bdq9MTNvTn48t3+64IqsmX/WM80R
PcQ05+QFjVmF1wymX7v5cILkHbFOyTvn6wuvMYyCUTAKhhQAALL1KbA;

DEFINE LABEL 2
eJwTZmBguF6jVRpdbZ+rWDk93bDYKOVodmlaWap9rk/spaqQKIbWaZFtndMi1RYURk9fk5j4+QB/hvOZ
5pyTF3hKA64eqgi4WlFz8kJG0+cDDKNgFIyCIQcA9dMiiA;




DEFINE LABEL 2
eJwzZ2Bg4LG6tY7F1vwwkMkQZsbbB6LNnNQW9LqWrlbzED3UFZS4eXlw05bGUNFDD0Kdz4DkN0cptjEl
zj4GYh8JKV0dE1662iTNfSGIr5OZehZE/5t4a11A9611rc1Ltq5q5Nip17Bhn15D1VHOJoZTP9vbzuX2
tJ2LneR85u5c88Oz530+ANHD2/duesQREDtvcdfiFUt9djuuuHcCxD85s6oBRPut71rcsbF0dcOWiCN2
W/cej95278TUHVq7Nu0UXMG5d8tkkJrWfZOvM4yCUTAKCAIAnLtatg;

DEFINE LABEL 3
eJwzZ2BguGT+uue9hfWMtZZ7j5+y4r0s6fj64nmX1LNAKYbFqrfWVZvsXPvcunQ1iC/nnbvno3/unpjw
o9vMI+03TYssXX0jwmhlasif9ZP8+/du9917fLuv8xkxv5MXzgROvj4vTPJOSNSPWyC9PrGX9j+Nv3Zz
R4r54Q3pPrvPZvjsTsuuOuqY23buf+7WG3vzUs/OKPh8oLhIa9eEYo6d2aUb9rFUtJ0D6d1U57O7t0H0
UEZTzL0nzWteHm/aeiOjyfxwa/POtYtaQlbdaRNcIdZZuvpK5611d9rsNwnWy25kGAWjYBQQBAC+3G5B
;)hrm");

/*
HRM_CODE.emplace("storagefloor", R"hrm(
-- 29 HUMAN RESOURCE MACHINE PROGRAM --

a:
    INBOX   
    COPYTO   14
    COPYFROM [14]
    OUTBOX  
    JUMP     a


DEFINE LABEL 14
eJwzZ2BgKNOyz/2kvWUykMkwW8PZCUTfMknIm2Ch1+9nOX+2n+Xrnmyrz9UstrI59+12Zpg57cxo9cgt
3+5rPQOk9lfy0UKmxKZ8n9imfP2Yo4UhUZ+rp0Xy9l2I3jJ5cULMTJWculkgdezZkkGOuVql3wt+TAXx
d1XPblzVGFG/vWNDZW7P8yK+nqb8j10JeefrE/IE648WCtab1Z2vZ2jlbNLrb23eMnlW+4+peyYVTgTp
vbQ2Ia96VVO+9OTJk0D8xbPXxMvPfZ94a9XkSZob1yy5v2X6GoZRMApGAUEAAEEZW0c;
)hrm");
*/

/*
HRM_CODE.emplace("stringstoragefloor", R"hrm(
-- 30 HUMAN RESOURCE MACHINE PROGRAM --

a:
    INBOX   
    COPYTO   24
b:
    COPYFROM [24]
    JUMPZ    a
    OUTBOX  
    BUMPUP   24
    JUMP     b


DEFINE LABEL 24
eJwLZWBgOBMYE+IRvCb+SMjsRuXQgAlHQi7tVw7l2FkZMX2NS8L82QcSY2b+Sq6bdTVVbcHV1JBV8klH
t12I7t/7N6x/r32Yz26gEQzNOX/W36/sWnyoQm3B2pKDc7YWHpxTnc8y/3/umiWOuUu2LsjV2iWTf2n/
1sKIIzylVUf/VPXvDa47ug2k17tx59rjTYmbE1qbtpR23lq3vltwRUC3+8I7bTEz97fGzGxtfjx3VWPm
Iu/Gc0tB6rt6b61TnlB1dP5E67sOEx8/etm75uX67jUvJ3VlPp/VLnmnto338v7WiCNybRw7IzqWbJWe
PH0NSN+zWe4LT850X9g/fefad9ObtmjMOrptx/wlW88u3rlWZ0nIKpAa3xn3mkH0ghWS0+NWssyPW2m/
yXFF/97m5Rw7m5f/We+4wmhl2BrrGTwbAia4bXo8l2Vz/977WzbsM9uRu+fN7qPbFu3fuZZhFIyCIQQA
qTWtRw;
)hrm");
*/

HRM_CODE.emplace("stringreverse", R"hrm(
-- 31 HUMAN RESOURCE MACHINE PROGRAM --

a:
b:
    INBOX   
    JUMPZ    c
    COPYTO   [14]
    BUMPUP   14
    JUMP     b
c:
d:
    BUMPDN   14
    COPYFROM [14]
    OUTBOX  
    COPYFROM 14
    JUMPZ    a
    JUMP     d


DEFINE LABEL 14
eJzzZWBgmBdWmhYQ8D5Rzvt94iq3kNTzLrcypzodLbzuGFEf7Hyy29vtx9Rab7UFfIHvl7GFCK64EH1u
aXem5HSgVgbH3KOF3wtOdhsWT56UXGE940ut9QzOpmtTFrUETEhoXdGV0FrVcLypKb+3YXr6rmr3mOSK
RxEiRWrRlgVdcf9zS9MW5Nrn7s1rygeZ1dXLEuXRx1GiPOF1z/yJWyZPnzJ/tsas+bOZ5sTMLFsQMGHD
otQOnSUR9d1LvMvyFmfG8i8+m/B/xesekF7NjV1xc7cytO7a9nhu9La2zuvbI+pX7UnISzgwPf3noZDU
K0eOFq4/1ta5/ljAhK4TLPNBeh6e3msrfU4w+dv5yZO2XHg81+CS4ArfyyGrXK8Zrey5/3ju3ieFE6uf
tnWC1P48lJD3++afLMOXIakMo2AUDCMAAFcQlJA;
)hrm");

/*
HRM_CODE.emplace("inventoryreport", R"hrm(
-- 32 HUMAN RESOURCE MACHINE PROGRAM --

a:
    COPYFROM 14
    COPYTO   18
    COPYTO   17
    INBOX   
    COPYTO   19
b:
    COPYFROM 19
    SUB      [17]
    JUMPZ    c
    JUMP     d
c:
    BUMPUP   18
d:
    BUMPUP   17
    COPYFROM [17]
    JUMPZ    e
    JUMP     b
e:
    COPYFROM 18
    OUTBOX  
    JUMP     a


DEFINE LABEL 14
eJxzYWBg8LMsTWuwK02z9vmTZewvm/PRPyFvkv/zop8+EfVPPE52v7fYuZbH6s/6+3aJmxm9lmy9m7Rk
K1Abw327FV1fnFI7Sv2c20H8sxmKbf9zFdtOle5tUmv2LmNsPVq4qOVPlnejYPKX2jXxf6oyY93Ku+K2
Fu7MqM5PyHPM7a9wzN3b9D93RVdxUczM98XzZydXqC24XqO24E3D47kgMyd1eZdp916b4jAxcfP8ibfW
KU+YP1t5wsnu2EmzG6dPuVR1cqZWad1c7zKThRH1IPUqy9s6t665tc5v/ZKtbpuObtu1bfqaqTvcF07d
cW3Kn62pHYpb7jWf2lDVsGBFVcOGRQytNvNWdNXN5e27O1dy+ux5MTMZRsEoGGEAAB36hF4;

DEFINE LABEL 17
eJzTYGBgYPQSra31Dphwx/vcUiCXQc+10AdES0VIBk2LzIz1iU3taItP3AwSUw5N7ZBPOtkNYh/Nlpwu
k79l8q38womWBSu6HuUotnVnKradzUjtOJuh178iy3pGXN77Zd8Lbq3zK1my1a5qyVaQvl3VvH1Tawsn
FtTHzPRunL5GsF5tQXCd5PSC+tSOVY3O7XJtK7oiOl73GHcVTmQYBaNgFNAUAACIPz8b;

DEFINE LABEL 18
eJxzZGBg2O91qeqnT3/Fdl/vsp8+HCXH3XPLe11Fa6c6ve4xc1JbIOhSujrDfcnWO94+uz2CfXbvDj+6
rTKiaQtQK8PihIAJixPcFyYmCq6wSf6z3iRtyVbPrCVbb+WXrt5a2LW4uOjgHJGiwonV+W2drzJXdP1K
vjZFPoll/t0k94Ug/ZvqGFpXNb5fltDKsfNne+6eMz25e1L7n2+Xnnxr3cVpagveTf8x9d10+01rZmjt
AqmPnF84cfLC0tWfFnLsDFz0Zz1IrHn5lslxK/X6Z6z+MdVwnfvCteunr1m6yWf3/S0Q+e2HvwdsP7wz
Y9JRvf6uE7IbQWJfdjC0qu1L7dhywbmdYRSMghEMADbJfeU;

DEFINE LABEL 19
eJyLZGBgOBLyudojmKNEO+hooZiffe4TD/tcPVet0gKXz9VvXBXbnD31+q/4xcw0STu6LTJFaxdTos/u
v2E+uz2Cn28HamfgihOtPVV6bmlHWdfi52XzZ3eU3WteWq62YGn51hssFXpXQGo21TG0LmoxPyzX1rV4
e4def0A3Q+vL3tmNR/qd26smz589fcr7ZdOn3Fq3ZWrhRN8ZorXts1I76uY+niu+4Pl2k4Vt50wWRhwB
mXN28fvEvSt5+y6tFT00Yd3z7c83nlvasOXgnE07J0/S2104kXPvtSkJB4xWWh96vt36UO6eOwcv7Wc8
EHGkd/fsY/e3zD7mtiniiObG/r0gszxOdi1WPv18e+zZzwemn5997OSVvccPXjc//Pvmkq2/b4asqr+h
tiDryo+psWevTVl+MmbmlSNdi38emr7m56E/6ycdXbI19uySrSBz3r9cE9/xpr9C8b37wob3f9b/+XBp
f8WnzweO/7DfJPdn51qGUTAKBjEAAOqYwDs;
)hrm");
*/


HRM_CODE.emplace("vowelincinerator", R"hrm(
-- 34 HUMAN RESOURCE MACHINE PROGRAM --

a:
b:
    COPYFROM 5
    COPYTO   6
    INBOX   
    COPYTO   9
c:
    COPYFROM [6]
    SUB      9
    JUMPZ    b
    BUMPUP   6
    COPYFROM [6]
    JUMPZ    d
    JUMP     c
d:
    COPYFROM 9
    OUTBOX  
    JUMP     a


DEFINE LABEL 5
eJyzZGBgeOMaktoXszODK+5W5ok4+9wTcRwlb2Mi6h+EXpvS68qxU81Da5dKjtFKoFKGQ7YruuzDFNuO
ZpvVgfjejW2dAd2zG/dM6q+omqxVKj35aOHDCU35k7qa8s/Xa5U2VH6uPlU6u1Gk6GT394JrU8IK589e
W/J+2aGK0tVybdPXKE94v0x4mtoCkFknZ67oEl8wfU3kfOsZO+a/7slbzNDquGJv05cd95pB8s772zqt
D12bsv3wj6m5x7dMXn6Sty/11Mlu5dP3mo+c+lz98oRWacAxjpLWfRH1b3YztPbuTu3w3lM4kWEUjIJR
gBMAALyTbgg;

DEFINE LABEL 6
eJxTZmBgOBKSW64cKjkdyGQQ89viB6JPxKU6uyScTWBKhIhPi/QuS8vWKgWxLQvuNR+qEK1d1Xi0cGrt
0UK7Ko4Sw+LZjd8L2jpv5RdOtCw4OMewWG3B8zK1BQX1B+fsb/0xFaTPuEu01mHi+2UPJ6zo2jNpb9OW
qWZ1NvP6K2asBmHRWoZRMApGAd0BAGaQOEY;

DEFINE LABEL 9
eJwLYmBgyA1cEy/n3RVX4SiYvNSmNM3QIiHvkvnRQkMLrdIO66qGYGfevlk+dbMm+bsvnOS/Zsl+L8EV
BS6CK4BaGY6EfK4ujC6c6JLweG5Z6vtl/BlGK3Uyzy19lfl47qvM1I7AdI6dk9NWnJ+cdu1mYHrhNZCe
peVapUvLefs0y0QPaZZp7VKsdF84tfZkt1pzf4VzS3+FWOePqeu7p69Z382xc333+2Uvewsnzp8YUV81
eUOl7wz3hSdn5u5pn2W/CWRW5PzvARsWbZmct3jn2u4lmYvSllnPqF6V2vF+XVXD841mdbu2BUzQ2220
Um+37MaCXUu2/tnav7dj4+cDlqs37Du7+NY6kBmlR9o6c4+fW+pxUnZj6qnEzU2n7TftOVu6+tt5tQXf
zm+Z/O9MakfXCcW2iMOve/Yf+DE14UDMTOOjktNBenvu63ndemp+mGEUjIIhCgBWSaFF;
)hrm");

HRM_CODE.emplace("duplicateremoval", R"hrm(
-- 35 HUMAN RESOURCE MACHINE PROGRAM --

    COPYFROM 14
    COPYTO   13
    COPYTO   12
    INBOX   
    COPYTO   11
    COPYTO   [12]
    OUTBOX  
    BUMPUP   12
a:
b:
    INBOX   
    COPYTO   11
    COPYFROM 13
c:
    COPYFROM 11
    SUB      [13]
    JUMPZ    d
    JUMP     e
d:
    COPYFROM 14
    COPYTO   13
    JUMP     a
e:
    BUMPUP   13
    COPYFROM 12
    SUB      13
    JUMPN    g
    JUMPZ    f
    JUMP     c
f:
g:
    COPYFROM 11
    COPYTO   [12]
    OUTBOX  
    BUMPUP   12
    COPYFROM 14
    COPYTO   13
    JUMP     b


DEFINE LABEL 11
eJxTYWBgkPM+m/DEoyuu17UrLthZMFnS8U+WpCNHSbCzaK2Y3+uegIBrUwICWOZP8s9cNMvn3NICl3NL
gdoYHoQ+L5KKeF7EFXep6moqb9+GdIbWDeneZTtS+veC5EWKvMsuFW2onFCs2La25P2yQxUnuwXrjxZy
Ns1ubG2ePKm1uWvxnTbFNr4e2Zyu3udFNyevWcIwCkbBKKArAADEDD5s;

DEFINE LABEL 12
eJwzYmBgSA1RbAsIUGyr9W7rzHAvnMjpfm3Kfq/Hc7WDjFYeCZHdaB+Wuyc15NL+CN+qo29cZx8DamGY
nfx47uxk0UMgNlOidxmIVslRbOsoc263qzrZHV1dODG6um7W3Cq1BUvLd64NK/x8oKEy4ghnU8QRkNrv
BSzzvRtZ5oPYuT1GK6Unn1u6ZWrmIt8Z82dzz7SesWZG4cTQqSu6Hk5o69Tufd0zqWvL5Cudj+cad9lv
Cujm2MnWX3U0dhLDqS1TGU4lzoG4YxSMglFAOgAAlqJVpQ;

DEFINE LABEL 13
eJwzYmBgOGS7Jj7bqq3zlNW1Kcm2MTMrHOfPbvWomwWUYmCx7TMG0cqhCZqNoRsqd4fz9l2IVlvwNiZz
kUtC5qLIFIg6a5/nRReilxSA2JeK+iuel3mXSdYsKfhSK5uzqW5nxpfa94mKle4xz8vcYzTLuuJOlWqV
Li3X679f+WPq9ZqYmRlNMTPFOn9M/dg1eZJHX8CEI/2ve0BmLZ7dFVc3l6Mkcn7drANzUjuY5ojWus5u
yl88OzP27ly16LIFatFHl51NYBgFo2AUkAUAwWlWmQ;

DEFINE LABEL 14
eJxzZmBg0LQ2Skm25fG8b3fSrTLipNuNCD0v7aDp6avc9PpPWclutLNfsjXBi2PngUStXTtSOHaKpy7Z
ejdJcAVQK8MXp8/VyqGfq0HsGQUMrZI1HCXHm0JSGVvdYxa11IWqNVsH6zVIBmWXWgfvzXsUkZbdFfcq
833ip7SmfJO0S1WT0/T6+TOsZ3hmqS2QyTda+b5459qMplvrcnsEV2j3nlsKMvfd9IPh7bNEa5nmmB9m
mjO7UX7unyybeYLJO+Z3xZkszIz1W68WDVJ3fG9mrN7uhDyzHSu6zHawzC/YZbSyd3fpasYD09dMOmq0
suvEmiXKpw/OmX9m8qTYs6K18888LzpyqjSN7ZRRyp2DpWkMo2AUjEAAAKandMQ;
)hrm");

HRM_CODE.emplace("alphabetizer", R"hrm(
-- 36 HUMAN RESOURCE MACHINE PROGRAM --

a:
b:
c:
    COPYFROM 23
    COPYTO   20
    COPYTO   22
d:
    INBOX   
    JUMPZ    e
    COPYTO   [20]
    BUMPUP   20
    JUMP     d
e:
    COPYTO   [20]
    BUMPUP   20
    COPYFROM 20
    COPYTO   21
f:
    INBOX   
    JUMPZ    g
    COPYTO   [21]
    BUMPUP   21
    JUMP     f
g:
    COPYTO   [21]
    BUMPUP   21
    COPYFROM 20
    COPYTO   15
    COMMENT  2
    JUMP     i
h:
    BUMPUP   20
    BUMPUP   22
i:
    COPYFROM [22]
    JUMPZ    j
    COPYFROM [20]
    JUMPZ    m
    SUB      [22]
    JUMPZ    h
    JUMPN    l
j:
    COMMENT  0
    COPYFROM 23
    COPYTO   22
    COPYFROM [22]
    JUMPZ    a
k:
    OUTBOX  
    BUMPUP   22
    COPYFROM [22]
    JUMPZ    b
    JUMP     k
l:
    COMMENT  1
m:
n:
    COPYFROM [15]
    JUMPZ    c
    OUTBOX  
    BUMPUP   15
    JUMP     n


DEFINE COMMENT 0
eJxzZGBg0NFjcMjT3WsrrlVlzaRWZc2l3ObSp/g9QF9xdiOTms/uA2q8l4HKGNwlePtcVLdMtjT9MRXE
t7M/OCfaoWvxLoemLSA+j5VWKYjuCpo/OzVEcEVj6JKtR4D0kZCYmfPCePsuRFc1rI691/w0Xq8fpC65
wrl9Qe7J7leZhRN1Mq9N0cl8PHdF1rmlj3Kmr7mVb78pu/T59o6y/r3vi0UPVedHHOnOND8M0jepqysu
td96xvyJs4+B+E+aD86pm3twzt6VdbNAfLmDWyZHHH6+/coR0UMfj17av/zkrXUGl+41R93S2nXt9tFt
jo8h9n9/9vlA2POIIwyjYBSMYAAApapwaQ;

DEFINE COMMENT 1
eJzLZWBgSJE0SkmRPJvAK2YdfFn4mv8+/i1+nDx1oYKcpWnBHEsKgjlEa99w8fbJCjyeGyPTtOWvtM9u
f1Gf3TMFOXYm8C/ZCjSCQV1lzZI69fmzbTQkp8/WMKu7q/652kU1or5NZW+TukrhxKcqB+fs0OTYeVb3
8wFL088HNK19doP07XJ43XPI9mT3KavXPYYW16ZcMp8/29DCaGW2lezGQ7aJm687Jm5u9fiz/kyg4Arl
0K7FID2NoeeWNoaWrp4X1rTlRsSl/X0xVUfb4quOlqXm7glMX7I1L2Pn2ryMc0s/pbHMl0+qm/U2pm4W
W4jagq6gNUs8gktXV0bsXAsyZ2/elsl78x7PtSzQ2uVXcm5pR5naAruqulmb6mJm6jXUzeJsUluwqOXP
+tq2S/vvtN07EdGx9zhI39UFbZ1ZM1nmX5wWskp4ms/ui9Mu7eeeKXrIZp754bIFooe6l+TuaV7etAWk
1mRhaZrJwudFZxdfm/Jo+dFte1dq7QKJ8x0vnPjyhOCKh6dFD1Wd47387fzk6/U3QlbtvPVjKsfDeydk
nlrfvfV0xfkJL2+ty379YypIz/M3AVeXvt16g2EUjAIqAgCW9s1i;

DEFINE COMMENT 2
eJwLZmBg2GqWWy5izlGy1exW5gxTo5QFRl1xZ3W74n5pTE9nUntetFoptaNP8dxSLuVb604o22+SV+fY
eVXLZ3eagc/uCRZNWyQdBVcAjWGQ807tuOJ3bumZwD/rPYLtNymH2m+aF/Zn/d+w0tX2YQfnHAnZMlk7
aEVXqZ9i2x1v3r473oUTI3yvTQHp/ZXM0BqZktrxK/nP+tnJ7gt3pFybEpi+t+lVZm75gtzCiWGFVUfd
yidPmls1u3FqrfWM3ob+vSB9sZO8y/qnn3slPG32MeFpev3vpnOULJ69Jv7XvLpQk4WSQXmLJYO6l8SE
LFixM6N6VW75rVUR9bdWneyWWRUwIW2Z5PSrC+pmuc6um9U/XXI6yLy1659v79iYuBnEbr+aGet7uSuu
64RsjvFR7zLjo6K1HidPdivcEFxRflt2Y/ntJVt33uLYueSa+eGii7OPMYyCUTAEAQDXiZog;

DEFINE LABEL 15
eJwTZGBg2JsnWvs/99a6uLyuxVsL2zqflz0vul9pn7ur+nlRQf3Jbu9GtQWrGm+tq22bPOljV1XDy97+
Crb+z9XC07oWy881P8wwzAH3TJb57bMkpy+ebT1j8kKf3YGLNuzLW3xpP0hObd+SgoenjxYKX8wtf3dJ
tNb3cmpH6IVrU84cN1o56ej0NeuP7VyrcCNzEUit67WYEIUb7jHlt48WTrnzPjH/3qMIrwd1oY8elaYt
eOxdduvphkqQulXf60KnflaL/vNBMPnPh1uZgl//ZHl/35nx5Kdg8v7fatF3/kgGgdTd+bPXdv/vvbaM
v3k8//+3rB64EBoFQxkAAF6de3c;

DEFINE LABEL 20
eJzTZ2BgaItvyu+LmZ4uFRGS2hVUmrY+4E8WX6B3WWOoYpt8UszM2cks82cndy0+kBiyyif21rqvkbIb
G0NlN2oH3VoH1M7gmNvWaVmgtuBSUf/eW/mS0+PyOEqq8zlKDIsvVR2qcG7/UxUw4U/VlsmHKn5MjcuL
mQnSc71GLTq4zj3mSbNWqVjnwTkgselT7jVfnHZuKYi9vntNPIi+umCy79UForUbFtXNAvG5Z/ZX6Czp
rzi1QauUYRSMglFAMQAAUpZOcQ;

DEFINE LABEL 21
eJzTYmBg2O/1Y2qE74+pL4N4+5YHn+xeHszQ+jJob9Mk/71NEb6Kbfu9Cifu93o8V8wvcfMk/+fbzwRy
7JSKsN8E1MqwOvZkt3p84cS2+B9Tn8YLrmiLP9ntksDQKp8kWvsrub/CJK2/4mzG7EbH3J1rQeq/1K7o
Yqk42c1TOnmSX0nMzOxStQXPy7oWR1cbrdxUJ7gCpEaw3j43o8m5/Umz5HTG1lvr5NqObmMYBaNgFNAE
AAB75UpI;

DEFINE LABEL 22
eJwTZGBg2FpYmjajYE38/9xHEYHpjyIiU7riEhOnpx9I5CiZnHav2bD4x1TFSveFc6sEV9hV3VrnV7Jk
64yC59sjU55vfxvTtIVhFIyCUTBkAQCoMiF4;

DEFINE LABEL 23
eJyzZ2BgiHYwq4t2sM+1s3eP8XbLjG31WBPvESyYzBZyK5Mt5GihR/Dnar7AFV1X/H5MDXbesC/Ba+9x
+7B7J/6GVR21D3u+HWgEQ4IXy3y2ELUFIPbs5CUFV1MvVfFnNG2ZUdC0BSTmmTV/9q38mJkg9uQ0jpJb
+UcLQew3Dbcy1Zpzyxe1pHbMaj+6bVa7+eHehmtTzte3dZ6v31D5puF5kXNLU/6dtqb8+RNFax0mOrcf
6T/ZHdBdOHFRy7UpoVOnrwGZEzk/Ia97ieAKz6U71y5Y4bN778rcPZartXZlbwhZ9Xxj5qKlmwonam5s
6xRZa1YXt3JDJf/i/gqGUTAKRgEDADoEcK4;
)hrm");


HRM_CODE.emplace("digitexploder", R"hrm(
-- 38 HUMAN RESOURCE MACHINE PROGRAM --

a:
    COPYFROM 9
    COPYTO   3
    COPYTO   4
    COPYTO   5
    INBOX   
    COPYTO   0
b:
    COPYFROM 0
    SUB      11
    JUMPN    c
    COPYTO   0
    BUMPUP   3
    JUMP     b
c:
    ADD      11
    COPYTO   0
d:
    COPYFROM 0
    SUB      10
    JUMPN    e
    COPYTO   0
    BUMPUP   4
    JUMP     d
e:
    ADD      10
    SUB      5
    COPYTO   5
    COMMENT  0
    COPYFROM 3
    JUMPZ    f
    OUTBOX  
    JUMP     g
f:
    COPYFROM 4
    JUMPZ    h
g:
    COPYFROM 4
    OUTBOX  
h:
    COPYFROM 5
    OUTBOX  
    JUMP     a


DEFINE COMMENT 0
eJzLZWBgEFK413xDlqGVVXLL5Byxc0tzxOw3pUiaH7aXvnfiq9yK820qAVfzdAuv/Td6fXGGaerZrWb3
ToiYL9m619h6BrtBasdZ3b1NJtoR9bM1RGszlUVrb8jObrSXdm5/ILWiC2g8wx/750XRDqK1Zk56/b2u
55Za+/TvXR8w+9jy4HsnYsLvnQiJ+nxgdWziZpeEzEWLE6xnuCSc7G6LD5jgknBuqU3y5wNlqSvOf0oL
uAoyKy37UYRKjmDyhGKW+fcrRQ8F15280NvAe1mt+fXFrt4N+0BqrqayzD/T82PqgTl6/SD++u5U56YJ
7jGhUy9VPZt1bqn83LpZO+ZPnnR28cnuvSsV2/5snd24aadim97uk92r9qgtWLVHduP5Xbl77LbeO3Fr
1ckLnxa+vrh4ttJp7pnmh0HmWR/aMtn6kOzG0iOih3KPK51+eLrtnPS5tnPvLjGcsr3Zvzf/XunqnvuZ
i3ruW88wvXOye85N5/Yl1xhaq84xtE46uqKL8UDABOf9kyc92Rczc/8BlvnbD3ctBpnr/Z2hNfhLzMxD
755vX/p29jGQ2KNHEfVxTxTboj/eWtf6U/TQ//9m5xhGwSigIgAAGsnZHg;

DEFINE LABEL 3
eJxzZGBgEDHfazvBQrR2goXWLiCX4Wm8omNhtKJjV9BJt1K/a/7WPmrRjF4cJU88Xvc88WCZv8gzc5GY
X+lqvsBb6/RjlmydnLZk69HsnWsdcwVX7M07OOdWPkPrrXyOkgW5ZxNeZR4Mj0z5HgAyd1KXZFBER0yI
c0tmrHdjU35vw+fq3oaACd6NMTNbm9csqW0LWaXde2udxZSda9fMWLOkfdbBOYtnF05cPHtF18mZueXC
00JSHSayRJ3piQkBmae5MSbk1IazCe/XJeSB+P/O7MzIPR6SKnYkJJXxwK1M5/2yOc77m/ITDniXGR+t
alA+ndox/fyWycIXH8+dfr5rMd/xkFX3t4SsYhgFo2AEAwBWIXYC;

DEFINE LABEL 4
eJzTYmBgkHRkcJB01PNaahMz082GYydQiOFtjJ6XfdiaeI/go4VdQXr9y4PrZimHZi6KCTdaWRi9c21g
+q11rzJLVzfnqC1oznFuP5q9oTIv42zCpzT3mNnJ88Pa4utCQeYwtnbFObfcyuRsyi0H8RPn2OdqzLLP
/TaFo2T+RNFah4lVDdOnpHYcmPNj6ux582dHzs9c9GveuaWJc6avCZ26ZOv6bq1djK1auxhGwSgYBTQB
ANnkRxE;

DEFINE LABEL 5
eJyTZGBgkE+65n83ySjlbtL0NfJJz7cDhRiyS78HnCqNCckuPVoI4i/vs8/t6v2TdaXzVub+VtmcjCat
0lWNEfVqzc7ttW28fTcnqy24OM1o5cVpO9cKT5PdKD15ydYj/Ue3fak9uo1hFIyCUTBoAQDmTS5C;

DEFINE LABEL 9
eJzzZ2Bg2OVgn7vLwTrYzOl7wBvX7wFsIWrRR0IyY4+EhKSyhcjmTPK/VMXoldphaPFn/S0TrV0zTPv3
zrUXPeTsaX74b5joIaARDLdMtky+bzd50hU/yekg/uKEkNS6pM/V4qmihywLLu1fW5K7ByT+KU1tAX/G
47lxeTEzQfwDiYLJk9PWxBsWZ8aC+IL1LFGtzUcLa9smT7JuD1ll3Z569k7byQtybW3nWptvrctoqpuV
0cTbd7xpbxNjq33unbaQVLHOswlNE9bEf5simHxyZlN++yzvsvZZEfXvprd1WkzR60/t3zI5oTVm5p5J
Iasi52/YB7Jn6xrvsq1rePsM182f7bZp+probUe3ee95vv3OQftNEYdvrft49NzS9cdY5vMdl5y+/tiK
rlmHcssz9pamFexaEx+97WzCrVW55QyjYBQMAwAAICSRZw;
)hrm");

HRM_CODE.emplace("recoordinator", R"hrm(
-- 39 HUMAN RESOURCE MACHINE PROGRAM --

a:
    COPYFROM 14
    COPYTO   1
    INBOX   
    COPYTO   0
b:
    SUB      15
    JUMPN    c
    COPYTO   0
    BUMPUP   1
    COPYFROM 0
    JUMP     b
c:
    COPYFROM 0
    OUTBOX  
    COPYFROM 1
    OUTBOX  
    JUMP     a


DEFINE LABEL 0
eJxzYmBg2OWwM2OT89HCUr+uxR7BR7elhnDs/BopWisU/ScLKM2gkjO78Wh27p4FubOPyeTvPf69IOLI
2pLEzW7l7guTK65NSa5Y0fW8rKrBr+RS1d683HKQHpGi3PLnZfNnR1fbb+pt2HtcrPPxoyuddfevdPJe
Bsnz9eh58fVkxr7sFa1tmvBjauhUrV2+M5ROg+Tq5m6ZLL5g59oNizbsW7G07dzelZJ3wta8vnh/i/WM
6G3uCwt2zT7mvefkhYy9Jy+A1DedjplZdc56RuiFLZPfXWrr9L3M0Op7uaqh/5JZHdsp3r6PR+tmiR15
v0zsyNFtXSdSzzadDrh68aL13XP3Yu4dfWh99/1LvSsMo2AUjFAAAFRNhaY;

DEFINE LABEL 1
eJzjZ2BgEJ6W6uzRZ5TypNm5fW9e1dG4vM8HgMIMMwpeu68tcY+xqzpayNm0omt995olW6Zy7Jw9b/Yx
xxWvLzKMglEwCoY8AABinhtj;

DEFINE LABEL 14
eJxzZmBgcLO51zzX/l4zWwhvn33Y5Ekx4TEz54X9WZ8acmn/+oAV5+3DXl8UTz15AaiUQdBl+porfrIb
mRJlN+pk3lp3NHv6GpD4qdI1SzKa1BY4TLw25eZkvf5vU5zbp0+51xw7qaoht2d2o3PL657z9dYzBOsP
zhGsN1pZUH9rnXfj8+0JraKHrnTOPlY1efax2fNy92xYtGQryLxHywsnxq1M3By30vnM3pWzjy1YEbJq
wYotk/eufN2zdY1z+/t1im2KWxTbMva2dYLU1x5UW2B8tGnLmeO5e46c+nxg+nnRQxcvbth38Pqf9b9v
Cq6QuH1wjsTta1OibhVOvHfjdY/rtdc9AccyF5Ue6VpcezBkVe3BnWsZRsEoGIEAAH68jIA;

DEFINE LABEL 15
eJzjZmBgWB27wcQnNtV5WuT0dPNIjpLZyaK1hsV7m4BSDDylG0yyS312Py+zvstAZSCTzzJfpOjP+lOl
z7fPrfLZbVbrs1uv4fn2J8231jG2Cq540hwzk9p2joJRMApQAQAekiJg;
)hrm");








HRM_CODE.emplace("sorting", R"hrm(
-- HUMAN RESOURCE MACHINE PROGRAM --

a:
    COPYFROM 24
    COPYTO   20
    COPYTO   21
    COPYTO   22
    COPYTO   19
b:
    INBOX   
    COPYTO   [20]
    JUMPZ    c
    BUMPUP   20
    JUMP     b
c:
    JUMP     h
    COMMENT  0
d:
    COPYFROM [21]
    COPYTO   23
    COPYFROM [19]
    COPYTO   [21]
    COPYFROM 23
    COPYTO   [19]
    JUMP     l
    COMMENT  1
    COMMENT  2
e:
f:
    COPYFROM [19]
    SUB      [22]
    JUMPN    g
    COPYFROM 22
    COPYTO   19
g:
    BUMPUP   22
    SUB      20
    JUMPN    f
    JUMP     d
    COMMENT  3
    COMMENT  4
h:
i:
    BUMPUP   22
    SUB      20
    JUMPN    j
    JUMP     k
j:
    JUMP     e
k:
l:
    BUMPUP   21
    SUB      20
    JUMPN    m
    JUMP     n
m:
    COPYFROM 21
    COPYTO   19
    COPYTO   22
    JUMP     i
    COMMENT  5
    COMMENT  6
n:
    COPYFROM 24
    COPYTO   21
o:
    COPYFROM [21]
    JUMPZ    a
    OUTBOX  
    BUMPUP   21
    JUMP     o


DEFINE COMMENT 0
eJybycDAoCWuZfBB5JbKNqFSxZmCO5RbeRkcjvMEeLfy1oUu4nsUYSWYGSsq3BUnr/4+MVLTKMVFNSHv
reLzonnSueXLJHLLecW0SoHGMPDrBnhbmv7JKjY/WnjKKrc82XZnRrQDj6e3G0cJo1dVg5z37MYI34Q8
Mb818WJ+K1xBegqjt/idiDub4JKQW56Y6B4TmXLSbXLabJsN6ffs2LO/B+zNW1IAUrc4oSuuOYclam/e
wXAQ3618ts3cqmv+12v+ZF2veV70p2p+2J+qe3a7qkUtvtR6G+o1aBkktDI4yLUFeDu3SAadrz8YDlID
0rtDs25WolrMzK9y7gsPS5auZpX8s75RqmlLiLzWLkcj88NxxlVH9xrfO/HfKPWsiuHJCxt09K7UqQdc
Xa108gJIv6b147n37Zq2/LHfe/yLk9LpWT5Kp0HidvY7165yK10t5y24AsRXtKubtcl5y+RFnnr9ID5X
nPtC80j3hY2hgiuUQ7V2/Q2bfWxapPMZ9fgV58VTV5z3zEo9+z/33gmQ2u8FP6ZeKno8l6f01rqOsogj
a0su7QeJb0hnme+ZdXDO2pKDc87Xx8wEif1svzZle4fk9IDuP+s9+kQPgcRYKmYfk2uLOFI0rX8viL+p
bstkg+kQd3QviZnpudR9oefSnWvTljVtebR8yVaRtU1b7m95v0xy++O5Fdt/TD20mbevY+PJ7hmrT3an
LQuY0L1ky+SziyWnn108fzbIjCOn7p1YftJo5fKTJ7sdzkye1H41cbPtzdw9pnc+H8i/9/lAz/3+vV4P
jm5b+SBgAsMoGNQAAJNBCqQ;

DEFINE COMMENT 1
eJzLYWBgMJfLNbKXfq7rLnFU57LwUZ2Zghz6NQK5RrICs21+CPJ4+otaB9+QFUw20V5S0K3nXZan218h
ry5aqyVuVgfUzpBmoOgYZpYZu9RGq3Sqk2LbJudLVYIuel6LPI1S+AI/V88La+t8G3NtClfctSmZca97
MuOa8rniuuL6Yng8Qfof5UgGLcid3dicYz2DPftk94qsqgb27OnpC3LrQqvzr/lvLdzip1lWF3q+fklB
Quvepp/tev0Qffa5IkVN+RU1Tfnejfa5IDG+nsm+y/tiQr5Nkc3hnrm3qX1WwIQ9k0rTHCbWhc6f2OZS
NVnRsWiaouPsedf8Py1kiQpclBnrOlswma0/JBWkn0mNY+cvjdnHxLXazk3WVnu6QSfzOY/V40f37R4/
AsmDxG+ZOJ/RtJ59DMR3UU3cnKf7Z32H9bmlIP5H/8ePjP1z9/AFbtg3LfLkhbokyTs2ydduRqYonY5M
+XwApKY7896JvXkx91RylE4fzY44cjT78wHHXJ/dWwufbz9U8Xz71FqtXYytVUcjOl5f/Nl+7eb+Vuu7
rc1197/U1t1nGAWjgMoAALCmtRI;

DEFINE COMMENT 2
eJyrZWBgmCe9xW+etFHKA6ncciUprdIHUtPTb8geDO9TvOa/WPWa/111yaAyLbXoQJ2dGfy6RwvzdF/3
bNBp69yg05R/Vlcwudlwi1+csZ7Xd1M9LxbbutBoh644M6em/KlOEfUFLnubgFYwLA++5n8kpKoBxL7i
J6sOonektLkEpk/2zcs4GK6TyVGik/k+0TNrflhzToD3rfwVrmGFJ91YKtSi51ZNT99V/bxoV/WGSpA+
R6M1S3T03Bde1XJfeEDt3NJEtelr5NXtN5Vp9e99ZOh8Js749cU444CrRw0mX7+qNfk6lzLv5T7FkxdA
etU8cvcsD7617kbEmiUx4ZmLJvlnLtrvdW7pE4/S1ZzuPrs53Z3PPPF4ffGOt96Vl0F6VxpDX1+0D1tx
HqSXP8No5dP4JVsvRIse2hzFcGpzlN6VC9Fbb2TG/bj1KU3yzqOcrTdEingvg9Ter9wyeW7V64sg9t48
+012VUu2vmlYsrW2LXEzSGx9d+nq3J4/67t6lU6D+AHdK7pA9Ox5IavaZyVu5p6Zu6d/+uTrJ2dOvn5g
Du/lyQtnHzu7WGvX2cWyG/kXT19zdcH7Zc9mrVniO8NoZf90+00gve/XTV+zdj3HTp4Nk6/zbKg6qrnx
+XaWzaWrK7Y/niu4q26W957Hc2sP9u9lGAWjAAkAAPki4Sc;

DEFINE COMMENT 3
eJyLZWBguCzcpGUkImqhJS6YrCXelH9ajCWqU/ya/2FJZ6cYmb22FxR4PJ+qqEUnqiXk3VXPjN2haR2s
o5fqzG5wz07F8J7dJfM18X6WSwqARjEcd1/husjzUYSc99FCEH+u/Q5lEM0WwuPZGHrNf3f4+8TKiNxy
88iuuMLoa/6LE+7Z2SRHWJmkmVk+yin0qc4/GH6qdHYjSM8yiYMPLyhY39XRO3lhq5n54QkWPruLzRM3
xxmHrOrWE1xhoi24ok1l51p9xaPbvsqJHjKXSz1rLvf64gWFuvttKpnPZ2u8f52na/RugoXROzeb96+T
bde8BJm7y+HWuk3OqWf1XI3eFbi8fy3oMvn6eRel095uEUeeeGjtEvO7tU47yGilfZjgCqkIo5VfI3eu
9Ynt37s4IeDqr+Rbn0FmRFcnbl5borWrOj/iSFye85m4PL0rt/Kv3ewoW/DAuWXBg65eyTts/ddusvUX
XgOp720QrVVrnr5mUpfRO4ZRMAoGEQAAXqqbPQ;

DEFINE COMMENT 4
eJxrYWBg+KVRZT1b47NZopq34VOVPmNzuUKfB1LuMQ+kBJN3y0xPz1ReUlCnzlHyS2NDZaTmvebZGs7t
bSptnT5KbZ36is7tQCMYTlmVpmVbHS3UtN5QmWwrWivoYlaX4f65er8XR0mt963MRZ5dcYIuXXHJttPT
Fe3+ZIH0+MTGhLgkPC86kHiy2yXhUtXT+OnpiYksUb+Sr/nzZ/B4Hs1+7R6XFxNiWXArE6T+eZmio1mt
bA5j67UpEH5C3pfahLymCdPTQXw7e9FDmtbmh99b3DsxwcL5DI8V7+Vk2603Erwyn8t5r3nJ6HXu1XmX
NS9BaqUiCq/5xK44fyLu3onMuM8HpCJEDz0InX3sQWjbOakIyTtvY9yfuSR0v5ic1v3iVebjRyA9IkWz
j/3P3Xu8OaftXHPO5OuOuTH3ZPLVnooUZT7PLs18Llmz4EFwneQdkFrB+jVL1JrNDye0/ri1vWP6x5/t
Ie9B4tHVqWf1Glacn9S14vyeSQynQGL/Jm7YJz350v5vU2Yf65+ud6V9luBb3xkQOx9OMFo5f2LpahD7
7lznMxsW/bh1dBnLE8vVa15mbzj3imXz40cNW15fPLTZZ3fHRvtN2RtkNxqus980YzXHzublEUc8l644
77lU7wpI/6adshvlDj5+NOvQggeTjvJe7jrBcCr1VNXRf2dED1WdEz108vb//wyjYMQBAGEr8w4;

DEFINE COMMENT 5
eJyLY2BgmKzN4NCmYmbpo2RmGSLv7PRVLsA7RJ4lqk8xJLVNpSm/TGtDpaWpYluYWWpHmNnJ7lsmr3te
6Z3sXq10qQqoneG649kEScf+CjOn2Y3BznubzrtUNWS4J+SB5L44Ffpcdyz0SbadH9ZhLZjsZlOaBhL3
CD4YfiPic/WD0Gv+D0JXuLbFzw8rSzVKAcmlZV8ytatq6wSxj2Y35f9sL02bP/FsAljOwPnMfyPey5fM
C6/NtS+85u3Ge/mO970T1j79e+94c+zc73V0m7fb8+0d1lVHs62cz2Rb6V05ZbX1xiFb92ebnHd+svax
/54aIvstJjzkPci8kKjZx07ELXiQmGj/nSnR+m5iYuE1m+S2c5PT9h6Xyb+0/1LRpf1u5XuPb6pb8KC3
Yc1L78bpH0H6mObIboydVHXUo+/1xdJO92cRHSHvt3cc/W3cpfXfYaLWf4Pptz5zzxR8C1IbOylgws3J
XYs1ZjGc+jWv7r7Lyv//GUbBKBgkAAA8uaYS;

DEFINE COMMENT 6
eJwLYWBg2GpmHVxtEhMSZ6wW/d/obEKc8YbK76apHe8tCieesro25b6d9YxVbtemPPEonPjTx7k9IEA2
JyDgbMIk/4PhEb7WwYxe1sGa1mcTgEYxMCXyeM5O3pnxKY2hVSczYEJzjuT0GQW8fcVFzu3JFfa50dVq
0buqP1dX1LR1FtRnLgLpedmboOnRV2XtMLE0rWpyRP2aGbfWrZlRuhokJ9Z5tLBo2tFCnSWyOSB+9ao6
AcvVe207Nl6bkrw5dw/L5sxF97dYz6jYPnnS+V0BE9T2BUxYtH/ypJ+HMheVHrm17sqR/r2lR0QPzTrE
cOr4XqXTervvnQCZM+cmy/ydt9YsKb/NMl/i9o+pv2/y9i25trcp60pVQ9HFqoZv5/c2SZ9r69xztnDi
zXM/poZekJwO0nf9k1bp5zt/1kPMqGroub9lstvbxM2K7zl2Nrzv38swCkbBEAMABBCfHg;

DEFINE LABEL 19
eJxTZWBgUA6dnv4g9FZmTHhueWWEc3tlhOR088i9TYXRf7LexrxP5IpbE5+YaJ97N8m7TDxVr1889doU
kzTFtrMZz4sW5GbGWhYIJmuWidYuLXduBxrHcL3GPUavYWfG8SbRWhA/uWKvLYi+0vk94OGE6elVkzlK
QqfubUqc8ydLfMH7xLzFXXGOK5ryDdet6OrY+HguwygYBaOAbgAAIZY+eA;

DEFINE LABEL 20
eJxzYmBgeBA6P+xMYJtLQICio5jfPTs573t23m5tLsHOAd5/7OtCD9kapRyylc1psFtScN3xc3WGu2Lb
fq+2zjOBLPOXB79ftjzYflOp39FtX5yObgsz+7M+zjhkFdBYBvbsnRlp2UcLdTIDJvBnlK4GiS3ITXUG
0YcqMmOvdBqlxE66lek7Y0lB1sznRRqz+itOznRuD516bcrDCSzz5dr+rK+oeb7drur59hVLfXaD9M1q
P9ntMPF1z6eFJ7tB/PfrJKe37lNsm3Uooj7i8OdqsSNN+daHbmUu2j89/fr2nRlum5YUdGzkKMne8Lk6
e8PrnlMbCifO3fp+2ZcdIavU9oWsij17bqnGVfeFDKNgFIxQAAD0EHyB;

DEFINE LABEL 21
eJzjZmBg+F7wJ8uyYEWXZYH1jO8F82evLWGZH13tvnBqLct8zqYtk4FKGDyzzCwZqAD+TbyV6dF3K3NV
I0fJmwbvst6Gz9W9DakdqxoDJji3xMzc33pwjlin2gLt3sxF/yZmLro47fFckL5XS1I72JetWcK+bMlW
z6WTJ7EvU2xTWT678f8Ks7oZq0Vrl24Srb2+PaJ+6g7FNmq4cxSMgpECAB25Q38;

DEFINE LABEL 22
eJwTZGBgEKxvynduMau70nltSm6P0cozPYmbc3su7f/YVXV0VrvS6SfNqWe/1KaebahUOl1cFHFka6Ho
oa2FG/YBtTKIFP1RZaAhsFzts5uW5o+CUTDSAQC1dSKt;

DEFINE LABEL 23
eJzzYmBgeOLRX3Hc3bl9rv3nA8m2W28k29bdb7A7+FDS8fGjL06PHx13t74LVMYgYm6/qdbbfpNy6K11
IP6FaPPDd5Mu7b+V/2c9T+maJTyl7gsNiw/OWZB7cM7V1HNLmRJvrTsRt2SrT+yl/W9jIo74xK44X5dU
d39ymvuzV5ndL8IKu1+cKlV7er+y7j7IvDcNRis5m8wPczadvJDRtPd4Qqv9pjttpavFOo1Wfuzaubar
V2tX04S2cw4Tt96YPzH1bOjU6WvWzGCZf3Imy3zxBf17AxfxXgaZU7xWbYHbpv690dvcnzHs+P8fJMYL
pO9vefzo0Gbey26bfHZ3bFRbwLL58dwvOx7PVdvXtdj6UOnqWYdurbtzMHHzm91au55v/Hzg/wrRQ+zL
Nux7tcRnN8MoGAXDFAAA6F2bQQ;

DEFINE LABEL 24
eJyzZWBgYLG95p/hXhda6jc/bJL/wfCP/u4xinYxM5farFmSbFu6+r7dzrXBzrfW1XrfWlcZUbp6dvKa
JUBtDGd1+ys0rT9Xv3H9XP3TJ7f8b5hWKUicp7StM7huQ+Xxpqb8J80hqcebuuKC69bEs1TcyjQs5igJ
KxStnVGwoiuscPKkCcUxM+9XZi5KaF2zxKPPfeG/iY/nXpx2bQrInDUzNlTKzy1dnTinrfPuXNHaHfNz
y/kXHy3U3Pi8qGNjbjlIjeT2LZNX7Tm39M5B2Y0fjyZudjhjv2n6+dLVFy+6LzS4NH92/6VLVRcvHi2c
fj4h7+FprdKEAzEzGUbBKBgFcAAANL52lA;
)hrm");
