/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/**
 * \author DeepNet42
 * \file parser.main.compliance.cpp
 * \version 0
 * \date 2022
 * \copyright (c) 2021 DeepNet42.
 * This project is released under the Mozilla Public License v. 2.0. 
 */

#include <iostream>
#include <iomanip>
#include "lexer.h"
#include "parser.h"
#include <errno.h>
#include <cstring>
#include <chrono>
#include <string_view>

using namespace hrmtk;

void usage(std::string_view processName);

int main(int argc, char* argv[]) {


	if(argc < 2) {
		std::cerr << "fetal error  : no input file." << std::endl;
		usage(argv[0]);
		return EXIT_FAILURE;
	}


    bool exitStatusGood = true;
    for(int i = 1; i < argc; i++) {
        std::ifstream file(argv[i]);

        if(!file) {
            std::clog << argv[i] << ",OPEN_FAIL,0\n";
            exitStatusGood = false;
            continue;
        }

        Lexer lexer(file);
        Parser parser(lexer);

        auto startClock = std::chrono::high_resolution_clock::now();
            bool good = parser.parse();
        auto endClock = std::chrono::high_resolution_clock::now();
        

        
        std::chrono::duration<double> exectime = endClock-startClock;
        std::clog << argv[i] << "," << (good?"PARSE_PASS":"PARSE_FAIL") << "," << std::scientific << exectime.count() << "\n";

        if(!good) {
            exitStatusGood = false;
        }

    }
	
    return exitStatusGood?EXIT_SUCCESS:EXIT_FAILURE;
}


void usage(std::string_view processName) {
	std::cerr 	<< "usage: " << processName << " source_file\n";
}