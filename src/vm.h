/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/**
 * \author DeepNet42
 * \file vm.h
 * \version 1
 * \date 2021
 * \date 2023
 * \copyright (c) 2021 DeepNet42.
 * This project is released under the Mozilla Public License v. 2.0. 
 */


/**
 * \brief VM is the heart of running hrm code.
 */

#ifndef __HRMTK__VM_H__
#define __HRMTK__VM_H__

#include <vector>
#include <array>
#include <list>
#include <functional>
#include <stack>
#include <random>
#include "is.h"
#include "lexer.h"
#include "parser.h"
#include "vm.config.h"

namespace hrmtk {




/**
 * \brief	MState (Machine State) determines how VM will behave and transition giving a specific 
 *			set of rules. 
 * 
 * 			<br>
 *			| STATE | Description
 *			| ----- | -----------
 *			| READY | code has loaded and the machine can now execute
 *			| EXECUTING	| in the process of code execution
 *			| IDLE | machine has finished execution (same as HALT)
 *			| RESET	| a machine is ready to restart execution (same as ready but code reloading not necessary)
 *			| INTERUPT	| usually some sort of an event caused a machine to enter non user code to process an event or error.
 *			| UNRECOVERABLE_ERROR | as the name implies. Aborts execution. A serious error
 *
 */



enum class MState {
	READY,			///< Machine is ready to execute code
	EXECUTING,		///< Machine is in the process of executing code 
	IDLE,			///< Machine is halted (usually due to finishing execution)
	RESET,			///< Machine has been reset but not ready to execute
	INTERUPT,		///< Machine needs to process errors and or external events
	UNRECOVERABLE_ERROR	///< Machine is in a state that cannot be recovered (e.g. floating point error, memory access violation, etc.)
};





/**
 * 
 * \brief 	Classifies errors by type
 * \remark	There can be other kinds of specific errors but they are grouped under a general 
 *			umbrella error. This can change in the future.
 *
 */

enum class VMError {
	REGISTER_EMPTY,				///< Tried reading from uninitialized register (empty hands)
	REGISTER_RANGE_ERROR,		///< Tried to write a value to a register outside of the range bounds
	MEMORY_EMPTY,				///< Tried reading from a uninitialized memory cell (empty cell)
	MEMORY_RANGE_ERROR,			///< Tried to write a value to a cell outside of the range bounds
	MEMORY_PROTECTION_FAULT,	///< Security related. Bad boy!
	DIV_ZERO_EXCEPTION,			///< Tried to divide by zero (div, mod instruction)
	BAD_INBOX_FILE,				///< malformed inbox or values out of range
	CODE_PROTECTION_FAULT,		///< trying to jump outside code area
	CODE_UNKNOWN_OPCODE,		///< malformed, corrupt, or unimplemented instruction
	BAD_MEM_FILE,				///< malformed memory file
	TYPE_MISMATCH,				///< not used. we allow operations on different types for now. [feature?]
	STACK_EMPTY,
	STACK_FULL,
};



struct Stats {
	long cycles;				///< each instruction takes one cycle by definition (labels are not instructions)
	double time;				///< real time it takes to execute the whole process
};







std::string toString(MState mstate);		///< utility for debug
std::string toString(VMError vmerror);		///< utility for debug

/**
 * \class 	VM 
 * \brief 	we can both run hrm code or allow stepping of the code line by line.
 *			Two use cases:
 *
 *			\code{.cpp}
 *			Lexer lexer(sourceFile);
 *			Parser parser(lexer);
 *			VM vm;
 *			vm.setMemory(memory);
 *			vm.load(parser.getIR());
 *			vm.setinbox(in);
 *			vm.run();
 *			
 * 
 *			//The above will link memory and inbox to a process. The load will accept a valid
 *			//program. Results can be extracted by calling vm.getoutbox();
 * 
 *			//The second use case goes like this:
 * 
 * 
 *			Lexer lexer(sourceFile);
 *			Parser parser(lexer);
 *			VM vm;
 *			vm.setMemory(memory);
 *			vm.load(parser.getIR());
 *			vm.setinbox(in);
 *			while(vm.step()) { // will step through line by line
 *				// your code here
 *			}
 *			\endcode
 *
 * \warning	VM is not meant for production use. Running untrusted code requires more hardening.
 */

class VM {

public:


	VM();

	void run();
	void run(std::list<PIR> p, std::vector<int> inbox, std::vector<int> memory, std::vector<int>& outbox);
	
	bool 	validatedRun(	std::list<PIR> p, 
							std::vector<int> inbox, 
							std::vector<int> memory,
							std::vector<int>& outbox, 
							std::function<bool(const std::vector<int>&, const std::vector<int>&)> validator
						);

	bool load(std::vector<INST> p);
	bool load(std::list<PIR> p);

	void setinbox(const std::vector<int> in);
	std::vector<int> getoutbox();


	bool step();

	void setmemory(const std::vector<int>& memory);

	void mem(int address, int value);
	int mem(int address) const;

	void enableTrace();
	void disiableTrace();

	Stats stats();
	
	MState machineState();

	//void snapshot(int cycle);

protected:

	void fetch();
	void decode();
	void execute();

	// management functions start
	void reset();
	//void init();
	// management functions end


	// vm functions start
	void inbox();
	void outbox();
	void copyfrom();
	void copyfromi();
	void copyto();
	void copytoi();
	void add();
	void addi();
	void sub();
	void subi();
	void bumpup();
	void bumpupi();
	void bumpdown();
	void bumpdowni();
	void jump();
	void jumpn();
	void jumpz();
	void halt();


	// extended

	void mul();
	void muli();
	void div();
	void divi();
	void mod();
	void modi();

	void rand();
	void push();
	void pop();

	// extended

	// internal
	void unknown_instruction();
	// internal
	// vm functions end

	void mguard(int address);		///< memory guards an address out of bounds for direct instructions
	void mguardi(int address);		///< memory guards an address out of bounds for indirect instructions
	void cguard(int address);		///< code guards for jumping into illigal  code segments (bounds)
	void rguard();					///< register guard for overflow and underflow and empty


	bool check_memory(const std::vector<int>& mem);		///< checks if the memory has all legal values
	bool check_inbox(const std::vector<int>& input);	///< checks if the inbox has all legal values



	void printMem();				///< utility
	

	std::string generateFullErrorReportWithState(std::vector<std::string> messages = {});
	
	
	bool staticCheck(std::vector<int>& checks);

private:
	
	std::array<int, MEM_SIZE> mem_;
	std::array<int, MEM_SIZE> mem_original;

	int reg_;
	int r_;
	int const r0_;
	int const r1_;

	std::size_t ip_;
	INST ir_;

	OP op_;
	int oper_;


	std::size_t inp_{ZERO};

	std::vector<INST> program_;
	std::map<std::string, int> labels_;
	std::vector<int> inbox_;
	std::vector<int> outbox_;

	MState mstate_;

	long cycle_{ZERO};

	bool traceCode_;

	double time_;


	std::stack<int> stack_;
	
	std::random_device rd; 
    std::mt19937 gen;




};


}

#endif
