/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
std::map<std::string, INBOX> INBOXES;


INBOXES.emplace("mailroom",INBOX{1,2,3});
INBOXES.emplace("busymailroom",INBOX{1,2,3,3,2,1});
INBOXES.emplace("copyfloor",INBOX{'B','U','G'});
INBOXES.emplace("scramblerhandler",INBOX{1,2,'A','B', 'X','Y', 420, 69, 1, 1, 99, -99});
INBOXES.emplace("rainysummer",INBOX{1,2,0,1,2,-1,-2,-2,3,3,9,0,5,-5,0,-1,-1,0,-99,99, 123,321});
INBOXES.emplace("triplerroom",INBOX{0,1,2,3,0,0,0,'A',99,-1,0,0,0,0,1,2,3});
INBOXES.emplace("zeroexterminator",INBOX{0,1,2,3,4,5,6,7,8,9,-1,-2,-3,-4,-5,-6,-7,-8,-9});
INBOXES.emplace("octopliersuite",INBOX{0,1,2,3,0,0,0,'A',99,-1,0,0,0,0,1,2,3});
INBOXES.emplace("zeropreservationinitiative",INBOX{0,1,2,3,4,5,6,7,8,9,-1,-2,-3,-4,-5,-6,-7,-8,-9});
INBOXES.emplace("tetracontiplier",INBOX{0,1,0,1,-1,0,0,-1,-1,-2,-2,-5,5,-5,-5,5,10,0,10,-5,-5,-1,-6,-7,7,6});
INBOXES.emplace("subhallway",INBOX{0,1,2,3,4,5,6,7,8,9,-1,-2,-3,-4,-5,-6,-7,-8,-9,0});
INBOXES.emplace("eqaulizationroom",INBOX{0,0,-1,1});
INBOXES.emplace("maximizationroom",INBOX{1,1,1,2,2,1,-1,-2,-2,-1,1,-1,-1,1,1,0});
INBOXES.emplace("sortingfloor",INBOX{10,9,8,7,6,5,4,3,2,1,0,-1,-2,-3,-4,5,-6,-7,-8,-9});
INBOXES.emplace("absolutepositivity",INBOX{1,1,1,2,2,1,-1,-2,-2,-1,1,-1,-1,1,1,0});
INBOXES.emplace("exclusivelounge",INBOX{10,-9,8,7,-6,5,4,-3,-2,-1,0,-1,-2,-3,4,5,6,-7,-8,-9,1,1});
INBOXES.emplace("vowelincinerator",INBOX{'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'});
INBOXES.emplace("duplicateremoval",INBOX{'A','C','D','E','F','G','A','C',1,2,2,'C','Z'});
INBOXES.emplace("alphabetizer",INBOX{'A','P','P','L','E',0,'R','A','G','E',0});
INBOXES.emplace("digitexploder",INBOX{123,321,555,1,2,129,649,999});
INBOXES.emplace("recoordinator",INBOX{0,11,15,20});
INBOXES.emplace("sorting",INBOX{14,13,12,11,10,9,8,7,6,5,4,3,2,1,0,'H','U','M','A','N',0,'C','A','T'});

INBOXES.emplace("countdown",INBOX{10,100,999,0,-10,-100,-999});
INBOXES.emplace("cumulativecountdown",INBOX{10,9,8,7,6,5,4,3,2,1});
INBOXES.emplace("threesort", INBOX{1,2,3,-1,-2,-3,9,0,-1,999,99,55});
INBOXES.emplace("zeroterminatedsum", INBOX{10,9,8,7,6,5,4,3,2,1,0,-1,-1,-1,-1,-2,-2,6,6-8,0});
INBOXES.emplace("modmodule", INBOX{10,5, 5,3, 7,4, 7,7, 0,2, 4,7, 999,88});
INBOXES.emplace("thelittlestnumber", INBOX{1,2,3,5,0,5,5,8,9,2,0,-1,5,5,0,5,5,-1,0,5,0});
INBOXES.emplace("smalldivide", INBOX{10,5, 5,3, 7,4, 7,7, 0,2, 4,7, 999,88});
INBOXES.emplace("stringreverse", INBOX{'R','E','V','E','S','E',0,'U','F','O','T','O','F','U',0});
INBOXES.emplace("fibonaccivisitor", INBOX{10,55,5,100,200,300,400,610});
INBOXES.emplace("multiplicationworkshop", INBOX{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20});





