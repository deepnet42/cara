/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/**
 * \author DeepNet42
 * \file cara.validator.vmerrors_detection_internal.cpp
 * \version 0
 * \date 2024
 * \copyright (c) 2024 DeepNet42.
 * This project is released under the Mozilla Public License v. 2.0.
 */




#include <iostream>
#include <functional>
#include <vector>
#include <algorithm>
#include <random>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <stack>
#include <climits>
#include <list>
#include "vm.h"
#include "lexer.h"
#include "parser.h"
#include "common.h"


using namespace hrmtk;

using INBOX = std::vector<int>;
using OUTBOX = std::vector<int>;
using MEMORY = std::vector<int>;
using FLOOR = std::vector<int>;

constexpr const int E = MEMORY_EMPTY;


// QUICK RIG
struct TestCase {
	std::string name;
	std::list<PIR> ir;
	INBOX inbox;
	OUTBOX outbox;
	FLOOR floor;
	VMError expected;
	bool pass{false};
};



int main([[maybe_unused]] int argc, [[maybe_unused]] char* argv[]) {

	std::cout << "Cara Default Internal Validator\n";
	std::cout << "VMERROR detection test\n";
	std::cout << "-------------------------------\n";



	std::vector<TestCase> testCases;
	
	// ref
	/*
	struct PIR {
	int				statementNo;		///< order is determined the way it appears in the source
	std::string		statementLabel;		///< extracted label name from the source for the line number
	Instruction		statement;			///< the actuall instruction
	std::string 	parameter;			///< extract parameter that follows an instruction
	bool			isLabeled;			///< is this statement number labeled
	bool			needsTranslation;	///< is deferred translation needed for a label location
	};
	*/
	testCases = {
		TestCase{
			.name = "Forcing Unknown Instruction",
			.ir = std::list<PIR>{{0,"",{OP::UNDEFINED,0},"0",false,false}},
			.inbox = {99},
			.outbox = {},
			.floor = {},
			.expected = VMError::CODE_UNKNOWN_OPCODE
		}
	};





	{
		
		std::size_t testsFailCount		= 0;
		std::size_t testSuccessCount 	= 0;
		std::size_t parsingErrorCount 	= 0;


		for(auto& testCase : testCases) {

			std::cout << "Running test named " << std::setw(50) << testCase.name << " ... \n" << std::flush;
			

			try {
				VM Cara;

				OUTBOX outbox;
				FLOOR memory = testCase.floor;
				
				std::cout << "\n\n\n";
				std::cout << "TEST START\n\n\n";

				Cara.run(testCase.ir, testCase.inbox, testCase.floor, testCase.outbox);

				if(Cara.machineState() == MState::UNRECOVERABLE_ERROR) {
					std::cout << "[info ] : UNRECOVERABLE_ERROR was expected and recieved\n";
					testSuccessCount++;
					testCase.pass = true;
				} else {
					std::cerr << "[error] : UNRECOVERABLE_ERROR was expected and BUT NOT recieved\n";
					testsFailCount++;
					testCase.pass = false;
				}

			} catch (VMError& exc) {
				if(exc == testCase.expected) {
					testSuccessCount++;
					std::cout << "[info ] : EXCEPTION expected and recieved; value: " << toString(exc) << "\n";
					testCase.pass = true;
				} else {
					std::cerr	<< "[error] : EXCEPTION expected and recieved BUT of INCORRECT TYPE; value: " << toString(exc) << ", but expected value : "
								<< toString(testCase.expected) << "\n";
					testCase.pass = false;
				}
			}

			std::cout << "TEST END\n\n\n";
		}


		std::cout << "Summary\n\n";

		std::cout << "Lexer/Parser Errors : " << parsingErrorCount << "\n";
		std::cout << "Expected Errors     : " << testSuccessCount << "\n";
		std::cout << "Total tests         : " << testCases.size() << "\n";



		std::cout << "\n\nReport\n\n";
		std::cout << "------\n";
		for(auto& testCase : testCases) {
			std::cout << "Test " << std::setw(50) << testCase.name << " ... " << std::flush;

			std::cout << (testCase.pass?"\x1B[34mPASS (EXPECTED)\033[0m":"\x1B[31mFAIL\033[0m") << "\n";
		}


		if(parsingErrorCount>0) {
			std::cerr   << "[error] : " << "Parse errors detected. Please check test cases.\n";
			std::cout   << "[info ] : " << "Test count for lexer or parser errors must always be zero.\n";
		}

		if(testSuccessCount != testCases.size()) {
			std::cerr << "[error ] : " << "Some test cases failed.\n";
		}

		if(parsingErrorCount>0 or testSuccessCount != testCases.size()) {
			return EXIT_FAILURE;
		}

	}

	std::cout << "All test cases passed :)\n";

	return EXIT_SUCCESS;
}