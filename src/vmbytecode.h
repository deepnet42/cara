/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/**
 * \author DeepNet42
 * \file vmbytecode.h
 * \version 0
 * \date 2022
 * \copyright (c) 2022 DeepNet42.
 * This project is released under the Mozilla Public License v. 2.0. 
 */

#pragma once

#include "vmbytecode.spec.h"
#include "is.h"


namespace hrmtk {


	unsigned short pack(BYTECODE_IMPL::OP opCode, unsigned short data);
	void unpack(unsigned short instruction, BYTECODE_IMPL::OP& opCode, unsigned short& data);


	unsigned short narrow(int value);
	unsigned short pack(const INST& inst);

	
	struct HRMVMBytecodeHeader {
		char identifier[4];
		char version;
	};

	constexpr const HRMVMBytecodeHeader DEFAULT_HRMVMBytecodeHeader() {
		return HRMVMBytecodeHeader{{'H','R','M','B'}, 0};
	}

	
	Instruction unpackToVMNative(unsigned short instruction);
	
};