/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/**
 * \author DeepNet42
 * \file lexer.main.cpp
 * \version 0
 * \date 2021
 * \copyright (c) 2021 DeepNet42.
 * This project is released under the Mozilla Public License v. 2.0.	 
 */

#include <iostream>
#include <fstream>
#include <errno.h>
#include <cstring>
#include <iomanip>
#include <chrono>
#include <type_traits>
#include <string_view>

#include "lexer.h"


using namespace hrmtk;


void usage(std::string_view processName);

int main(int argc, char* argv[]) {


	if(argc < 2) {
		std::cerr << "fetal error  : no input file." << std::endl;
		usage(argv[0]);
		return EXIT_FAILURE;
	}

	std::ifstream file(argv[1]);

	if(!file) {
		std::cerr << "fetal error  : cannot open file named " << argv[1] << "\n";
		std::cerr << "      reason : " << std::strerror(errno) << "\n";
		std::cerr << "      action : exit.\n";
		return EXIT_FAILURE;
	}

	Lexer lexer(file);

	auto startClock = std::chrono::high_resolution_clock::now();
		auto tokens = lexer.getTokens();
	auto endClock	= std::chrono::high_resolution_clock::now();

	

	std::cout << "[" << std::setw(6) << "cursor" << ",(" << std::setw(3)<< "row" << "," << std::setw(3)<< "col" << ")] type   " << std::setw(20) << "\n";
	for(auto& tok : tokens)
		std::cout << tok << std::endl;


	std::cout << "total tokens: " << tokens.size() << "\n";
	std::chrono::duration<double> exectime = endClock-startClock;
	std::cout << "lexer time : " << std::scientific << exectime.count() << " s\n";

	return 0;

}


void usage(std::string_view processName) {
	std::cerr	<< "usage: " << processName << " source_file" << std::endl;
}