/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/**
 * \author DeepNet42
 * \file vm.cpp
 * \version 1
 * \date 2021
 * \date 2023
 * \copyright (c) 2021 DeepNet42.
 * This project is released under the Mozilla Public License v. 2.0. 
 */

#include "vm.h"
#include "is.h"
#include <iostream>
#include <map>
#include <string>
#include <iomanip>
#include <chrono>
#include <functional>
#include "common.h"
#include <sstream>
#include <random>


namespace hrmtk {


VM::VM() : 	mem_{ZERO},
			mem_original{ZERO},
			reg_{MEMORY_EMPTY},
			r_{MEMORY_EMPTY},
			r0_{ZERO},
			r1_{ONE},
			ip_{ZERO},
			ir_{OP::HALT,ZERO},
			op_{OP::HALT},
			oper_{ZERO},
			inp_{ZERO},
			program_{},
			labels_{},
			inbox_{},
			outbox_{},
			mstate_{MState::RESET},
			cycle_{ZERO},
			traceCode_{false},
			time_{0.0},
			stack_{},
			rd{},
			gen{rd()}
			{
}



void VM::fetch() {
	ir_ = program_[ip_++];
}

void VM::decode() {

	op_ = ir_.op;
	oper_ = ir_.oper;
	if(traceCode_) {
		std::cout << ir_ << "\n";
	}

}


void VM::execute() {

	switch(op_) {
		case OP::INBOX: 	if(inp_<inbox_.size()) { 
								inbox();
								rguard();
							} else { 
								 mstate_ = MState::IDLE;
								  /*std::cout << "INBOX IS EMPTY. MACHINE STATE SET TO IDLE\n";*/
							}; 
							break;
		
		case OP::OUTBOX: 	
		
							if(reg_ == MEMORY_EMPTY) {
								throw VMError::REGISTER_EMPTY;
							}

							rguard();
							outbox();

							reg_ = MEMORY_EMPTY;

							break;
		
		case OP::COPYFROM: 	mguard(oper_); copyfrom(); 	rguard(); break;
		case OP::COPYFROMI:	mguardi(oper_); copyfromi(); rguard(); break;
		
		case OP::COPYTO: 	mguard(oper_); rguard(); copyto(); break;
		case OP::COPYTOI: 	mguardi(oper_); rguard(); copytoi(); break;
		
		case OP::ADD:		mguard(oper_); add(); rguard(); break;
		case OP::ADDI:		mguardi(oper_); addi(); rguard(); break;

		case OP::SUB:		mguard(oper_); sub(); rguard();	break;
		case OP::SUBI:		mguardi(oper_); subi(); rguard(); break;

		case OP::BUMPUP:	mguard(oper_);
		
							if	(mem_[oper_]==MEMORY_EMPTY) {
								throw VMError::MEMORY_EMPTY;
							}
							if (mem_[oper_]+1>MAX_REG_VALUE_SIZE) {
								throw VMError::REGISTER_RANGE_ERROR;
							}

							bumpup(); break;
		case OP::BUMPUPI:	mguardi(oper_); 
		
							if	(mem_[mem_[oper_]]==MEMORY_EMPTY) {
								throw VMError::REGISTER_EMPTY;
							}
							if (mem_[mem_[oper_]]+1>MAX_REG_VALUE_SIZE) {
								throw VMError::REGISTER_RANGE_ERROR;
							}
							
							
							bumpupi(); break;

		case OP::BUMPDOWN:	mguard(oper_);
		
		
							if	(mem_[oper_]==MEMORY_EMPTY) {
								throw VMError::REGISTER_EMPTY;
							}
							if (mem_[oper_]-1<MIN_REG_VALUE_SIZE) {
								throw VMError::REGISTER_RANGE_ERROR;
							}

							bumpdown(); break;

		case OP::BUMPDOWNI:	mguardi(oper_);
		

							if	(mem_[mem_[oper_]]==MEMORY_EMPTY) {
								throw VMError::REGISTER_EMPTY;
							}
							if (mem_[mem_[oper_]]-1<MIN_REG_VALUE_SIZE) {
								throw VMError::REGISTER_RANGE_ERROR;
							}
							
							bumpdowni(); break;

		case OP::JUMP:		cguard(oper_); jump(); break;
		case OP::JUMPZ:		cguard(oper_); jumpz(); break;
		case OP::JUMPN:		cguard(oper_); jumpn(); break;



		case OP::HALT:		halt(); break;


		// extended
		case OP::MUL:		mguard(oper_); mul(); rguard(); break;
		case OP::MULI:		mguardi(oper_); muli(); rguard(); break;

		case OP::DIV:		mguard(oper_); div(); rguard(); break; 
		case OP::DIVI:		mguardi(oper_); divi(); rguard(); break; 

		case OP::MOD:		mguard(oper_); mod(); rguard(); break; 
		case OP::MODI:		mguardi(oper_); modi(); rguard(); break; 


		case OP::RAND:		rand(); rguard(); break;

		case OP::PUSH:		rguard(); push(); break;
		case OP::POP:		pop(); rguard(); break;
		// extended

		case OP::UNDEFINED:	unknown_instruction();
							break;

		default: 			halt(); break;
	}


}


bool VM::load(std::vector<INST> p) {
	//check params
	if(p.empty()) {
		return false;
	}


	reset();

	program_ = p;
	mstate_ = MState::READY;

	return true;
}

void VM::run() {


	auto start = std::chrono::high_resolution_clock::now();

	std::vector<int> checks;
	if (!staticCheck(checks)) {
		error("static checks failed");
		info("program will not run!");
		info("see details below");

		std::map<int, std::string> addressToLabel;

		for(auto check : checks) {
			std::stringstream ss;
			ss << program_.at(check).op << " " << program_.at(check).oper;
			error(std::string("Code Address # ") + std::to_string(check) + " " + ss.str() + " (BAD JUMP LOCATION.)");


		}

		mstate_ = MState::UNRECOVERABLE_ERROR;
		return;
	}


	if(mstate_==MState::READY)
		mstate_=MState::EXECUTING;
	if(traceCode_) {
		info("trace started...");
	} else {
		//std::cout << "not tracing execution\n";
	}
	while(ip_<program_.size() and mstate_ == MState::EXECUTING) {
		cycle_++;
		if(traceCode_) std::cout << "[" << std::setw(5) << cycle_ << "]" << " [" << std::setw(5) << ip_ << "]" << "\t";
		//printMem();
		fetch();
		decode();
		try {
			execute();
			r_ = reg_;
		} catch (VMError& e) {
			::error("recovery impossible. Machine halted.");
			warning("outbox may have partial data up to this point.)");
			std::cerr << "original exception: " << toString(e) << "\n";
			mstate_ = MState::UNRECOVERABLE_ERROR;
			std::cerr << generateFullErrorReportWithState();
		}

		reg_ = r_;
	}

	auto end = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> diff = end-start;
	time_ = diff.count();
}


void VM::run(std::list<PIR> p, std::vector<int> inbox, std::vector<int> memory, std::vector<int>& outbox) {
	load(p);
	setinbox(inbox);
	setmemory(memory);
	//printMem();
	run();
	outbox = outbox_;
}



bool VM::validatedRun	(	std::list<PIR> p, 
							std::vector<int> inbox, 
							std::vector<int> memory,
							std::vector<int>& outbox, 
							std::function<bool(const std::vector<int>&, const std::vector<int>&)> validator
						) {
							run(p,inbox,memory,outbox);
							//printVec(outbox);
							//std::cout << "\n";
							return validator(inbox,outbox);
						}

bool VM::load(std::list<PIR> p) {
	//check params
	if(p.empty()) {
		return false;
	}

	program_.clear();

	for(auto iter = p.begin(); iter != p.end(); ++iter) {
		if(iter->statement.op == OP::RESERVED) {
			//skip over labels as they also have been already
			//preprocessed by the pasrer
		} else {
			program_.push_back(iter->statement);
		}
	}


	mstate_ = MState::READY;
	return true;
}


bool VM::step() {
	if(mstate_==MState::READY)
		mstate_=MState::EXECUTING;
	if(ip_<program_.size() and mstate_ == MState::EXECUTING) {
		cycle_++;
		if(traceCode_) std::cout << "[" << std::setw(5) << cycle_ << "]" << " [" << std::setw(5) << ip_ << "]" << "\t";
		fetch();
		decode();
		try {
			execute();
			r_ = reg_;
		} catch (VMError& e) {
			::error("recovery impossible. Machine halted.");
			warning("outbox may have partial data up to this point.)");
			std::cerr << "orginal excpetion: " << toString(e) << "\n";
			mstate_ = MState::UNRECOVERABLE_ERROR;
			std::cerr << generateFullErrorReportWithState();
		}

		reg_ = r_;
	} else {
		return false;
	}

	return true;
}
void VM::setinbox( const std::vector<int> in) {
	//sanitize in input file;
	
	if(not check_inbox(in)) {
		throw VMError::BAD_INBOX_FILE;
	}

	inbox_ = in;
}

std::vector<int> VM::getoutbox() {
	return outbox_;
}


void VM::setmemory(const std::vector<int>& memory) {


	if(not check_memory(memory)) {
		throw VMError::BAD_MEM_FILE;
	}

	if (memory.size() <= mem_.size()) {
		for(std::size_t i = 0; i < memory.size(); i++) {
			mem_[i] = memory[i];
			mem_original[i] = memory[i];
			// do this better what is this
		}
	}
}


void VM::mem(int address, int value)  {
	if(address < 0 and address >=MEM_SIZE) {
		throw VMError::MEMORY_PROTECTION_FAULT;
	}

	if(value < MIN_REG_VALUE_SIZE and value > MAX_REG_VALUE_SIZE) {
		throw VMError::MEMORY_RANGE_ERROR;
	}

	mem_[address] = value;
}

int VM::mem(int address) const {
	if(address < 0 and address >=MEM_SIZE) {
		throw VMError::MEMORY_PROTECTION_FAULT;
	}
	return mem_[address];
}


void VM::reset() {
	// init the machine begin
	mem_=  mem_original;	// reset memory to original configuration
	reg_ = MEMORY_EMPTY;	// ZERO OUT REGISTER; should behave like HRM (empty)
	r_ = MEMORY_EMPTY;

	ip_ = ZERO;
	ir_ = {OP::HALT,ZERO};

	op_ = ir_.op;
	oper_ = ir_.oper;

	inp_ = ZERO;

	outbox_.clear();
	mstate_ = MState::RESET;

	cycle_ = ZERO;
	time_ = 0.0;
	// init machine done
}




MState VM::machineState() { return mstate_;}


void VM::enableTrace() { traceCode_ = true; }
void VM::disiableTrace() { traceCode_ = false; }

Stats VM::stats() {
	return {cycle_, time_};
}

void VM::inbox()		{ reg_ = inbox_[inp_++]; 	}
void VM::outbox()		{ outbox_.push_back(reg_); 	}
void VM::copyfrom()		{ reg_ = mem_[oper_]; 			}
void VM::copyfromi()	{ reg_ = mem_[mem_[oper_]]; 		}
void VM::copyto()		{ mem_[oper_] = reg_;				}
void VM::copytoi()		{ mem_[mem_[oper_]] = reg_;		}
void VM::add()			{ reg_ = reg_ + mem_[oper_]; 		}
void VM::addi()			{ reg_ = reg_ + mem_[mem_[oper_]]; 	}
void VM::sub()			{ reg_ = reg_ - mem_[oper_]; 			}
void VM::subi()			{ reg_ = reg_ - mem_[mem_[oper_]];	}
void VM::bumpup()		{ mem_[oper_]++; reg_ = mem_[oper_]; 	}
void VM::bumpupi()		{ mem_[mem_[oper_]]++; reg_ = mem_[mem_[oper_]];	}
void VM::bumpdown()		{ mem_[oper_]--; reg_ = mem_[oper_];	}
void VM::bumpdowni()	{ mem_[mem_[oper_]]--; reg_ = mem_[mem_[oper_]];	}
void VM::jump()			{ ip_ = oper_; }
void VM::jumpn()		{ ip_ = ((reg_<0)?oper_:ip_); }
void VM::jumpz()		{ ip_ = ((reg_==0)?oper_:ip_); };
void VM::halt()			{ mstate_ = MState::IDLE; }

//extended
void VM::mul()			{ reg_ = reg_ * mem_[oper_]; 		}
void VM::muli()			{ reg_ = reg_ * mem_[mem_[oper_]]; 	}
void VM::div()			{ if(mem_[oper_]==0) {mstate_ = MState::INTERUPT; throw VMError::DIV_ZERO_EXCEPTION;} reg_ = reg_ / mem_[oper_]; 		}
void VM::divi()			{ if(mem_[mem_[oper_]]==0) {mstate_ = MState::INTERUPT; throw VMError::DIV_ZERO_EXCEPTION;} reg_ = reg_ / mem_[mem_[oper_]]; 	}
void VM::mod()			{ if(mem_[oper_]==0) {mstate_ = MState::INTERUPT; throw VMError::DIV_ZERO_EXCEPTION;} reg_ = reg_ % mem_[oper_]; 		}
void VM::modi()			{ if(mem_[mem_[oper_]]==0) {mstate_ = MState::INTERUPT; throw VMError::DIV_ZERO_EXCEPTION;} reg_ = reg_ % mem_[mem_[oper_]]; 	}

void VM::rand()	{
	std::uniform_int_distribution<> d{MIN_REG_VALUE_SIZE,MAX_REG_VALUE_SIZE};
	reg_ = d(gen);
}

void VM::push() {
	if(stack_.size() >= MEM_SIZE) { // maybe a different size for stack?
		mstate_ = MState::INTERUPT;
		throw VMError::STACK_FULL;
	}
	stack_.push(reg_);
	reg_ = MEMORY_EMPTY; 	// to keep the value or not to keep? mimic outbox operation?
							// pop mirrors inbox. so maybe for consistency mimic outbox? yes.
}


void VM::pop() {
	if(stack_.empty()) {
		mstate_ = MState::INTERUPT;
		throw VMError::STACK_EMPTY;
	}
	reg_ = stack_.top();
	stack_.pop();
}
//extended


//internal

void VM::unknown_instruction() {
	mstate_ = MState::UNRECOVERABLE_ERROR;
	throw VMError::CODE_UNKNOWN_OPCODE;
}


//internal
void VM::mguard(int address) {
	if (address < 0 or address >= MEM_SIZE) {
		mstate_ = MState::INTERUPT;
		throw VMError::MEMORY_PROTECTION_FAULT;
	} 
	
	/*
	if(mem_[address]==MEMORY_EMPTY) {
		mstate_ = MState::INTERUPT;
		throw VMError::MEMORY_EMPTY;
	}
	*/

	

}


void VM::mguardi(int address) {
	if ((address >= 0 or address < MEM_SIZE) and
		(mem_[address] < 0 or mem_[address] >= MEM_SIZE)) {
		mstate_ = MState::INTERUPT;
		throw VMError::MEMORY_PROTECTION_FAULT;
	} else if(address < 0 or address >= MEM_SIZE) {
		throw VMError::MEMORY_PROTECTION_FAULT;
	}

	/*
	if(mem_[mem_[address]]==MEMORY_EMPTY) {
		mstate_ = MState::INTERUPT;
		throw VMError::MEMORY_EMPTY;
	} 
	*/

}

void VM::cguard(int address) {
	if (address < 0 or address >= static_cast<int>(program_.size())) {
		mstate_ = MState::INTERUPT;
		throw VMError::CODE_PROTECTION_FAULT;
	}
}


void VM::rguard() {
	if(reg_ == MEMORY_EMPTY) {
		mstate_ = MState::INTERUPT;
		throw VMError::REGISTER_EMPTY;
	} else if(reg_ < MIN_REG_VALUE_SIZE or reg_ > MAX_REG_VALUE_SIZE) {
		mstate_ = MState::INTERUPT;
		throw VMError::REGISTER_RANGE_ERROR;
	}
}



bool VM::check_memory(const std::vector<int>& mem) {
	for(std::size_t i = 0; i < mem.size(); i++) {
		if(mem[i]!=MEMORY_EMPTY) {
			if(mem[i]<MIN_REG_VALUE_SIZE or mem[i]>MAX_REG_VALUE_SIZE) {
				return false;
			}
		}
	}
	return true;
}

bool VM::check_inbox(const std::vector<int>& input) {
	for(std::size_t i = 0; i < input.size(); i++) {
		if(input[i]<MIN_REG_VALUE_SIZE or input[i]>MAX_REG_VALUE_SIZE) {
			return false;
		}
	}
	return true;
}


std::string VM::generateFullErrorReportWithState(std::vector<std::string> messages) {
	std::stringstream ss;
	ss 	<< "FULL ERROR REPORT:\n";

	for(std::string& m : messages) {
		ss << "error message : " << m << "\n";
	}

	ss 	<< "CPU STATE :\n";
	ss 	<< " cpu ip    = " << ip_ << "\n"
				<< " cpu ir    = " << ir_ << "\n"
				<< " cpu reg   = " << reg_ << "\n"
				<< " cpu cycle = " << cycle_ << "\n"
				<< " cpu state = " << toString(mstate_) << "\n";
	ss 	<< "MEMORY DUMP :\n";
	ss 	<< " AS INT :\n";
	for(std::size_t i = 0; i < MEM_SIZE; i++) {
		if(i % 8 == 0)
			ss << "\n";
		ss << " " << std::setw(5) << mem_[i];
	}

	ss	<< "\n";
	ss	<< "INBOX\n";
	ss	<< " inp_ = " << inp_ << "\n"
		<< " inbox item " << (inp_<inbox_.size()?inbox_[inp_]:'.') << "\n";

	ss 	<< "INBOX DUMP :\n";
	ss 	<< " AS INT :\n";
	for(std::size_t i = 0; i < inbox_.size(); i++) {
		ss << " " << std::setw(5) << inbox_[i];
	}
	ss  << "\n";
	ss	<< "OUTBOX DUMP :\n";
	ss 	<< " AS INT :\n";
	for(std::size_t i = 0; i < outbox_.size(); i++) {
		ss << " " << std::setw(5) << outbox_[i];
	}
	ss << "\n";

	return ss.str();

}

bool VM::staticCheck(std::vector<int>& checks) {
	int isgood = true;
	int programSize = program_.size();

	// tricky
	// what about a listing with no instructions but only labels?
	int i = 0;
	for(auto& statement : program_) {
		if(	statement.op == OP::JUMP or
			statement.op == OP::JUMPN or
			statement.op == OP::JUMPZ) {
			if(statement.oper >= programSize or statement.oper < 0) {
				checks.push_back(i);
				isgood = false;
			}
		}
		i++;
	}

	return isgood;

}

void VM::printMem() {
	for(std::size_t i = 0; i < mem_.size(); i++) {
		std::stringstream ss;
		if(mem_[i] != MEMORY_EMPTY) {
			ss << "[i:" << i << ","<< mem_[i] << "]"<< std::setw(6);
			info(ss.str());
		}
	}
}

std::string toString(MState mstate) {
	using enum MState;
	switch(mstate) {
		case READY					: return "READY";
		case IDLE					: return "IDLE";
		case INTERUPT				: return "INTERUPT";
		case RESET					: return "RESET";
		case EXECUTING				: return "EXECUTING";
		case UNRECOVERABLE_ERROR	: return "UNRECOVERABLE_ERROR";
		default						: return "UNKNOWN STATE";
	};
	return "UNKNOWN STATE";	
}
std::string toString(VMError vmerror) {
	using enum VMError;
	switch(vmerror) {
		case MEMORY_EMPTY			: return "Uninitialized Memory. Initialize memory with a valid value.";
		case MEMORY_RANGE_ERROR		: return "Value in memory is out of range. Write to memory with a valid value.";
		case MEMORY_PROTECTION_FAULT: return "Memory protection fault! You cannot address memory you don't own.";
		case REGISTER_EMPTY			: return "Register is empty. A register must have a valid value.";
		case REGISTER_RANGE_ERROR	: return "Register range error. A register must contain a valid value.";
		case DIV_ZERO_EXCEPTION		: return "Division by zero error. Division by zero is undefined.";
		case BAD_INBOX_FILE			: return "Inbox file malformed.";
		case BAD_MEM_FILE			: return "Memory file malformed.";
		case CODE_PROTECTION_FAULT 	: return "Code address violation. Trying to address non executable memory.";
		case CODE_UNKNOWN_OPCODE 	: return "Unknown instruction (OPCODE).";
		case STACK_EMPTY		 	: return "STACK EMPTY. Trying to pop from an empty stack";
		case STACK_FULL 			: return "STACK FULL. Trying to push into a full stack.";
		default 					: return "generic error. ???";
	};
	return "HAT TRICK!";	
}


}
