/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/**
 * \author DeepNet42
 * \file is.h
 * \version 0
 * \date 2021
 * \copyright (c) 2021 DeepNet42.
 * This project is released under the Mozilla Public License v. 2.0. 
 */

#ifndef __HRMTK__IS_H__
#define __HRMTK__IS_H__

#include <map>
#include <iostream>

namespace hrmtk {


/**
 * \brief		Operator contains all valid instructions. See [README.md](README.md) for detailed instruction
 *				documentation.
 */
enum class OPERATOR {

	INBOX,
	OUTBOX,
	COPYTO,
	COPYTOI,
	COPYFROM,
	COPYFROMI,
	ADD,
	ADDI,
	SUB,
	SUBI,
	BUMPUP,
	BUMPUPI,
	BUMPDOWN,
	BUMPDOWNI,
	JUMP,
	JUMPZ,
	JUMPN,


	// extended
	MUL,
	DIV,
	MOD,
	MULI,
	DIVI,
	MODI,

	RAND,
	PUSH,
	POP,
	// extended

	// NOT ACTUAL INSTRUCTIONS BUT HAVE MEANING TO THE INTERNALS
	// OF LEXER AND PARSER
	
	HALT,
	UNDEFINED,
	RESERVED
};


using OP = OPERATOR;

struct Instruction {
	OP op;
	int oper;
};

using INST = Instruction;


const std::map<const OPERATOR, const std::string_view> OPERATOR_STRINGS = {
		{OP::INBOX,"INBOX"},
		{OP::OUTBOX,"OUTBOX"},
		{OP::COPYTO,"COPYTO"},
		{OP::COPYTOI,"COPYTOI"},
		{OP::COPYFROM,"COPYFROM"},
		{OP::COPYFROMI,"COPYFROMI"},
		{OP::ADD,"ADD"},
		{OP::ADDI,"ADDI"},
		{OP::SUB,"SUB"},
		{OP::SUBI,"SUBI"},
		{OP::BUMPUP,"BUMPUP"},
		{OP::BUMPUPI,"BUMPUPI"},
		{OP::BUMPDOWN,"BUMPDOWN"},
		{OP::BUMPDOWNI,"BUMPDOWNI"},
		{OP::JUMP,"JUMP"},
		{OP::JUMPZ,"JUMPZ"},
		{OP::JUMPN,"JUMPN"},
		{OP::HALT,"HALT"},
		{OP::UNDEFINED,"UNDEFINED"},
		{OP::RESERVED,"RESERVED"},
		// extended
		{OP::MUL,"MUL"},
		{OP::DIV,"DIV"},
		{OP::MOD,"MOD"},
		{OP::MULI,"MULI"},
		{OP::DIVI,"DIVI"},
		{OP::MODI,"MODI"},

		{OP::RAND,"RAND"},
		{OP::PUSH,"PUSH"},
		{OP::POP,"POP"}
		// extended
};


std::ostream& operator<<(std::ostream& os, const OPERATOR& op);
std::ostream& operator<<(std::ostream& os, const Instruction& inst);

}

#endif
