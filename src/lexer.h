/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/**
 * \author DeepNet42
 * \file lexer.h
 * \version 0
 * \date 2021
 * \copyright (c) 2021 DeepNet42.
 * This project is released under the Mozilla Public License v. 2.0.	 
 */



/**
 * A very very simple tokenizer. 
 */


#ifndef __HRMTK__LEXER_H__
#define __HRMTK__LEXER_H__

#include <fstream>
#include <string>
#include <unordered_set>
#include <vector>
#include <string_view>

namespace hrmtk {


/**
 * \brief HRM_TOKEN_TYPE represents all possible tokens for the HRM language.
 */


enum class HRM_TOKEN_TYPE {
	MNEMONIC,				///< a set of {INBOX, OUTBOX, COPYTO, COPYFROM, etc...}
	ADDRESS,				///< a numerical (integer) value greater than or equal to zero
	LABEL_DECLARATION,		
	LABEL_NAME,				///< label names can be a single character or a string
	LBRACKET,				
	RBRACKET,
	COLON,
	WS,						///< one of "\n\r\t "
	COMMENT,				///< this is a comment that starts with \p --- and ends with \p---
	BEGIN_DEFINITION,
	KEYWORD_COMMENT,
	KEYWORD_LABEL,
	RAW_BLOB,				///< the base64 comment data or cell labeling
	UNDEFINED,				///< all tokens start as \p UNDEFINED
	END_TOKEN				///< A special token indicates that there are no more tokens (end of listing)
};



constexpr const char S_WS =			' ';
constexpr const char S_NL =			'\n';
constexpr const char S_CR =			'\r';
constexpr const char S_HT =			'\t';
constexpr const char S_LBRACKET =	'[';
constexpr const char S_RBRACKET =	']';
constexpr const char S_COLON =		':';
constexpr const char S_MINUS =		'-';
constexpr const char S_SEMICOLON =	';';


/*
Case sensitivity mattered in the early versions but is relaxed in the implementation to be case insensitive. 
Mnemonics are converted to uppercase internally. Label names and comment sections are case sensitive.
*/


constexpr const std::string_view M_INBOX		= "INBOX";
constexpr const std::string_view M_OUTBOX		= "OUTBOX";
constexpr const std::string_view M_COPYTO 		= "COPYTO";
constexpr const std::string_view M_COPYFROM 	= "COPYFROM";
constexpr const std::string_view M_ADD 			= "ADD";
constexpr const std::string_view M_SUB 			= "SUB";
constexpr const std::string_view M_BUMPUP 		= "BUMPUP";
constexpr const std::string_view M_BUMPDOWN		= "BUMPDN";
constexpr const std::string_view M_JUMP			= "JUMP";
constexpr const std::string_view M_JUMPN		= "JUMPN";
constexpr const std::string_view M_JUMPZ		= "JUMPZ";
constexpr const std::string_view M_HALT			= "HALT";
constexpr const std::string_view M_COMMENT		= "COMMENT";
constexpr const std::string_view M_DEFINE		= "DEFINE";
constexpr const std::string_view M_LABEL		= "LABEL";
constexpr const std::string_view M_RESERVED		= "RESERVED";





/* --- extended instructions --- */
/* 
	leaving this as a suggestion:
	below functionality can easily be added.
*/
constexpr const std::string_view M_MUL         = "MUL";
constexpr const std::string_view M_DIV         = "DIV";
constexpr const std::string_view M_MOD         = "MOD";

constexpr const std::string_view M_RAND         = "RAND";
constexpr const std::string_view M_PUSH         = "PUSH";
constexpr const std::string_view M_POP			= "POP";

/* --- extended instructions --- */



/**
 * \brief 	collection of important mnemonics symbols to actual string values
 *			used to catch all mnemonics. Only supported mnemonics are stored
 *			in the MNEMONIC_TABLE set.
 */

const std::unordered_set<std::string_view> MNEMONIC_TABLE = {
	M_INBOX, M_OUTBOX, M_COPYTO, M_COPYFROM, M_ADD, M_SUB,
	M_BUMPUP, M_BUMPDOWN, M_JUMP, M_JUMPZ, M_JUMPN, M_HALT,
	/* extended instructions*/
	M_MUL, M_DIV, M_MOD, M_RAND, M_PUSH, M_POP
};



/**
 * \brief 	a HRM_Token consists of the most basic data.
 *			All tokens live as undefined initially. It is possible to have something that doesn't
 *			resemble a valid token. 
 */

struct HRM_Token {
	HRM_TOKEN_TYPE type{HRM_TOKEN_TYPE::UNDEFINED};			///< actual type 
	std::string value{};									///< string text value that determined the actual type
	int cursor{-1}, 										///< global index (character position)
	row{-1}, 												///< '\n' increases row
	col{-1};												///< count since last '\n'
};


std::ostream& operator<<(std::ostream& os, const HRM_Token& token);


/**
 * \brief 	Lexer will tokenize a source file into tokens which 
 *			can be used by the parser for further processing.
 *			All tokens are generated at once. 
 */
class Lexer {
public:

	/**
	 * \param[in]	file 	Must be opened by the caller. Lexer will not manage this 
	 *						file resource. The caller must manage opening and closing 
	 *						of the file. File can be closed after getTokens() is called
	 *						the first time. At that point all tokens are ready.
	 * \pre					File must be valid.
	 * \remark 				parsing is delayed until \p getTokens();
	 *
	 */

	Lexer(std::istream& file);



	/**
	 * \brief 		get all of the tokens at once. [streaming is not currently supported]
	 *
	 * \pre			valid file
	 * \post		valid tokens
	 * \return 		all of the tokens. 
	 * \warning		A lexer does not handle any kind of errors or warnings
	 *				but it will emit them. The parser handles all errors. Warnings are ignored.
	 *
	 */
	std::vector<HRM_Token> getTokens();


private:



	HRM_Token nextToken();
	
	void lex();

	bool atend();
	char getch();


	std::string getword();
	char peekch();


	void set(HRM_TOKEN_TYPE type);

	void lexText();
	void lexComment();
	void lexNumber();

	bool isws(char ch);

	void skipWS(bool ws=true, bool nl=true, bool cr=true, bool ht=true);

	void tokenize();

	std::string getTextUntilSemicolon();


	enum class LEXER_STATE {NORMAL, DEFINITIONS};

	void setLexState(LEXER_STATE state);



	std::istream& file_;

	bool atend_;
	int cursor_;
	int row_;
	int col_;
	
	HRM_Token tok_;
	std::vector<HRM_Token> tokens_;
	bool tokenized_;
	bool previouslyNewLine;

	LEXER_STATE lexState_;
	int define_;
	int label_;
	int comment_;
	int number_;
	int order_;

};




}
#endif
