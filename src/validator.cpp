
/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
/*
ORGANIZATION

Files:
"hrm_code_data.cpp" - string data for all hrm test cases (hrm programs)
"hrm_code_lambda_test_cases.cpp" - bindings for testcase id to actual hrm program for loading
"hrm_code_inboxes.cpp" - inbox for the test cases
"hrm_code_memory.cpp" - memory for the test cases

*/

#include "hrm_code_data.cpp"
#include "hrm_code_lambda_test_cases.cpp"
#include "hrm_code_inboxes.cpp"
#include "hrm_code_memory.cpp"
