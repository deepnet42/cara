/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/**
 * \author DeepNet42
 * \file is.cpp
 * \version 0
 * \date 2021
 * \copyright (c) 2021 DeepNet42.
 * This project is released under the Mozilla Public License v. 2.0. 
 */

# include "is.h"
# include "common.h"

namespace hrmtk {



	std::ostream& operator<<(std::ostream& os, const OPERATOR& op)
	{
		try {
			os << OPERATOR_STRINGS.at(op);
		} catch (std::out_of_range& e) {
			//std::cout << "??? UNKNOWN ???";
			warning("??? UNKNOWN ???"); // might be legal we just don't have a string representation
		}
		return os;
	}


	std::ostream& operator<<(std::ostream& os, const Instruction& inst) {
		try {
			os << OPERATOR_STRINGS.at(inst.op);
		} catch (std::out_of_range& e) {
			//std::cout << "??? UNKNOWN ???";
			warning("??? UNKNOWN ???"); // might be legal we just don't have a string representation
		}
		
		switch(inst.op) {
		case OP::INBOX: [[fallthrough]];
		case OP::OUTBOX: [[fallthrough]];
		case OP::RAND: [[fallthrough]];
		case OP::PUSH: [[fallthrough]];
		case OP::POP: 
			break;
		default:
			os << ' ' << inst.oper;
			break;
		}
		return os;
	}
}
