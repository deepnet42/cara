/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

 
using MEMORY = std::vector<int>;
std::map<std::string, MEMORY> FLOOR;

constexpr const int E = MEMORY_EMPTY;

FLOOR.emplace("exclusivelounge",MEMORY{E,E,E,E,0,1});
FLOOR.emplace("vowelincinerator",MEMORY{'A','E','I','O','U',0});
FLOOR.emplace("duplicateremoval", MEMORY{E,E,E,E,E,E,E,E,E,E,E,E,E,E,0});
FLOOR.emplace("alphabetizer",MEMORY{E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,0,10});
FLOOR.emplace("digitexploder",MEMORY{E,E,E,E,E,E,E,E,E,0,10,100});
FLOOR.emplace("recoordinator",MEMORY{E,E,E,E,E,E,E,E,E,E,E,E,E,E,0,4});
FLOOR.emplace("sorting",MEMORY{E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,0});
FLOOR.emplace("cumulativecountdown",MEMORY{E,E,E,E,E,0});
FLOOR.emplace("zeroterminatedsum",MEMORY{E,E,E,E,E,0});
FLOOR.emplace("smalldivide",MEMORY{E,E,E,E,E,E,E,E,0});
FLOOR.emplace("stringreverse",MEMORY{E,E,E,E,E,E,E,E,E,E,E,E,E,E,0});
FLOOR.emplace("fibonaccivisitor",MEMORY{E,E,E,E,E,E,E,E,E,0});
FLOOR.emplace("multiplicationworkshop",MEMORY{E,E,E,E,E,E,E,E,E,0});

